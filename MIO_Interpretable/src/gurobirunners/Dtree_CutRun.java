package gurobirunners;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import org.ejml.data.DenseMatrix64F;
import org.ejml.ops.MatrixIO;
import org.ejml.simple.SimpleMatrix;

import gurobi.GRB;
import gurobi.GRBCallback;
import gurobi.GRBConstr;
import gurobi.GRBException;
import gurobi.GRBLinExpr;
import gurobimodels.Dtree_Cut;


public class Dtree_CutRun { 

	public static Dtree_Cut actual;
	public static String basedir;
	public static int Depth;
 	public static double timeLimit;
 	static double numerical_instability_epsilon = 0.001;

	 public static void main(String[] args) {
		 
		 	
		 	basedir = "/Users/amulyayadav/Dropbox/Work/PostAAMAS/PhebeWork/Bottleneck/";
		 	Depth=2;
		 	timeLimit = 600;
		 	
		 	
		 
		 	
		 	DenseMatrix64F dataset;
		    SimpleMatrix originalData, data, train, test;
		    
            try
            {
                
                /*String basedir = args[0];
                int Depth=Integer.parseInt(args[1]);
                double timeLimit = Double.parseDouble(args[2]);*/
                
            	
                
                //dataset = MatrixIO.loadCSV(basedir+"masterdataset-test.csv");
            	dataset = MatrixIO.loadCSV(basedir+"BinaryDatasets/german_numer/german_numer_normalized_pres2.csv");
            	
                originalData = SimpleMatrix.wrap(dataset);


                data = originalData;


                train = data;
                ////train and test constructed
                
                
                ///running code  
                actual = new Dtree_Cut(Depth, train);
                //actual.model.set(GRB.IntParam.LazyConstraints, 1);
                actual.model.getEnv().set(GRB.IntParam.Method, 0);
                actual.model.getEnv().set(GRB.IntParam.BranchDir, 1);
                actual.model.getEnv().set(GRB.IntParam.NodeMethod, 0);
                //actual.model.getEnv().set(GRB.DoubleParam.TimeLimit, timeLimit);
                actual.model.getEnv().set(GRB.IntParam.MIPFocus, 3);
                //actual.model.getEnv().set(GRB.IntParam.SubMIPNodes, 2000);
                
                //Dtree4Run cb = new Dtree4Run();
                
               //actual.model.setCallback(cb);
                //actual.provideWarmStartCART(basedir+"masterdataset-mip-weka.csv");
                actual.provideWarmStartCART(basedir+"BinaryDatasets/german_numer/german_numer_weka.csv");
                actual.model.write(basedir+"BinaryDatasets/german_numer/model.lp");
                //actual.model.write(basedir+"model.lp");
                actual.model.optimize();
				
	    	
		actual.printSolution();
                
                /*System.out.println("The model is infeasible; computing IIS");
			      actual.model.computeIIS();
			      System.out.println("\nThe following constraint(s) "
			          + "cannot be satisfied:");
			      for (GRBConstr c : actual.model.getConstrs()) {
			        if (c.get(GRB.IntAttr.IISConstr) == 1) {
			          System.out.println(c.get(GRB.StringAttr.ConstrName));
			        }
			      }
			      
			      actual.model.write(basedir+"model.ilp");*/
			      
		System.out.println("Solution Quality: "+actual.model.get(GRB.DoubleAttr.ObjVal));
		//System.out.println("Runtime: "+actual.model.get(GRB.DoubleAttr.Runtime));
				
 
            }
            catch(IOException e)
            {
            	e.printStackTrace();
            } catch (GRBException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		    

	 }

}

