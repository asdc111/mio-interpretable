package gurobirunners;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import org.ejml.data.DenseMatrix64F;
import org.ejml.ops.MatrixIO;
import org.ejml.simple.SimpleMatrix;

import gurobi.GRB;
import gurobi.GRBCallback;
import gurobi.GRBConstr;
import gurobi.GRBException;
import gurobi.GRBLinExpr;
import gurobimodels.Dtree_CutBertsimas;
import weka.classifiers.Evaluation;
import weka.classifiers.trees.SimpleCart;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;


public class Dtree_CutBertsimasRun { 

	public static Dtree_CutBertsimas actual;
	public static String basedir;
	public static int Depth;
 	public static double timeLimit;
 	static double numerical_instability_epsilon = 0.001;

	 public static void main(String[] args) {
		 
		 	
		 	basedir = "/Users/amulyayadav/Dropbox/Work/PostAAMAS/PhebeWork/Bottleneck/";
		 	Depth=2;
		 	timeLimit = 600;
		 	
		 	
		 
		 	
		 	DenseMatrix64F dataset;
		    SimpleMatrix originalData, data, train, test;
		    
            try
            {
                
                /*String basedir = args[0];
                int Depth=Integer.parseInt(args[1]);
                double timeLimit = Double.parseDouble(args[2]);*/
                
            	
                
                //dataset = MatrixIO.loadCSV(basedir+"masterdataset-test.csv");
            	dataset = MatrixIO.loadCSV(basedir+"BinaryDatasets/german_numer/german_numer_normalized_pres2.csv");
            	
                originalData = SimpleMatrix.wrap(dataset);


                data = originalData;


                train = data;
                ////train and test constructed
                
                
                double upperBound = setFullCARTUpperBound(basedir+"BinaryDatasets/german_numer/german_numer_weka.csv");
                upperBound=733;
                System.out.println("The upper bound constructed is: "+ upperBound);
                
                ///running code
                actual = new Dtree_CutBertsimas(Depth, train, upperBound);
                //actual.model.set(GRB.IntParam.LazyConstraints, 1);
                actual.model.getEnv().set(GRB.IntParam.Method, 0);
                actual.model.getEnv().set(GRB.IntParam.BranchDir, 1);
                actual.model.getEnv().set(GRB.IntParam.NodeMethod, 0);
                //actual.model.getEnv().set(GRB.DoubleParam.TimeLimit, timeLimit);
                actual.model.getEnv().set(GRB.IntParam.MIPFocus, 1);
                //actual.model.getEnv().set(GRB.IntParam.SubMIPNodes, 2000);
                
                //Dtree4Run cb = new Dtree4Run();
                
               //actual.model.setCallback(cb);
                //actual.provideWarmStartCART(basedir+"masterdataset-mip-weka.csv");
                actual.provideWarmStartCART(basedir+"BinaryDatasets/german_numer/german_numer_weka.csv");
                //actual.model.write(basedir+"BinaryDatasets/german_numer/model.lp");
                //actual.model.write(basedir+"model.lp");
                actual.model.optimize();
				
	    	
		actual.printSolution();
                
                /*System.out.println("The model is infeasible; computing IIS");
			      actual.model.computeIIS();
			      System.out.println("\nThe following constraint(s) "
			          + "cannot be satisfied:");
			      for (GRBConstr c : actual.model.getConstrs()) {
			        if (c.get(GRB.IntAttr.IISConstr) == 1) {
			          System.out.println(c.get(GRB.StringAttr.ConstrName));
			        }
			      }
			      
			      actual.model.write(basedir+"model.ilp");*/
			      
		System.out.println("Solution Quality: "+actual.model.get(GRB.DoubleAttr.ObjVal));
		//System.out.println("Runtime: "+actual.model.get(GRB.DoubleAttr.Runtime));
				
 
            }
            catch(IOException e)
            {
            	e.printStackTrace();
            } catch (GRBException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		    

	 }
	 
	 public static double setFullCARTUpperBound(String file)
	 {
		 double upperBound=0;
		 try
		 {
			DataSource source = new DataSource(file);
			Instances data = source.getDataSet();
			if (data.classIndex() == -1)
				   data.setClassIndex(data.numAttributes() - 1);
		
			
			SimpleCart tree = new SimpleCart();
			String[] options = weka.core.Utils.splitOptions("-M 2.0 -N 5 -C 1 -S 1");
			//String[] options = weka.core.Utils.splitOptions("-M "+Nmin+" -V 0.001 -N 3 -S 1 -L "+treeDepth+" -P -I 0.0");
			//String[] options = weka.core.Utils.splitOptions("-M "+Nmin+" -L "+treeDepth);
			tree.setOptions(options);
			tree.buildClassifier(data);
			
			Evaluation eval = new Evaluation(data);
			eval.evaluateModel(tree, data);
			
			System.out.println("True positives in Upper Bound: " + eval.numTruePositives(1));
			System.out.println("True negatives in Upper Bound: " + eval.numTrueNegatives(1));
			
			upperBound = eval.numTruePositives(1) + eval.numTrueNegatives(1);
			
			//System.out.println(tree.toString());
		 }
		 catch(Exception e)
		 {
			 e.printStackTrace();
		 }
		 return upperBound;
	 }

}

