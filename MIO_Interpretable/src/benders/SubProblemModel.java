package benders;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

import org.ejml.simple.SimpleMatrix;

import gurobi.GRBVar;
import weka.classifiers.trees.REPTree;
import weka.classifiers.trees.REPTree.Tree;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;
import gurobi.GRB;
import gurobi.GRBEnv;
import gurobi.GRBException;
import gurobi.GRBLinExpr;
import gurobi.GRBModel;

//MIP 4 IMPLEMENTATION
public class SubProblemModel {
    public GRBEnv    env;
    public GRBModel  model;
    
    public GRBVar alpha1[], alpha2[][], alpha3[][], alpha4[][], alpha5[][], alpha6[][], alpha7[], alpha8[], alpha9[][], alpha10[][], alpha11[], alpha12[];
    public GRBVar alpha13[];
    
    
    SimpleMatrix train, test;
    SimpleMatrix  Xtrain, Ytrain, Xtest, Ytest;
    
    int treeDepth, Nmin;
    double interpretabilityWeights[];
    double lambda,lambda2;
    String outputfile;
    
    static double numerical_instability_epsilon = 0.001;
    
    public SubProblemModel(int treeDepth, SimpleMatrix Xtrain, SimpleMatrix Ytrain)
    {
    	try
    	{	
    		
    		this.treeDepth = treeDepth;
        	
        	
       		this.Xtrain = Xtrain; 
       		this.Ytrain = Ytrain;
    		
    		env   = new GRBEnv("mip1.log");
    		model = new GRBModel(env);
    	
    		specifyModel();
    	}
    	catch(GRBException e)
    	{
    		System.out.println("Error code: " + e.getErrorCode() + ". " +
                    e.getMessage());
    	}
    }
    
    public void specifyModel()
    {
    	try
    	{
    		int numTreeNodeVars = (int) (Math.pow(2, treeDepth+1) - 1);
    		
    		alpha1 = new GRBVar[Xtrain.numRows()];
    		alpha7 = new GRBVar[Xtrain.numRows()];
    		alpha8 = new GRBVar[Xtrain.numRows()];
    		alpha11 = new GRBVar[Xtrain.numRows()];
    		alpha12 = new GRBVar[Xtrain.numRows()];
    		for (int i=0;i<Xtrain.numRows();i++)
    		{
    			alpha1[i] = model.addVar(-GRB.INFINITY, GRB.INFINITY, 0.0, GRB.CONTINUOUS, "alpha1_"+i);//Unrestricted
    			alpha7[i] = model.addVar(-GRB.INFINITY, GRB.INFINITY, 0.0, GRB.CONTINUOUS, "alpha7_"+i);//Unrestricted
    			alpha8[i] = model.addVar(-GRB.INFINITY, GRB.INFINITY, 0.0, GRB.CONTINUOUS, "alpha8_"+i);//Unrestricted
    			
    			alpha11[i] = model.addVar(0.0, GRB.INFINITY, 0.0, GRB.CONTINUOUS, "alpha11_"+i);//Non negative
    			alpha12[i] = model.addVar(0.0, GRB.INFINITY, 0.0, GRB.CONTINUOUS, "alpha12_"+i);//Non negative
    		}
    		
    		
    		alpha2 = new GRBVar[Xtrain.numRows()][numTreeNodeVars/2];
    		alpha3 = new GRBVar[Xtrain.numRows()][numTreeNodeVars/2];
    		for (int i=0;i<Xtrain.numRows();i++)
    		{
    			for (int n=0;n<numTreeNodeVars/2;n++)
    			{
    				alpha2[i][n] = model.addVar(0.0, GRB.INFINITY, 0.0, GRB.CONTINUOUS, "alpha2_"+i+"_"+n);//Non negative
    				alpha3[i][n] = model.addVar(0.0, GRB.INFINITY, 0.0, GRB.CONTINUOUS, "alpha3_"+i+"_"+n);//Non negative
    			}
    		}
    		
    		
    		alpha4 = new GRBVar[Xtrain.numRows()][numTreeNodeVars/2+1];
    		alpha5 = new GRBVar[Xtrain.numRows()][numTreeNodeVars/2+1];
    		alpha6 = new GRBVar[Xtrain.numRows()][numTreeNodeVars/2+1];
    		alpha9 = new GRBVar[Xtrain.numRows()][numTreeNodeVars/2+1];
    		alpha10 = new GRBVar[Xtrain.numRows()][numTreeNodeVars/2+1];
    		alpha13 = new GRBVar[numTreeNodeVars/2+1];
    		for (int l=0;l<numTreeNodeVars/2+1;l++)
    		{
    			alpha13[l] = model.addVar(0.0, GRB.INFINITY, 0.0, GRB.CONTINUOUS, "alpha13_"+l);//Non negative 
    			for (int i=0;i<Xtrain.numRows();i++)
    			{
    				alpha4[i][l] = model.addVar(0.0, GRB.INFINITY, 0.0, GRB.CONTINUOUS, "alpha4_"+i+"_"+l);//Non negative
    				alpha5[i][l] = model.addVar(0.0, GRB.INFINITY, 0.0, GRB.CONTINUOUS, "alpha5_"+i+"_"+l);//Non negative
    				alpha6[i][l] = model.addVar(0.0, GRB.INFINITY, 0.0, GRB.CONTINUOUS, "alpha6_"+i+"_"+l);//Non negative
    				alpha9[i][l] = model.addVar(0.0, GRB.INFINITY, 0.0, GRB.CONTINUOUS, "alpha9_"+i+"_"+l);//Non negative
    				alpha10[i][l] = model.addVar(0.0, GRB.INFINITY, 0.0, GRB.CONTINUOUS, "alpha10_"+i+"_"+l);//Non negative
    			}
    		}
    		
    		model.update();

    		
    		//DUMMY OBJECTIVE
    		GRBLinExpr objf1 = new GRBLinExpr();
    		objf1.addTerm(1.0, alpha1[0]);
    		model.setObjective(objf1, GRB.MINIMIZE);
    		
    		
    		///constraints
    		for (int l=0;l<numTreeNodeVars/2+1;l++)
    		{
    			ArrayList<Integer> leftAncestors = new ArrayList<Integer>();
    			ArrayList<Integer> rightAncestors = new ArrayList<Integer>();
    			
    			int currLeafNode = numTreeNodeVars/2 + l;
    			boolean rootReached = false;
    			while(!rootReached)
    			{
    				if (currLeafNode%2==0)///right child
    				{
    					currLeafNode = currLeafNode/2-1;
    					rightAncestors.add(currLeafNode);
    					if (currLeafNode<=0)
							rootReached = true;
    				}
    				else//left child
    				{
    					currLeafNode = currLeafNode/2;
    					leftAncestors.add(currLeafNode);
    					if (currLeafNode<=0)
							rootReached = true;
    				}
    			}
    			
    			
    			GRBLinExpr ex5 = new GRBLinExpr();
    			ex5.addTerm(1.0, alpha13[l]);
    			for (int i=0;i<Xtrain.numRows();i++)
    			{
    				ex5.addTerm(1.0, alpha4[i][l]);
    				ex5.addTerm(-1.0, alpha5[i][l]);
    			}
    			model.addConstr(ex5, GRB.GREATER_EQUAL, 0.0, "eq5_"+l);
    			
    			for (int i=0;i<Xtrain.numRows();i++)
    			{
    				GRBLinExpr ex1 = new GRBLinExpr();
    				ex1.addTerm(1.0, alpha1[i]);
    				
    				for (int n=0;n<rightAncestors.size();n++)
    					ex1.addTerm(1.0, alpha2[i][rightAncestors.get(n)]);
    				
    				for (int n=0;n<leftAncestors.size();n++)
    					ex1.addTerm(1.0, alpha3[i][leftAncestors.get(n)]);
    				
    				ex1.addTerm(1.0, alpha4[i][l]);
    				ex1.addTerm(-1.0, alpha6[i][l]);
    				if (Ytrain.get(i)<numerical_instability_epsilon)
    					ex1.addTerm(-1.0, alpha8[i]);
    				
    				ex1.addTerm(1.0, alpha9[i][l]);
    				
    				model.addConstr(ex1, GRB.GREATER_EQUAL, 0.0, "eq1_"+i+"_"+l);
    				
    				
    				GRBLinExpr ex2 = new GRBLinExpr();
    				ex2.addTerm(-1.0, alpha4[i][l]);
    				ex2.addTerm(1.0, alpha5[i][l]);
    				ex2.addTerm(1.0, alpha6[i][l]);
    				if (Ytrain.get(i)>1-numerical_instability_epsilon)
    					ex2.addTerm(-1.0, alpha7[i]);
    				if (Ytrain.get(i)<numerical_instability_epsilon)
    					ex2.addTerm(1.0, alpha8[i]);
    				ex2.addTerm(1.0, alpha10[i][l]);
    				
    				model.addConstr(ex2, GRB.GREATER_EQUAL, 0.0, "eq2_"+i+"_"+l);
    			}
    		}
    		
    		
    		for (int i=0;i<Xtrain.numRows();i++)
    		{
    			if (Ytrain.get(i)>1-numerical_instability_epsilon)
    			{
    				GRBLinExpr ex3 = new GRBLinExpr();
    				ex3.addTerm(1.0, alpha7[i]);
    				ex3.addTerm(1.0, alpha11[i]);
    				model.addConstr(ex3, GRB.GREATER_EQUAL, 1.0, "eq3_"+i);
    			}
    			
    			if (Ytrain.get(i)<numerical_instability_epsilon)
    			{
    				GRBLinExpr ex4 = new GRBLinExpr();
    				ex4.addTerm(1.0, alpha8[i]);
    				ex4.addTerm(1.0, alpha12[i]);
    				model.addConstr(ex4, GRB.GREATER_EQUAL, 1.0, "eq4_"+i);
    			}
    		}
    		model.update();
    	}
    	catch(GRBException e)
    	{
    		System.out.println("Error code: " + e.getErrorCode() + ". " +
                    e.getMessage());
    	}
    }
    
    public void updateSubProblemObjective(double w[][])
    {
    	try
    	{
	    	int numTreeNodeVars = (int) (Math.pow(2, treeDepth+1) - 1);
	    	
	    	GRBLinExpr newObj = new GRBLinExpr();
	    	for (int i=0;i<Xtrain.numRows();i++)
	    	{
	    		newObj.addTerm(1.0, alpha1[i]);
	    		if (Ytrain.get(i)>1-numerical_instability_epsilon)
	    			newObj.addTerm(1.0, alpha11[i]);
	    		if (Ytrain.get(i)<numerical_instability_epsilon)
	    			newObj.addTerm(1.0, alpha12[i]);
	    		
	    		for (int n=0;n<numTreeNodeVars/2;n++)
	    		{
	    			newObj.addTerm(1-w[i][n], alpha2[i][n]);
	    			newObj.addTerm(w[i][n], alpha3[i][n]);
	    		}
	    		
	    		for (int l=0;l<numTreeNodeVars/2+1;l++)
	    		{
	    			newObj.addTerm(1.0, alpha4[i][l]);
	    			newObj.addTerm(1.0, alpha9[i][l]);
	    			newObj.addTerm(1.0, alpha10[i][l]);
	    		}
    	}
    	
    	for (int l=0;l<numTreeNodeVars/2+1;l++)
    		newObj.addTerm(1.0, alpha13[l]);
    	
		model.setObjective(newObj, GRB.MINIMIZE);
		model.update();
		
		} catch (GRBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    public void printSolution()
    {

    }
}

