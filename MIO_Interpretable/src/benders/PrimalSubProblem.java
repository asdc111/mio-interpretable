package benders;

import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

import org.ejml.simple.SimpleMatrix;

import gurobi.GRBVar;
import weka.classifiers.trees.REPTree;
import weka.classifiers.trees.REPTree.Tree;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;
import gurobi.GRB;
import gurobi.GRBEnv;
import gurobi.GRBException;
import gurobi.GRBLinExpr;
import gurobi.GRBModel;

//MIP 4 IMPLEMENTATION
public class PrimalSubProblem {
    public GRBEnv    env;
    public GRBModel  model;
    
    public GRBVar x[][], lin[][], z[], tp[], tn[];// fp[], fn[], lin[][];// hplus[], hminus[];
    //public GRBVar yplus[][], yminus[][];
    
    SimpleMatrix train, test;
    public SimpleMatrix  Xtrain, Ytrain, Xtest, Ytest;
    
    int treeDepth, Nmin;
    double interpretabilityWeights[];
    double lambda,lambda2;
    String outputfile;
    
    double wVals[][];
    
    
    static double numerical_instability_epsilon = 0.001;
    
    public PrimalSubProblem(int treeDepth, SimpleMatrix Xtrain, SimpleMatrix Ytrain, double w[][])
    {
    	try
    	{	
    		this.treeDepth = treeDepth;
        	
        	
    		this.Xtrain = Xtrain; 
       		this.Ytrain = Ytrain;
        	
       		wVals = w;
        	
    		
    		env   = new GRBEnv("mip1.log");
    		model = new GRBModel(env);
    	
    		specifyModel();
    	}
    	catch(GRBException e)
    	{
    		System.out.println("Error code: " + e.getErrorCode() + ". " +
                    e.getMessage());
    	}
    }
    
    public void specifyModel()
    {
    	try
    	{
    		int numTreeNodeVars = (int) (Math.pow(2, treeDepth+1) - 1);
    		
    		lin = new GRBVar[Xtrain.numRows()][numTreeNodeVars/2+1];
    		x = new GRBVar[Xtrain.numRows()][numTreeNodeVars/2+1];
    		z = new GRBVar[numTreeNodeVars/2+1];
    		for (int l=0; l<numTreeNodeVars/2+1; l++)
    		{
    			z[l] = model.addVar(0.0, 1.0, 0.0, GRB.CONTINUOUS, "z_"+l);
    			for (int i=0;i<Xtrain.numRows();i++)
    			{
    				x[i][l] = model.addVar(0.0, 1.0, 0.0, GRB.CONTINUOUS, "x_"+i+"_"+l);
    				lin[i][l] = model.addVar(0.0, 1.0, 0.0, GRB.CONTINUOUS, "lin_"+i+"_"+l);
    			}
    		}
    		
    		tp = new GRBVar[Xtrain.numRows()];
    		tn = new GRBVar[Xtrain.numRows()];
    		
    		for (int i=0;i<Xtrain.numRows();i++)
    		{
    			tp[i] = model.addVar(0.0, 1.0, 0.0, GRB.CONTINUOUS, "tp_"+i);
    			tn[i] = model.addVar(0.0, 1.0, 0.0, GRB.CONTINUOUS, "tn_"+i);
    		}
    		
    		model.update();
    		
    		///TO DO:CHECK NUMERICAL
    		GRBLinExpr objf1 = new GRBLinExpr();
    		for (int i=0;i<Xtrain.numRows();i++)
    		{
    			if (Ytrain.get(i)>1-numerical_instability_epsilon)
    				objf1.addTerm(1.0, tp[i]);
    			else if (Ytrain.get(i)<numerical_instability_epsilon)
    				objf1.addTerm(1.0, tn[i]);
    		}
    		model.setObjective(objf1, GRB.MAXIMIZE);
    		
    		
    		///constraints
    		for (int i=0;i<Xtrain.numRows();i++)
    		{
    			//EQ: each datapoint has to be assigned to exactly one leaf
    			GRBLinExpr ex1 = new GRBLinExpr();
    			for (int l=0;l<numTreeNodeVars/2+1;l++)
    				ex1.addTerm(1.0, x[i][l]);
    			
    			//EQUATION 2
    			model.addConstr(ex1, GRB.EQUAL, 1.0, "eq1_"+i);
    		}
    		
    		for (int n=0;n<numTreeNodeVars/2;n++)
    		{    			
    			int rightNode = 2*n+2;
    			ArrayList<Integer> rightIndices = new ArrayList<Integer>();
    			ArrayList<Integer> leftIndices = new ArrayList<Integer>();
    			for (int l=0;l<numTreeNodeVars/2+1;l++)
    			{
    				int currInd = l+numTreeNodeVars/2;
    				while(currInd>rightNode)
    				{
    					if (currInd%2==0)
    						currInd = currInd/2-1;
    					else
    						currInd = currInd/2;
    					
    					if (currInd==rightNode || currInd==rightNode-1)
    						break;
    				}
    				if (currInd==rightNode)
    					rightIndices.add(l);
    				else if (currInd == rightNode - 1)
    					leftIndices.add(l);
    			}
    			
    			for (int i=0;i<Xtrain.numRows();i++)
    			{
    				GRBLinExpr ex6 = new GRBLinExpr();
    				for (int l=0;l<rightIndices.size();l++)
    					ex6.addTerm(1.0, x[i][rightIndices.get(l)]);
    				model.addConstr(ex6, GRB.LESS_EQUAL, 1.0 - wVals[i][n], "eq6_"+n+"_"+i);
    				
    				GRBLinExpr ex7 = new GRBLinExpr();
    				for (int l=0;l<leftIndices.size();l++)
    					ex7.addTerm(1.0, x[i][leftIndices.get(l)]);
    				model.addConstr(ex7, GRB.LESS_EQUAL, wVals[i][n], "eq7_"+n+"_"+i);
    			}
    		}
    		
    		
    		for (int i=0;i<Xtrain.numRows();i++)
    		{
    			for (int l=0;l<numTreeNodeVars/2+1;l++)
    			{
    				GRBLinExpr ex7 = new GRBLinExpr();
    				ex7.addTerm(1.0, lin[i][l]);
    				ex7.addTerm(-1.0, z[l]);
    				model.addConstr(ex7, GRB.LESS_EQUAL, 0.0, "eq7_"+i+"_"+l);
    				
    				GRBLinExpr ex8 = new GRBLinExpr();
    				ex8.addTerm(1.0, lin[i][l]);
    				ex8.addTerm(-1.0, x[i][l]);
    				model.addConstr(ex8, GRB.LESS_EQUAL, 0.0, "eq8_"+i+"_"+l);
    				
    				GRBLinExpr ex9 = new GRBLinExpr();
    				ex9.addTerm(1.0, lin[i][l]);
    				ex9.addTerm(-1.0, x[i][l]);
    				ex9.addTerm(-1.0, z[l]);
    				model.addConstr(ex9, GRB.GREATER_EQUAL, -1.0, "eq9_"+i+"_"+l);
    				//EQUATION 10
    			}
    			
    			
    			////TO DO: CHECK NUMERICAL
    			double val;
    			if (Ytrain.get(i)>1-numerical_instability_epsilon)
    			{
    				GRBLinExpr ex10 = new GRBLinExpr();
    				ex10.addTerm(1.0, tp[i]);
    				
    				for (int l=0;l<numTreeNodeVars/2+1;l++)
        				ex10.addTerm(-1.0, lin[i][l]);
    				model.addConstr(ex10, GRB.EQUAL, 0.0, "eq10_"+i);
    			}
    			
    			else if (Ytrain.get(i)<numerical_instability_epsilon)
    			{
    				GRBLinExpr ex31 = new GRBLinExpr();
    				ex31.addTerm(1.0, tn[i]);
    				
    				for (int l=0;l<numTreeNodeVars/2+1;l++)
    				{	
    					ex31.addTerm(-1.0, x[i][l]);
        				
        				ex31.addTerm(1.0, lin[i][l]);
    				}
    				model.addConstr(ex31, GRB.EQUAL, 0.0, "eq31_"+i);
    			}
    			
    		}
		model.update();
    		
    	}
    	catch(GRBException e)
    	{
    		System.out.println("Error code: " + e.getErrorCode() + ". " +
                    e.getMessage());
    	}
    }
    
    public void printSolution()
    {
    	try
    	{
    	int numTreeNodeVars = (int) (Math.pow(2, treeDepth+1) - 1);
		
    	System.out.println("THE Z Values: ");
		for (int i=0;i<z.length;i++)
			System.out.print(z[i].get(GRB.DoubleAttr.X)+" ");
		System.out.println();

		/*System.out.println("THE X AND XHAT Values: ");
        for (int i=0;i<Xtrain.numRows();i++)
        {
		    System.out.print("Datapoint "+i+"....."); 
            System.out.print(x[i][0].get(GRB.DoubleAttr.X)+", "+ lin[i][0].get(GRB.DoubleAttr.X)+ "   ");
            System.out.print(x[i][1].get(GRB.DoubleAttr.X)+", "+ lin[i][1].get(GRB.DoubleAttr.X)+ "   ");
            System.out.print(x[i][2].get(GRB.DoubleAttr.X)+", "+ lin[i][2].get(GRB.DoubleAttr.X)+ "   ");
            System.out.print(x[i][3].get(GRB.DoubleAttr.X)+", "+ lin[i][3].get(GRB.DoubleAttr.X)+ "   ");
            System.out.println();
            if (Ytrain.get(i)>1-numerical_instability_epsilon)
				System.out.print("TP Val: "+tp[i].get(GRB.DoubleAttr.X));
			else if (Ytrain.get(i)<numerical_instability_epsilon)
				System.out.print("TN Val: "+tn[i].get(GRB.DoubleAttr.X));
            System.out.println();
        }*/
                
    	}
    	catch(GRBException e)
    	{
    		e.printStackTrace();
    	}
    }
}

