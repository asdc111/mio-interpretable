package benders;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

import org.ejml.data.DenseMatrix64F;
import org.ejml.ops.MatrixIO;
import org.ejml.simple.SimpleMatrix;

import gurobi.GRBVar;
import models.Benders_Dtree4;
import models.Dtree4;
import weka.classifiers.trees.REPTree;
import weka.classifiers.trees.REPTree.Tree;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;
import gurobi.GRB;
import gurobi.GRBCallback;
import gurobi.GRBEnv;
import gurobi.GRBException;
import gurobi.GRBLinExpr;
import gurobi.GRBModel;
import gurobi.GRBConstr;


public class Controller extends GRBCallback {
	
	public static SubProblemModel subProb;
	public static MasterModel master;
	
	public static SimpleMatrix Xtrain, Ytrain;
	
	public static int treeDepth, numTreeNodeVars;
	
	public static double wVals[][];
	
	static double numerical_instability_epsilon = 0.001;
	
	public Controller(int Depth, SimpleMatrix train)
	{
		treeDepth = Depth;
		numTreeNodeVars = (int) (Math.pow(2, treeDepth+1) - 1);
        Xtrain = train.extractMatrix(0, train.numRows(), 1, train.numCols()-1);//leave first col..last column for label
 		Ytrain = train.extractVector(false, train.numCols()-1);///last label
 		
 		wVals = new double[Xtrain.numRows()][numTreeNodeVars/2];
		
		master = new MasterModel(Depth, Xtrain, Ytrain);
		subProb = new SubProblemModel(Depth, Xtrain, Ytrain);
	}
	
	public double predict(double a[][], double b[])
	{
		//System.out.println("The non-orthogonal A is:");
		int numTreeNodeVars = (int) (Math.pow(2, treeDepth+1) - 1);
		/*for (int n=0;n<numTreeNodeVars/2;n++)
		{
			System.out.print(b[n]+ " -> ");
			for (int f=0;f<master.Xtrain.numCols();f++)
				System.out.print(a[n][f]+ ", ");
			System.out.println();
		}*/
		
		int leaves[][] = new int[numTreeNodeVars/2+1][2];
		for (int l=0;l<numTreeNodeVars/2+1;l++)
			for (int j=0;j<2;j++)
				leaves[l][j]=0;
		
		int currNode;
		for (int i=0;i<master.Xtrain.numRows();i++)
		{
			currNode=0;
			while(currNode<numTreeNodeVars/2)
			{
				double signAtNode=0;
				for (int f=0;f<master.Xtrain.numCols();f++)
					signAtNode += master.Xtrain.get(i, f)*a[currNode][f];
				signAtNode -= b[currNode];
			
				if (signAtNode<0)
					currNode=2*currNode+1;
				else if (signAtNode>=0)
					currNode=2*currNode+2;
			}
			if (master.Ytrain.get(i)<numerical_instability_epsilon)
				leaves[currNode-numTreeNodeVars/2][0] = leaves[currNode-numTreeNodeVars/2][0] + 1;
			else if (master.Ytrain.get(i)>1-numerical_instability_epsilon)
				leaves[currNode-numTreeNodeVars/2][1] = leaves[currNode-numTreeNodeVars/2][1] + 1;
		}
		
		double correctClassifications=master.Xtrain.numRows();
		for (int l=0;l<numTreeNodeVars/2+1;l++)
		{
			if (leaves[l][0]>leaves[l][1])
				correctClassifications -= leaves[l][1];///false negatives
			else if (leaves[l][0]<=leaves[l][1])
				correctClassifications -= leaves[l][0];//false positives
		}
		return correctClassifications;
	}
	
	@Override
	protected void callback() {
		try
		{
			/*if (where==GRB.Callback.MIPNODE && getIntInfo(GRB.Callback.MIPNODE_STATUS)==GRB.Status.OPTIMAL)
			{	
				int numTreeNodeVars = (int) (Math.pow(2, treeDepth+1) - 1);
				double aVals[][] = new double[numTreeNodeVars/2][master.Xtrain.numCols()];
				double bVals[] = new double[numTreeNodeVars/2];
				aVals = getNodeRel(master.a);
				bVals = getNodeRel(master.b);
				
				int finalN=-2;
	    		for (int n=0;n<numTreeNodeVars/2;n++)
	    		{
	    			boolean isIntegral=true;
	    			for (int f=0;f<master.Xtrain.numCols();f++)
	    			{
	    				if (aVals[n][f]>numerical_instability_epsilon && aVals[n][f]<1-numerical_instability_epsilon)
	    				{
	    					isIntegral=false;
	    					break;
	    				}
	    			}
	    			if (isIntegral==false)
	    			{
	    				finalN=n-1;
	    				break;
	    			}
	    		}
				
	    		//System.out.println("The best bound at this node is: "+getDoubleInfo(GRB.Callback.MIPNODE_OBJBND));
				
	    		if (finalN!=-2)
	    		{
	    			///upper bound
	    			double upperBound = predict(aVals, bVals);
	    			double currentBestObj = getDoubleInfo(GRB.Callback.MIPNODE_OBJBST);
	    			if (upperBound > currentBestObj)
	    			{
	    				System.out.println("The upper bound added on t is: "+upperBound);
	    				GRBLinExpr cut1 = new GRBLinExpr();
	    				cut1.addTerm(1.0, master.t);
	    				addCut(cut1, GRB.LESS_EQUAL, upperBound);
	    			}
	    			//upper bound end
	    		}
	    		
			}*/
			if (where == GRB.Callback.MIPSOL)
			{
				System.out.println("FOUND NEW INTEGRAL SOLUTION.");
				double masterObj = getDoubleInfo(GRB.Callback.MIPSOL_OBJ);
				wVals = getSolution(master.w);///contains integer
				/*for (int n=0;n<numTreeNodeVars/2;n++)
				{
					System.out.print("Node "+n+ " "+getSolution(master.b[n])+" --> ");
					for (int f=0;f<Xtrain.numCols();f++)
						System.out.print(getSolution(master.a[n][f])+ " ");
					System.out.println();
				}
				for (int i=0;i<Xtrain.numRows();i++)
				{
					System.out.print("For datapoint "+i+": ");
					for (int n=0;n<numTreeNodeVars/2;n++)
						System.out.print(wVals[i][n] + " ");
					System.out.println();
				}*/
				System.out.println("Integral solution found: ");
				for (int n=0;n<numTreeNodeVars/2;n++)
				{
					System.out.print("Node " + n+" : ");
					for (int f=0;f<Xtrain.numCols();f++)
					{
						double val = getSolution(master.a[n][f]);
						if (val>1-numerical_instability_epsilon)
						{
							System.out.print("a= "+ (f+1));
							break;
						}
					}
					System.out.println(" b= "+getSolution(master.b[n]));
				}
				
				subProb.updateSubProblemObjective(wVals);
				//subProb.model.getEnv().set(GRB.IntParam.Method, 0);
				System.out.println("SOLVING SUBPROBLEM IN ITERATION "+getIntInfo(GRB.Callback.MIPSOL_SOLCNT)+"...");
				subProb.model.write("/Users/amulyayadav/Dropbox/Work/PostAAMAS/PhebeWork/Bottleneck/"+"dualLP-model.lp");
				subProb.model.optimize();
				
				if (subProb.model.get(GRB.IntAttr.Status) == GRB.Status.OPTIMAL)
				{
					double subproblemObj = subProb.model.get(GRB.DoubleAttr.ObjVal);
					System.out.println("Master prob Obj: "+ masterObj + " Subproblem Obj: "+ subproblemObj);
					if (subproblemObj>=masterObj)
					{
						System.out.println("ACCEPT SOLUTION!!!");
						System.out.println("The optimal tree formed is as follows:");
						for (int n=0;n<numTreeNodeVars/2;n++)
						{
							System.out.print("Node " + n+" : ");
							for (int f=0;f<Xtrain.numCols();f++)
							{
								double val = getSolution(master.a[n][f]);
								if (val>1-numerical_instability_epsilon)
								{
									System.out.print("a= "+ (f+1));
									break;
								}
							}
							System.out.println(" b= "+getSolution(master.b[n]));
						}
						return;
					}
					else if (subproblemObj<masterObj)//add optimality cut
					{
				    	System.out.println("Adding optimality cut.");
				    	GRBLinExpr optCut = new GRBLinExpr();
				    	double constantTerm=0.0;
				    	for (int i=0;i<Xtrain.numRows();i++)
				    	{
				    		constantTerm += subProb.alpha1[i].get(GRB.DoubleAttr.X);
				    		if (Ytrain.get(i)>1-numerical_instability_epsilon)
				    			constantTerm +=  subProb.alpha11[i].get(GRB.DoubleAttr.X); 
				    		if (Ytrain.get(i)<numerical_instability_epsilon)
				    			constantTerm +=  subProb.alpha12[i].get(GRB.DoubleAttr.X);
				    		
				    		for (int n=0;n<numTreeNodeVars/2;n++)
				    		{
				    			constantTerm += subProb.alpha2[i][n].get(GRB.DoubleAttr.X);
				    			
				    			optCut.addTerm(-1*subProb.alpha2[i][n].get(GRB.DoubleAttr.X), master.w[i][n]);
				    			optCut.addTerm(subProb.alpha3[i][n].get(GRB.DoubleAttr.X), master.w[i][n]);
				    		}
				    		
				    		for (int l=0;l<numTreeNodeVars/2+1;l++)
				    		{
				    			constantTerm += (subProb.alpha4[i][l].get(GRB.DoubleAttr.X) + subProb.alpha9[i][l].get(GRB.DoubleAttr.X) + subProb.alpha10[i][l].get(GRB.DoubleAttr.X));
					    	}
				    	}
				    	
				    	for (int l=0;l<numTreeNodeVars/2+1;l++)
				    	{
				    		constantTerm += subProb.alpha13[l].get(GRB.DoubleAttr.X);
				    	}
				    	
				    	optCut.addTerm(-1.0, master.t);
				    	System.out.println(optCut.toString() +" >= "+-1*constantTerm);
				    	addLazy(optCut, GRB.GREATER_EQUAL, -1*constantTerm);
					}
				}
				else if (subProb.model.get(GRB.IntAttr.Status)== GRB.Status.UNBOUNDED)
				{
					//DEBUG: Check source of infeasibility
					/*PrimalSubProblem primalprob = new PrimalSubProblem(treeDepth, Xtrain, Ytrain, wVals);
					primalprob.model.optimize();
					if (primalprob.model.get(GRB.IntAttr.Status)==GRB.Status.INFEASIBLE)
					{
						System.out.println("Aleast the dual is correct");
						System.out.println("The model is infeasible; computing IIS");
					      primalprob.model.computeIIS();
					      System.out.println("\nThe following constraint(s) "
					          + "cannot be satisfied:");
					      for (GRBConstr c : primalprob.model.getConstrs()) {
					        if (c.get(GRB.IntAttr.IISConstr) == 1) {
					          System.out.println(c.get(GRB.StringAttr.ConstrName));
					        }
					      }
					}
					else
					{
						System.out.println("SOMETHING WRONG IN THE DUAL");
						System.out.println("CHECKING INTEGRALITY OF SOLUTION...");
						primalprob.printSolution();
					}*/
					///
					
					//add feasibility cut
					System.out.println("Adding feasibility cut.");
			    	GRBLinExpr feasCut = new GRBLinExpr();
			    	double constantTerm=0.0;
			    	for (int i=0;i<Xtrain.numRows();i++)
			    	{
			    		constantTerm += subProb.alpha1[i].get(GRB.DoubleAttr.UnbdRay);
			    		if (Ytrain.get(i)>1-numerical_instability_epsilon)
			    			constantTerm +=  subProb.alpha11[i].get(GRB.DoubleAttr.UnbdRay); 
			    		if (Ytrain.get(i)<numerical_instability_epsilon)
			    			constantTerm +=  subProb.alpha12[i].get(GRB.DoubleAttr.UnbdRay);
			    		
			    		for (int n=0;n<numTreeNodeVars/2;n++)
			    		{
			    			constantTerm += subProb.alpha2[i][n].get(GRB.DoubleAttr.UnbdRay);
			    			
			    			feasCut.addTerm(-1*subProb.alpha2[i][n].get(GRB.DoubleAttr.UnbdRay), master.w[i][n]);
			    			feasCut.addTerm(subProb.alpha3[i][n].get(GRB.DoubleAttr.UnbdRay), master.w[i][n]);
			    		}
			    		
			    		for (int l=0;l<numTreeNodeVars/2+1;l++)
				    	{
			    			constantTerm += (subProb.alpha4[i][l].get(GRB.DoubleAttr.UnbdRay) + subProb.alpha9[i][l].get(GRB.DoubleAttr.UnbdRay) + subProb.alpha10[i][l].get(GRB.DoubleAttr.UnbdRay));
				    	}
			    	}
			    	
			    	for (int l=0;l<numTreeNodeVars/2+1;l++)
			    	{
			    		constantTerm += subProb.alpha13[l].get(GRB.DoubleAttr.UnbdRay);
			    	}
			    	System.out.println(feasCut.toString() +" >= "+-1*constantTerm);
			    	addLazy(feasCut, GRB.GREATER_EQUAL, -1*constantTerm);
				}
				System.out.println("Callback finished. Resume processing MIP branch and bound tree.");
			}
		}
		catch(GRBException e)
		{
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		
	}
	

	
	public static void main(String args[])
	{
		String basedir = "/Users/amulyayadav/Dropbox/Work/PostAAMAS/PhebeWork/Bottleneck/";
	 	int Depth=2;
	 	double timeLimit = 600;
	 	
	 	
	 
	 	
	 	DenseMatrix64F dataset;
	    SimpleMatrix originalData, data, train, test;
	    
        try
        {
            
            /*String basedir = args[0];
            int Depth=Integer.parseInt(args[1]);
            double timeLimit = Double.parseDouble(args[2]);*/
            
        	
            
            dataset = MatrixIO.loadCSV(basedir+"masterdataset-mip.csv");
        	//dataset = MatrixIO.loadCSV(basedir+"BinaryDatasets/german_numer/german_numer_normalized_pres2.csv");

            originalData = SimpleMatrix.wrap(dataset);


            data = originalData;


            train = data;
            ////train and test constructed
            
            
            ///running code
    		Controller cb = new Controller(Depth, train);
    		
    		cb.subProb.model.set(GRB.IntParam.InfUnbdInfo, 1);
    		cb.subProb.model.set(GRB.IntParam.OutputFlag, 0);
    		
    		//cb.master.model.set(GRB.IntParam.OutputFlag, 0);
    	    //cb.master.model.set(GRB.DoubleParam.Heuristics, 0.0);
    		cb.master.model.set(GRB.IntParam.LazyConstraints, 1);
    		cb.master.model.getEnv().set(GRB.IntParam.Method, 0);
            cb.master.model.getEnv().set(GRB.IntParam.BranchDir, 1);
            cb.master.model.getEnv().set(GRB.IntParam.NodeMethod, 0);
            cb.master.model.getEnv().set(GRB.IntParam.PreCrush, 1);
            cb.master.model.getEnv().set(GRB.IntParam.MIPFocus, 3);//3 works best
            
            cb.master.model.getEnv().set(GRB.DoubleParam.TimeLimit, timeLimit);
            
    		cb.master.model.setCallback(cb);
    		//cb.master.provideWarmStartCART(basedir+"BinaryDatasets/german_numer/german_numer_weka.csv");
    		cb.master.provideWarmStartCART(basedir+"masterdataset-mip-weka.csv");
            
            //cb.master.model.write(basedir+"model.lp");
            
    		System.out.println("STARTING MASTER PROBLEM...");
    		cb.master.model.optimize();
    		
    		System.out.println("Solution Quality: "+cb.master.model.get(GRB.DoubleAttr.ObjVal));
    		System.out.println("Runtime: "+cb.master.model.get(GRB.DoubleAttr.Runtime));
    		cb.master.printSolution();
            
        }
        catch(GRBException e)
        {
        	e.printStackTrace();
        }
        catch(IOException e)
        {
        	e.printStackTrace();
        }
            
	}
}
