package benders;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

import org.ejml.simple.SimpleMatrix;

import benders.MasterModel.TreeNode;
import gurobi.GRB;
import gurobi.GRBEnv;
import gurobi.GRBException;
import gurobi.GRBLinExpr;
import gurobi.GRBModel;
import gurobi.GRBVar;
import weka.classifiers.trees.REPTree;
import weka.classifiers.trees.REPTree.Tree;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;

public class MasterModel{

public GRBEnv    env;
public GRBModel  model;



public GRBVar t;
public GRBVar a[][], b[], w[][];

SimpleMatrix train, test;
SimpleMatrix  Xtrain, Ytrain, Xtest, Ytest;

int treeDepth, Nmin;
double interpretabilityWeights[];
double lambda,lambda2;
String outputfile;

double wVals[][];

double smallM[];

double smallMVal;

double numerical_instability_epsilon = 0.001;

public MasterModel(int treeDepth, SimpleMatrix Xtrain, SimpleMatrix Ytrain)
{
	try
	{	
		this.treeDepth = treeDepth;
    
		
   		this.Xtrain = Xtrain;
   		this.Ytrain = Ytrain;
		
		int numTreeNodeVars = (int) (Math.pow(2, treeDepth+1) - 1);
		
		wVals = new double[Xtrain.numRows()][numTreeNodeVars/2];
	
		smallM = new double[Xtrain.numCols()];
		
		env   = new GRBEnv("mip1.log");
		model = new GRBModel(env);
	
		specifyModel();
	}
	catch(GRBException e)
	{
		System.out.println("Error code: " + e.getErrorCode() + ". " +
                e.getMessage());
	}
}

public void specifyModel()
{
	try
	{
		int numTreeNodeVars = (int) (Math.pow(2, treeDepth+1) - 1);
		
		
		double epsilon[] = new double[Xtrain.numCols()];
		double feature[] = new double[Xtrain.numRows()];
		double max_epsilon_entry=-1;
		int min_epsilon_index=-1;
		double min_epsilon_entry=10000;
		for (int i=0;i<Xtrain.numCols();i++)
		{
			for (int j=0;j<Xtrain.numRows();j++)
				feature[j] = Xtrain.get(j, i);
			Arrays.sort(feature);
			double minVal=10000;
			for (int j=feature.length-1;j>0;j--)
			{
				double temp = feature[j]-feature[j-1];
				if (temp!=0 && temp<minVal)
					minVal = temp;
			}
			epsilon[i] = minVal;
			if (epsilon[i]<min_epsilon_entry)
			{
				min_epsilon_index = i;
				min_epsilon_entry = epsilon[i];
			}
			if (epsilon[i]>max_epsilon_entry)
				max_epsilon_entry = epsilon[i];
		}
		for (int i=0;i<Xtrain.numCols();i++)
			smallM[i] = epsilon[i];
		smallMVal = min_epsilon_entry;
		//System.out.println("Min epsilon"+ min_epsilon_entry + " "+min_epsilon_index);
		
		t = model.addVar(-GRB.INFINITY, Xtrain.numRows(), 0.0, GRB.CONTINUOUS, "t");
		
		a = new GRBVar[numTreeNodeVars/2][Xtrain.numCols()];
		b = new GRBVar[numTreeNodeVars/2];
		
		for (int n=0;n<numTreeNodeVars/2;n++)
		{	
			b[n] = model.addVar(0.0, 1.0, 0.0, GRB.CONTINUOUS, "b_"+n);
			for (int f=0;f<Xtrain.numCols();f++)
			{
				a[n][f] = model.addVar(0.0, 1.0, 0.0, GRB.BINARY, "a_"+n+"_"+f);
			}
		}
		
		
		w = new GRBVar[Xtrain.numRows()][numTreeNodeVars/2];
		for (int i=0;i<Xtrain.numRows();i++)
		{
			for (int n=0;n<numTreeNodeVars/2;n++)
			{
				w[i][n] = model.addVar(0.0, 1.0, 0.0, GRB.BINARY, "w_"+i+"_"+n);
			}
		}
		
		
		model.update();


		/*int branch_priority = Integer.MAX_VALUE;
		for (int n=0;n<numTreeNodeVars/2;n++)
		{	
			for (int f=0;f<Xtrain.numCols();f++)
			{
				a[n][f].set(GRB.IntAttr.BranchPriority, branch_priority);
			}
			branch_priority--;
		}
		

		
		for (int n=0;n<numTreeNodeVars/2;n++)
		{	
			for (int i=0;i<Xtrain.numRows();i++)
			{
			
				w[i][n].set(GRB.IntAttr.BranchPriority, branch_priority);
			}
			branch_priority--;
		}*/
		
		//objective
		GRBLinExpr objf1 = new GRBLinExpr();
		objf1.addTerm(1.0, t);
		model.setObjective(objf1, GRB.MAXIMIZE);
		
		
		///constraints
		
		for (int n=0;n<numTreeNodeVars/2;n++)
		{
			///EQ: at each branch node...branch only on one variable
			GRBLinExpr ex2 = new GRBLinExpr();
			for (int f=0;f<Xtrain.numCols();f++)
			{
				ex2.addTerm(1.0, a[n][f]);
			}
			///EQUATION 3
			model.addConstr(ex2, GRB.EQUAL, 1.0, "eq2_"+n);

			
			for (int i=0;i<Xtrain.numRows();i++)
			{
				GRBLinExpr ex3 = new GRBLinExpr();
				for (int f=0;f<Xtrain.numCols();f++)
					ex3.addTerm(Xtrain.get(i, f) + smallM[f], a[n][f]);
				ex3.addTerm(-1.0, b[n]);
				
				GRBLinExpr ex35 = new GRBLinExpr();
				for (int f=0;f<Xtrain.numCols();f++)
					ex35.addTerm(Xtrain.get(i, f) , a[n][f]);
				ex35.addTerm(-1.0, b[n]);
				model.addGenConstrIndicator(w[i][n], 1, ex3, GRB.LESS_EQUAL, 0.0, "eq_indic1_"+i+"_"+n);
				model.addGenConstrIndicator(w[i][n], 0, ex35, GRB.GREATER_EQUAL, 0.0, "eq_indic2_"+i+"_"+n);
			}	
		}
		
		/*for (int n=0;n<numTreeNodeVars/2;n++)
		{
			ArrayList<Integer> rightIndices = new ArrayList<Integer>();
			ArrayList<Integer> leftIndices = new ArrayList<Integer>();
			
			int leftChild = 2*n+1;
			int rightChild = 2*n+2;
			
			if (leftChild<numTreeNodeVars/2)
				leftIndices.add(leftChild);
			if (rightChild<numTreeNodeVars/2)
				rightIndices.add(rightChild);
			
			int index=0;
			while(index<leftIndices.size())
			{
				int element = leftIndices.get(index);
				if (2*element+1<numTreeNodeVars/2)
					leftIndices.add(2*element+1);
				if (2*element+2<numTreeNodeVars/2)
					leftIndices.add(2*element+2);
				index++;
			}
			
			index=0;
			while(index<rightIndices.size())
			{
				int element = rightIndices.get(index);
				if (2*element+1<numTreeNodeVars/2)
					rightIndices.add(2*element+1);
				if (2*element+2<numTreeNodeVars/2)
					rightIndices.add(2*element+2);
				index++;
			}
			
			for (int i=0;i<Xtrain.numRows();i++)
			{
				for (int nprime=0;nprime<rightIndices.size();nprime++)
				{
					GRBLinExpr constraint12_1 = new GRBLinExpr();
					constraint12_1.addTerm(1.0, w[i][n]);
					constraint12_1.addTerm(1.0, w[i][rightIndices.get(nprime)]);
					model.addConstr(constraint12_1, GRB.LESS_EQUAL, 1.0, "eq12^1_"+i+"_"+n+"_"+nprime);
					//EQUATION 12_1
				}
				
				for (int nprime=0;nprime<leftIndices.size();nprime++)
				{
					GRBLinExpr constraint12_2 = new GRBLinExpr();
					constraint12_2.addTerm(1.0, w[i][n]);
					constraint12_2.addTerm(-1.0, w[i][leftIndices.get(nprime)]);
					model.addConstr(constraint12_2, GRB.GREATER_EQUAL, 0.0, "eq12^2_"+i+"_"+n+"_"+nprime);
					//EQUATION 12_2
				}
			}
		}*/
	model.update();	
	}
	catch(GRBException e)
	{
		System.out.println("Error code: " + e.getErrorCode() + ". " +
                e.getMessage());
	}
}

public void printSolution()
{
	try
	{
	int numTreeNodeVars = (int) (Math.pow(2, treeDepth+1) - 1);
	for (int n=0;n<b.length;n++)
	{
		System.out.print("Node "+n+" : ");
		for (int f=0;f<Xtrain.numCols();f++)
		{
			if (a[n][f].get(GRB.DoubleAttr.X)>1-numerical_instability_epsilon)
			{
				System.out.println("a is "+(f+1)+" | b is "+b[n].get(GRB.DoubleAttr.X));
			}
		}
	}

            
	}
	catch(GRBException e)
	{
		e.printStackTrace();
	}
}

public void provideWarmStartCART(String filename)
{
	
	try 
	{
		DataSource source = new DataSource(filename);
		Instances data = source.getDataSet();
		if (data.classIndex() == -1)
			   data.setClassIndex(data.numAttributes() - 1);
		data.deleteAttributeAt(0);
	
		
		REPTree tree = new REPTree();
		String[] options = weka.core.Utils.splitOptions("-M "+Nmin+" -V 0.001 -N 3 -S 1 -L "+treeDepth+" -P -I 0.0");
		//String[] options = weka.core.Utils.splitOptions("-M "+Nmin+" -L "+treeDepth);
		tree.setOptions(options);
		tree.buildClassifier(data);
		
		System.out.println(tree.toString());
		
		Tree root = tree.m_Tree;
		
		TreeNode rootNode = new TreeNode(root, null);
		
		Queue queue = new LinkedList();
		queue.add(rootNode);
		
		
		int index=0;
		while(index<b.length && !queue.isEmpty())
		{
			TreeNode treenode = (TreeNode)queue.remove();
			Tree node = treenode.node;
			Tree parent = treenode.parentNode;
			if (node.m_Attribute==-1)	
			{
				b[index].set(GRB.DoubleAttr.Start, parent.m_SplitPoint);
			}
			else
			{
				b[index].set(GRB.DoubleAttr.Start, node.m_SplitPoint);
			}
			int tamp = node.m_Attribute;
			if (tamp==-1)///need to revisit
				tamp = parent.m_Attribute;
			for (int j=0;j<Xtrain.numCols();j++)
			{
				
				if (j==tamp)
					a[index][j].set(GRB.DoubleAttr.Start, 1.0);
				else
					a[index][j].set(GRB.DoubleAttr.Start, 0.0);
			}
			index++;
			
			//add children
			if (node.m_Attribute!=-1)
			{
				for (int j=0;j<node.m_Successors.length;j++)
				{
					Tree successor = node.m_Successors[j];
					Tree parentVal = node;
					TreeNode child = new TreeNode(successor, parentVal);
					queue.add(child);
				}
			}
			else//construct binary tree
			{
				for (int j=0;j<2;j++)
				{
					Tree successor = tree.dummyTree;
					Tree parentVal = parent;
					TreeNode child = new TreeNode(successor, parentVal);
					queue.add(child);
				}
			}
		}
	} 
	catch (Exception e) 
	{
		e.printStackTrace();
	}
}

class TreeNode
{
	public Tree node;
	public Tree parentNode;
	
	public TreeNode(Tree node, Tree parentNode)
	{
		this.node = node;
		this.parentNode = parentNode;
	}
}

}