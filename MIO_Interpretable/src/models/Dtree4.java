package models;

import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

import org.ejml.simple.SimpleMatrix;

import gurobi.GRBVar;
import models.NewDecisionTree_Revamped.TreeNode;
import weka.classifiers.trees.REPTree;
import weka.classifiers.trees.REPTree.Tree;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;
import gurobi.GRB;
import gurobi.GRBEnv;
import gurobi.GRBException;
import gurobi.GRBLinExpr;
import gurobi.GRBModel;

//MIP 4 IMPLEMENTATION
public class Dtree4 {
    public GRBEnv    env;
    public GRBModel  model;
    
    public GRBVar x[][], a[][], b[], z[], w[][], tp[], tn[];// fp[], fn[], lin[][];// hplus[], hminus[];
    //public GRBVar yplus[][], yminus[][];
    public GRBVar lin[][];
    
    SimpleMatrix train, test;
    public SimpleMatrix  Xtrain, Ytrain, Xtest, Ytest;
    
    int treeDepth, Nmin;
    double interpretabilityWeights[];
    double lambda,lambda2;
    String outputfile;
    
    static double smallM[];
    
    static double smallMVal;
    
    static double numerical_instability_epsilon = 0.001;
    
    public Dtree4(int treeDepth, SimpleMatrix train)
    {
    	try
    	{	
    		
    		this.treeDepth = treeDepth;
        	
        	
        	
        	this.train = train;
       		Xtrain = train.extractMatrix(0, train.numRows(), 0, train.numCols()-1);//leave first col..last column for label
        	//Xtrain = train.extractMatrix(0, train.numRows(), 1, 5);
    		Ytrain = train.extractVector(false, train.numCols()-1);///last label
    	
    		smallM = new double[Xtrain.numCols()];
    		
    		env   = new GRBEnv("mip1.log");
    		model = new GRBModel(env);
    	
    		specifyModel();
    	}
    	catch(GRBException e)
    	{
    		System.out.println("Error code: " + e.getErrorCode() + ". " +
                    e.getMessage());
    	}
    }
    
    public void specifyModel()
    {
    	try
    	{
    		int numTreeNodeVars = (int) (Math.pow(2, treeDepth+1) - 1);
    		/*System.out.println("Chilgoza");
    		for (int i=0;i<Xtrain.numRows();i++)
    		{
    			double min=10000;
    			for (int f=0;f<Xtrain.numCols();f++)
    			{
    				double diffVector[] = new double[Xtrain.numRows()];
    				for (int j=0;j<diffVector.length;j++)
    					diffVector[j] = Xtrain.get(i, f) - Xtrain.get(j,f);
    				
    				double minElement = 10000;
    				for (int j=0;j<diffVector.length;j++)
    				{
    					if (j==i)
    						continue;
    					else if (diffVector[j] < minElement)
    						minElement = diffVector[j];
    				}
    				if (minElement < min)
    					min = minElement;
    			}
    			smallM[i] = min;
    		}*/
    		
    		
    		double epsilon[] = new double[Xtrain.numCols()];
    		double feature[] = new double[Xtrain.numRows()];
    		double max_epsilon_entry=-1;
    		int min_epsilon_index=-1;
    		double min_epsilon_entry=10000;
    		for (int i=0;i<Xtrain.numCols();i++)
    		{
    			for (int j=0;j<Xtrain.numRows();j++)
    				feature[j] = Xtrain.get(j, i);
    			Arrays.sort(feature);
    			double minVal=10000;
    			for (int j=feature.length-1;j>0;j--)
    			{
    				double temp = feature[j]-feature[j-1];
    				if (temp!=0 && temp<minVal)
    					minVal = temp;
    			}
    			epsilon[i] = minVal;
    			if (epsilon[i]<min_epsilon_entry)
    			{
    				min_epsilon_index = i;
    				min_epsilon_entry = epsilon[i];
    			}
    			if (epsilon[i]>max_epsilon_entry)
    				max_epsilon_entry = epsilon[i];
    		}
    		for (int i=0;i<Xtrain.numCols();i++)
    			smallM[i] = epsilon[i];
    		smallMVal = min_epsilon_entry;
    		//System.out.println("Min epsilon"+ min_epsilon_entry + " "+min_epsilon_index);
    		
    		a = new GRBVar[numTreeNodeVars/2][Xtrain.numCols()];
    		b = new GRBVar[numTreeNodeVars/2];
    		
    		for (int n=0;n<numTreeNodeVars/2;n++)
    		{	
    			b[n] = model.addVar(0.0, 1.0, 0.0, GRB.CONTINUOUS, "b_"+n);
    			for (int f=0;f<Xtrain.numCols();f++)
    			{
    				a[n][f] = model.addVar(0.0, 1.0, 0.0, GRB.BINARY, "a_"+n+"_"+f);
    			}
    		}
    		
    		
    		lin = new GRBVar[Xtrain.numRows()][numTreeNodeVars/2+1];
    		x = new GRBVar[Xtrain.numRows()][numTreeNodeVars/2+1];
    		z = new GRBVar[numTreeNodeVars/2+1];
    		for (int l=0; l<numTreeNodeVars/2+1; l++)
    		{
    			z[l] = model.addVar(0.0, 1.0, 0.0, GRB.BINARY, "z_"+l);
    			for (int i=0;i<Xtrain.numRows();i++)
    			{
    				x[i][l] = model.addVar(0.0, 1.0, 0.0, GRB.CONTINUOUS, "x_"+i+"_"+l);
    				lin[i][l] = model.addVar(0.0, 1.0, 0.0, GRB.CONTINUOUS, "lin_"+i+"_"+l);
    			}
    		}
    		
    		/*yplus = new GRBVar[Xtrain.numRows()][numTreeNodeVars/2];
    		yminus = new GRBVar[Xtrain.numRows()][numTreeNodeVars/2];
    		for (int i=0;i<Xtrain.numRows();i++)
    		{
    			for (int n=0;n<numTreeNodeVars/2;n++)
    			{
    				yplus[i][n] = model.addVar(0.0, 1.0, 0.0, GRB.CONTINUOUS, "yplus_"+i+"_"+n);
    				yminus[i][n] = model.addVar(0.0, 1.0, 0.0, GRB.CONTINUOUS, "yminus_"+i+"_"+n);
    			}
    		}*/
    		
    		


    		
    		
    		w = new GRBVar[Xtrain.numRows()][numTreeNodeVars/2];
    		for (int i=0;i<Xtrain.numRows();i++)
			{
    			for (int n=0;n<numTreeNodeVars/2;n++)
    			{
    				w[i][n] = model.addVar(0.0, 1.0, 0.0, GRB.BINARY, "w_"+i+"_"+n);
    			}
    		}
    		
    		tp = new GRBVar[Xtrain.numRows()];
    		tn = new GRBVar[Xtrain.numRows()];
    		
    		for (int i=0;i<Xtrain.numRows();i++)
    		{
    			tp[i] = model.addVar(0.0, 1.0, 0.0, GRB.CONTINUOUS, "tp_"+i);
    			tn[i] = model.addVar(0.0, 1.0, 0.0, GRB.CONTINUOUS, "tn_"+i);
    		}
    		
    		model.update();


    		
    		int branch_priority = Integer.MAX_VALUE;
    		/*for (int i=0;i<Xtrain.numRows();i++)
    		{
    			for (int l=0;l<numTreeNodeVars/2+1;l++)
    			{
    				x[i][l].set(GRB.IntAttr.BranchPriority, branch_priority);
    			}
    		}*/
    		
    		for (int n=0;n<numTreeNodeVars/2;n++)
    		{	
    			for (int f=0;f<Xtrain.numCols();f++)
    			{
    				a[n][f].set(GRB.IntAttr.BranchPriority, branch_priority);
    			}
    			branch_priority--;
    		}
    		

    		
    		for (int n=0;n<numTreeNodeVars/2;n++)
    		{	
    			for (int i=0;i<Xtrain.numRows();i++)
    			{
    			
    				w[i][n].set(GRB.IntAttr.BranchPriority, branch_priority);
    			}
    			branch_priority--;
    		}
    		
    		///TO DO:CHECK NUMERICAL
    		GRBLinExpr objf1 = new GRBLinExpr();
    		for (int i=0;i<Xtrain.numRows();i++)
    		{
    			if (Ytrain.get(i)>1-numerical_instability_epsilon)
    				objf1.addTerm(1.0, tp[i]);
    			else if (Ytrain.get(i)<numerical_instability_epsilon)
    				objf1.addTerm(1.0, tn[i]);
    		}
    		model.setObjective(objf1, GRB.MAXIMIZE);
    		
    		
    		///constraints
    		for (int i=0;i<Xtrain.numRows();i++)
    		{
    			//EQ: each datapoint has to be assigned to exactly one leaf
    			GRBLinExpr ex1 = new GRBLinExpr();
    			for (int l=0;l<numTreeNodeVars/2+1;l++)
    				ex1.addTerm(1.0, x[i][l]);
    			
    			//EQUATION 2
    			model.addConstr(ex1, GRB.EQUAL, 1.0, "eq1_"+i);
    		}
    		
    		for (int n=0;n<numTreeNodeVars/2;n++)
    		{
    			///EQ: at each branch node...branch only on one variable
    			GRBLinExpr ex2 = new GRBLinExpr();
    			for (int f=0;f<Xtrain.numCols();f++)
    			{
    				ex2.addTerm(1.0, a[n][f]);
    			}
    			///EQUATION 3
    			model.addConstr(ex2, GRB.EQUAL, 1.0, "eq2_"+n);
    			
    			
    			int rightNode = 2*n+2;
    			ArrayList<Integer> rightIndices = new ArrayList<Integer>();
    			ArrayList<Integer> leftIndices = new ArrayList<Integer>();
    			for (int l=0;l<numTreeNodeVars/2+1;l++)
    			{
    				int currInd = l+numTreeNodeVars/2;
    				while(currInd>rightNode)
    				{
    					if (currInd%2==0)
    						currInd = currInd/2-1;
    					else
    						currInd = currInd/2;
    					
    					if (currInd==rightNode || currInd==rightNode-1)
    						break;
    				}
    				if (currInd==rightNode)
    					rightIndices.add(l);
    				else if (currInd == rightNode - 1)
    					leftIndices.add(l);
    			}
    			
    			for (int i=0;i<Xtrain.numRows();i++)
    			{
    				GRBLinExpr ex6 = new GRBLinExpr();
    				for (int l=0;l<rightIndices.size();l++)
    					ex6.addTerm(1.0, x[i][rightIndices.get(l)]);
    				ex6.addTerm(1.0, w[i][n]);
    				model.addConstr(ex6, GRB.LESS_EQUAL, 1.0, "eq6_"+n+"_"+i);
    				
    				GRBLinExpr ex7 = new GRBLinExpr();
    				for (int l=0;l<leftIndices.size();l++)
    					ex7.addTerm(1.0, x[i][leftIndices.get(l)]);
    				ex7.addTerm(-1.0, w[i][n]);
    				model.addConstr(ex7, GRB.LESS_EQUAL, 0.0, "eq7_"+n+"_"+i);
    				
    				GRBLinExpr ex3 = new GRBLinExpr();
    				for (int f=0;f<Xtrain.numCols();f++)
    					ex3.addTerm(Xtrain.get(i, f) + smallM[f], a[n][f]);
    				ex3.addTerm(-1.0, b[n]);
    				
    				GRBLinExpr ex35 = new GRBLinExpr();
    				for (int f=0;f<Xtrain.numCols();f++)
    					ex35.addTerm(Xtrain.get(i, f) , a[n][f]);
    				ex35.addTerm(-1.0, b[n]);
    				model.addGenConstrIndicator(w[i][n], 1, ex3, GRB.LESS_EQUAL, 0.0, "eq_indic1_"+i+"_"+n);
    				model.addGenConstrIndicator(w[i][n], 0, ex35, GRB.GREATER_EQUAL, 0.0, "eq_indic2_"+i+"_"+n);
    			}
    			
    			/*for (int i=0;i<Xtrain.numRows();i++)
    			{	
    				for (int l=0;l<rightIndices.size();l++)
    				{
    					GRBLinExpr ex6 = new GRBLinExpr();
    					ex6.addTerm(1.0, x[i][rightIndices.get(l)]);
    					ex6.addTerm(1.0, w[i][n]);
    					///EQUATION 4
    					model.addConstr(ex6, GRB.LESS_EQUAL, 1.0, "eq6_"+n+"_"+i+"_"+l);
    				}
    				
    				for (int l=0;l<leftIndices.size();l++)
    				{
    					GRBLinExpr ex12 = new GRBLinExpr();
    					ex12.addTerm(1.0, x[i][leftIndices.get(l)]);
    					ex12.addTerm(-1.0, w[i][n]);
    					///EQUATION 5
    					model.addConstr(ex12, GRB.LESS_EQUAL, 0.0, "eq12_"+n+"_"+i+"_"+l);
    				}
    				
    				GRBLinExpr ex11 = new GRBLinExpr();
    				ex11.addTerm(1.0, yminus[i][n]);
    				ex11.addTerm(1.0, yplus[i][n]);
    				ex11.addTerm(smallMVal, w[i][n]);
    				///EQUATION 6
    				model.addConstr(ex11, GRB.GREATER_EQUAL, smallMVal, "eq11_"+i+"_"+n);
    				
    				GRBLinExpr ex3 = new GRBLinExpr();
    				for (int f=0;f<Xtrain.numCols();f++)
    					ex3.addTerm(Xtrain.get(i, f), a[n][f]);
    				
    				ex3.addTerm(-1.0, b[n]);
    				ex3.addTerm(1.0, yplus[i][n]);
    				ex3.addTerm(-1.0, yminus[i][n]);
    				///EQUATION 7
    				model.addConstr(ex3, GRB.EQUAL, 0.0, "eq3_"+i+"_"+n);
    				
    				GRBLinExpr ex4 = new GRBLinExpr();
    				ex4.addTerm(1.0, yplus[i][n]);
    				ex4.addTerm(-1.0, w[i][n]);
    				///EQUATION 8
    				model.addConstr(ex4, GRB.LESS_EQUAL, 0.0, "eq4_"+i+"_"+n);
    				
    				GRBLinExpr ex5 = new GRBLinExpr();
    				ex5.addTerm(1.0, yminus[i][n]);
    				ex5.addTerm(1.0, w[i][n]);
    				///EQUATION 9
    				model.addConstr(ex5, GRB.LESS_EQUAL, 1.0, "eq5_"+i+"_"+n);
    			}*/
    		
    			
    		}
    		
    		
    		for (int i=0;i<Xtrain.numRows();i++)
    		{
    			for (int l=0;l<numTreeNodeVars/2+1;l++)
    			{
    				GRBLinExpr ex7 = new GRBLinExpr();
    				ex7.addTerm(1.0, lin[i][l]);
    				ex7.addTerm(-1.0, z[l]);
    				model.addConstr(ex7, GRB.LESS_EQUAL, 0.0, "eq7_"+i+"_"+l);
    				
    				GRBLinExpr ex8 = new GRBLinExpr();
    				ex8.addTerm(1.0, lin[i][l]);
    				ex8.addTerm(-1.0, x[i][l]);
    				model.addConstr(ex8, GRB.LESS_EQUAL, 0.0, "eq8_"+i+"_"+l);
    				
    				GRBLinExpr ex9 = new GRBLinExpr();
    				ex9.addTerm(1.0, lin[i][l]);
    				ex9.addTerm(-1.0, x[i][l]);
    				ex9.addTerm(-1.0, z[l]);
    				model.addConstr(ex9, GRB.GREATER_EQUAL, -1.0, "eq9_"+i+"_"+l);
    				//EQUATION 10
    			}
    			
    			
    			////TO DO: CHECK NUMERICAL
    			double val;
    			if (Ytrain.get(i)>1-numerical_instability_epsilon)
    			{
    				GRBLinExpr ex10 = new GRBLinExpr();
    				ex10.addTerm(1.0, tp[i]);
    				
    				for (int l=0;l<numTreeNodeVars/2+1;l++)
        				ex10.addTerm(-1.0, lin[i][l]);
    				model.addConstr(ex10, GRB.EQUAL, 0.0, "eq10_"+i);
    			}
    			
    			else if (Ytrain.get(i)<numerical_instability_epsilon)
    			{
    				GRBLinExpr ex31 = new GRBLinExpr();
    				ex31.addTerm(1.0, tn[i]);
    				
    				for (int l=0;l<numTreeNodeVars/2+1;l++)
    				{	
    					ex31.addTerm(-1.0, x[i][l]);
        				
        				ex31.addTerm(1.0, lin[i][l]);
    				}
    				model.addConstr(ex31, GRB.EQUAL, 0.0, "eq31_"+i);
    			}
    			
    		}
    		
    		
		model.update();
    		
    	}
    	catch(GRBException e)
    	{
    		System.out.println("Error code: " + e.getErrorCode() + ". " +
                    e.getMessage());
    	}
    }

    public void printSolution()
    {
    	try
    	{
    	int numTreeNodeVars = (int) (Math.pow(2, treeDepth+1) - 1);
		for (int n=0;n<b.length;n++)
		{
			System.out.println("B Val: "+b[n].get(GRB.DoubleAttr.X));
			System.out.print("Node "+n+" : ");
			for (int f=0;f<Xtrain.numCols();f++)
			{
				if (a[n][f].get(GRB.DoubleAttr.X)>1-numerical_instability_epsilon)
				{
					System.out.println("a is "+f+" | b is "+b[n].get(GRB.DoubleAttr.X));
				}
			}
		}
		
		for (int i=0;i<z.length;i++)
                {
			System.out.println("z("+i+") is: "+z[i].get(GRB.DoubleAttr.X));
                }

               /* for (int i=0;i<Xtrain.numRows();i++)
                {
		    System.out.println("Datapoint "+i+".....");
                    System.out.print("w["+i+"]["+0+"]" + w[i][0].get(GRB.DoubleAttr.X)+" ");
		    System.out.print("w["+i+"]["+1+"] "+ w[i][1].get(GRB.DoubleAttr.X)+" ");
		    System.out.println("w["+i+"]["+2+"] "+ w[i][2].get(GRB.DoubleAttr.X));
                    System.out.print("x["+i+"]["+0+"] "+ x[i][0].get(GRB.DoubleAttr.X)+" ");
                    System.out.print("x["+i+"]["+1+"] "+ x[i][1].get(GRB.DoubleAttr.X)+" ");
                    System.out.print("x["+i+"]["+2+"] "+ x[i][2].get(GRB.DoubleAttr.X)+" ");
		    System.out.println("x["+i+"]["+3+"] "+ x[i][3].get(GRB.DoubleAttr.X)+" ");
                }*/
                
    	}
    	catch(GRBException e)
    	{
    		e.printStackTrace();
    	}
    }
    
    public void printRootRelaxationSolution()
    {
    	try
    	{
    	int numTreeNodeVars = (int) (Math.pow(2, treeDepth+1) - 1);
		for (int n=0;n<numTreeNodeVars/2;n++)
		{
			System.out.println("Node: "+n);
			System.out.println("B Val: "+b[n].get(GRB.DoubleAttr.X));
			System.out.print("A Val: ");
			for (int f=0;f<Xtrain.numCols();f++)
			{
				System.out.print(a[n][f].get(GRB.DoubleAttr.X)+" ");
			}
			System.out.println();
		}
		
		for (int l=0;l<numTreeNodeVars/2+1;l++)
			System.out.println("Z Val: "+z[l].get(GRB.DoubleAttr.X)+" ");
		
		
		for (int i=0;i<Xtrain.numRows();i++)
		{
			for (int l=0; l<numTreeNodeVars/2+1; l++)
			{
				System.out.print("X Val & Xhat Val: ");
			
				System.out.print(x[i][l].get(GRB.DoubleAttr.X)+" ");
				System.out.print(lin[i][l].get(GRB.DoubleAttr.X)+" ,");		
			}
			System.out.println();
		}
		
		System.out.println("W Val");
		for (int i=0;i<Xtrain.numRows();i++)
		{
			for (int n=0;n<numTreeNodeVars/2;n++)
			{
				System.out.print(w[i][n].get(GRB.DoubleAttr.X)+" ");
			}
		}
		
		System.out.println();
		/*for (int i=0;i<Xtrain.numRows();i++)
		{
			if (Ytrain.get(i)==1)
				System.out.print("Positive datapoint "+(i+1)+" ");
			System.out.print(tp[i].get(GRB.DoubleAttr.X)+" ");
			System.out.println(tn[i].get(GRB.DoubleAttr.X)+" ");
		}*/
                
    	}
    	catch(GRBException e)
    	{
    		e.printStackTrace();
    	}
    }
    
    public void provideWarmStartCART(String filename)
    {
    	
		try 
		{
			DataSource source = new DataSource(filename);
			Instances data = source.getDataSet();
			if (data.classIndex() == -1)
				   data.setClassIndex(data.numAttributes() - 1);
			//data.deleteAttributeAt(0);
		
			
			REPTree tree = new REPTree();
			String[] options = weka.core.Utils.splitOptions("-M "+Nmin+" -V 0.001 -N 3 -S 1 -L "+treeDepth+" -P -I 0.0");
			//String[] options = weka.core.Utils.splitOptions("-M "+Nmin+" -L "+treeDepth);
			tree.setOptions(options);
			tree.buildClassifier(data);
			
			System.out.println(tree.toString());
			
			Tree root = tree.m_Tree;
			
			TreeNode rootNode = new TreeNode(root, null);
			
			Queue queue = new LinkedList();
			queue.add(rootNode);
			
			
			int index=0;
			while(index<b.length && !queue.isEmpty())
			{
				TreeNode treenode = (TreeNode)queue.remove();
				Tree node = treenode.node;
				Tree parent = treenode.parentNode;
				if (node.m_Attribute==-1)	
				{
					b[index].set(GRB.DoubleAttr.Start, parent.m_SplitPoint);
				}
				else
				{
					b[index].set(GRB.DoubleAttr.Start, node.m_SplitPoint);
				}
				int tamp = node.m_Attribute;
				if (tamp==-1)///need to revisit
					tamp = parent.m_Attribute;
				for (int j=0;j<Xtrain.numCols();j++)
				{
					
					if (j==tamp)
						a[index][j].set(GRB.DoubleAttr.Start, 1.0);
					else
						a[index][j].set(GRB.DoubleAttr.Start, 0.0);
				}
				index++;
				
				//add children
				if (node.m_Attribute!=-1)
				{
					for (int j=0;j<node.m_Successors.length;j++)
					{
						Tree successor = node.m_Successors[j];
						Tree parentVal = node;
						TreeNode child = new TreeNode(successor, parentVal);
						queue.add(child);
					}
				}
				else//construct binary tree
				{
					for (int j=0;j<2;j++)
					{
						Tree successor = tree.dummyTree;
						Tree parentVal = parent;
						TreeNode child = new TreeNode(successor, parentVal);
						queue.add(child);
					}
				}
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
    }
    
    class TreeNode
    {
    	public Tree node;
    	public Tree parentNode;
    	
    	public TreeNode(Tree node, Tree parentNode)
    	{
    		this.node = node;
    		this.parentNode = parentNode;
    	}
    }
}

