package models;

import java.awt.GridBagConstraints;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Random;
import java.util.Scanner;

import org.ejml.data.DenseMatrix64F;
import org.ejml.ops.MatrixIO;
import org.ejml.simple.SimpleMatrix;

import gurobi.GRB;
import gurobi.GRBEnv;
import gurobi.GRBException;
import gurobi.GRBModel;
import gurobi.GRBLinExpr;
import gurobi.GRBVar;
import helpers.CARTTree;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;
import weka.classifiers.Evaluation;
import weka.classifiers.trees.*;

public class logisticRegression {
    public GRBEnv    env;
    public GRBModel  model;
    public GRBVar W[], I[]; 
    
    DenseMatrix64F dataset;
    SimpleMatrix data, X, Y, Xtrain, Ytrain, Xtest, Ytest;
    
    int featureNum;
    double interpretabilityWeights[];
    double M1,M2,M3;
    String outputfile;
    double threshold;
    static double M1 = 100000;
    static double M2 = -100000;
    static double M3 = 100000;

    
    public logisticRegression(String filename, double interpretabilityWeights[], String outputfile, double fractionTrainSet, double threshold)
    {
    	try
    	{
    		//this.treeDepth = treeDepth;
    		this.M1 = M1;
    	  	this.M2 = M2;
			this.M3 = M3;
			this.threshold = threshold;
        	this.interpretabilityWeights = interpretabilityWeights;
        	this.outputfile = outputfile;
    		dataset = MatrixIO.loadCSV(filename);
    		data = SimpleMatrix.wrap(dataset);
    	
    		X = data.extractMatrix(0, data.numRows(), 0, data.numCols()-1);//leave last column for label
    		Y = data.extractVector(false, data.numCols()-1);///last label
    		Xtrain = new SimpleMatrix(0,0);
    		Ytrain = new SimpleMatrix(0,0);
    		Xtest = new SimpleMatrix(0,0);
    		Ytest = new SimpleMatrix(0,0);
    		
    		int numRowsinTrainSet = (int)(fractionTrainSet*(double)X.numRows());
    		
    		ArrayList<Integer> testIndices = new ArrayList<Integer>();
    		for (int i=0;i<X.numRows();i++)
    			testIndices.add(i);
    		
    		ArrayList<Integer> trainIndices = new ArrayList<Integer>();
    		for (int i=0;i<numRowsinTrainSet;i++)
    		{
    			int index = (int)(Math.random()*testIndices.size());///select random number from all rows
    			int index_val = testIndices.remove(index);
    			trainIndices.add(index_val);
    		}
    		
    		for (int i=0;i<trainIndices.size();i++)
    		{
    			Xtrain = Xtrain.combine(i,0,X.extractVector(true, trainIndices.get(i)));
    			Ytrain = Ytrain.combine(i,0,Y.extractVector(true, trainIndices.get(i)));
    			
    		}
    		
    		for (int i=0;i<testIndices.size();i++)
    		{
    			Xtest = Xtest.combine(i,0,X.extractVector(true, testIndices.get(i)));
    			Ytest = Ytest.combine(i,0,Y.extractVector(true, testIndices.get(i)));
    		}
    		
    		
    		env   = new GRBEnv("mip1.log");
    		model = new GRBModel(env);
    		specifyModel();
    	}
    	catch(IOException e)
    	{
     	    System.out.println(e.getMessage());
    	}
    	catch(GRBException e)
    	{
      	    System.out.println("Error code: " + e.getErrorCode() + ". " +
                    e.getMessage());
    	}
    }
    
    public void specifyModel()
    {
    	try
    	{
    		//create T and W variables			
			I = new GRBVar[featureNum];
			W = new GRBVar[featureNum]
			
    		for (int i=0;i<featureNum; i++)
    		{
    			I[i] = model.addVar(0.0, 1.0, 0.0, GRB.BINARY, "Nt_"+i);
    			W[i] = model.addVar(-10.0, 20, 0.0, GRB.CONTINUOUS, "b_"+i);

    		}
    		
			
			// equation 1
			for (int i=0;i<featureNum;i++)
    		{
    			GRBLinExpr ex1 = new GRBLinExpr();
				ex1.addTerm(-1.0*M1, I[i]);
				ex1.addTerm(1.0, W[i])
    			model.addConstr(ex1, GRB.LESS_EQUAL , 0.0, "eq1_"+i);
			}
			
			// equation 2
			for (int i=0;i<featureNum;i++)
    		{
    			GRBLinExpr ex1 = new GRBLinExpr();
				ex1.addTerm(-1.0*M2, I[i]);
				ex1.addTerm(1.0, W[i])
    			model.addConstr(ex1, GRB.GREATER_EQUAL , 0.0, "eq2_"+i);
			}
			
			// equation 3
			for (int i=0;i<featureNum;i++)
    		{
    			GRBLinExpr ex1 = new GRBLinExpr();
				ex1.addTerm(1.0, I[i]);
				ex1.addTerm(-1.0*M3, W[i])
    			model.addConstr(ex1, GRB.LESS_EQUAL , 0.0, "eq3_"+i);
			}
			
            
            
            double result = 0.0;
            for(int i=0;i<featureNum;i++)
            {
              result = result + W[i]*x[i];
            }
            double output = 1/(1+Math.exp(-result));    
		    return output;
            
			/*
    		//define matrix Y
    		//System.out.println("Matrix Y:");
    		int matY[][] = new int[Xtrain.numRows()][2];
    		for (int i=0;i<Xtrain.numRows();i++)
    		{
    			for (int j=0;j<2;j++)
    			{
    				if (Ytrain.get(i)==j)
    					matY[i][j]=1;
    				else
    					matY[i][j]=-1;
    				//System.out.print(matY[i][j]+" ");
    			}
    			//System.out.println();
    		}
    		*/
    		
    	
    		
    		model.update();

     
			
			/////////
			double result = 0.0;
			for (int i=0;i<Xtrain.numRows();i++)
               {
                double y = logisticFunction(Xtrain[i]);
                result = result + y*Math.log(y)+(1-y)*log(1-y);
               }
            
				
		
	
    		
    		//objective function
    		double Lhat = 0;
    		for (int i=0;i<Xtrain.numRows();i++)
    			Lhat += Ytrain.get(i);
    		System.out.println("Lhat boy: "+Lhat);
    		
    		GRBLinExpr obj = new GRBLinExpr();
    		for (int i=0;i<numTreeNodeVars/2+1;i++)
    			obj.addTerm(1.0/Lhat, L[i]);
    		
    		for (int i=0;i<Xtrain.numCols();i++)
    			obj.addTerm(-1*lambda*interpretabilityWeights[i], e[i]);
    		
    		
    	  		model.setObjective(obj, GRB.MINIMIZE);
       		
    	}
    	catch(GRBException e)
    	{
      	    System.out.println("Error code: " + e.getErrorCode() + ". " +
                    e.getMessage());
    	}
    }
    
 	
	
    
   
    /*public void printSolution()
    {
    	try
    	{
    		System.out.println("Solution to be printed:");
    		FileWriter fw = new FileWriter(outputfile, true);
    		System.out.println("Number of d vars: "+d.length+" Number of b vars: "+b.length);
    		
    		for (int i=0;i<b.length;i++)
    		{
    			System.out.println("D Val: "+d[i].get(GRB.DoubleAttr.X)+" "+b[i].get(GRB.DoubleAttr.X));
    			fw.write("Node "+i+" : ");
    			System.out.print("Node "+i+" : ");
    			for (int j=0;j<Xtrain.numCols();j++)
    			{
    				if (a[j][i].get(GRB.DoubleAttr.X)==1)
    				{
    					fw.write("a is "+j+" | b is "+b[i].get(GRB.DoubleAttr.X));
    					System.out.println("a is "+j+" | b is "+b[i].get(GRB.DoubleAttr.X));
    				}
    			}
    		}
    		
    		//for (int i=0;i<l.length;i++)
    		//System.out.println("ckt: "+ckt[0][0]+" "+ckt[1][0]);
    		
    		double Lhat = 0;
    		for (int i=0;i<Xtrain.numRows();i++)
    			Lhat += Ytrain.get(i);
    		
    	   	String chosenFeatures="";
        	double accuracyVal=0, interpretabilityVal=0;
       		for (int i=0;i<L.length;i++)
       		{
       			System.out.println("Li: "+L[i].get(GRB.DoubleAttr.X));
    			accuracyVal += L[i].get(GRB.DoubleAttr.X);
       		}
       		for (int i=0;i<e.length;i++)
       		{
       			if (e[i].get(GRB.DoubleAttr.X)==1)
       				chosenFeatures += i+" , ";
       			interpretabilityVal += interpretabilityWeights[i]*e[i].get(GRB.DoubleAttr.X);
       		}
           	System.out.println("Lambda: "+lambda+" Accuracy: "+accuracyVal+" Interpretability: "+interpretabilityVal + " Features used: "+ chosenFeatures);
           	
           	fw.write("Lambda: "+lambda+" Objective val: "+  model.get(GRB.DoubleAttr.ObjVal) +" Objective first time: " + (accuracyVal/Lhat) +"Accuracy: "+accuracyVal+" Interpretability: "+interpretabilityVal + " Features used: "+ chosenFeatures+" Optimality Gap: "+ model.get(GRB.DoubleAttr.MIPGap) +"\n");
        	fw.close();
    	}
    	catch(GRBException e)
    	{
        System.out.println("Error code: " + e.getErrorCode() + ". " +
                    e.getMessage());
    	}
    	catch(IOException e)
    	{
      	System.out.println(e.getMessage());
    	}
    	
    }*/
    

    /*public double[] evaluateClassifieronTestSet()
    {
    	double result[] = new double[4];
    	try
    	{
    		
    		double Lhat = 0;
    		for (int i=0;i<Xtest.numRows();i++)
    			Lhat += Ytest.get(i);
    		System.out.println("Lhat on test set: "+ Lhat);
    		int numTreeNodeVars = (int) (Math.pow(2, treeDepth+1) - 1);
    		
    		double misclassified = 0;
    		double tp=0,fp=0,tn=0,fn=0;
    		for (int i=0;i<Xtest.numRows();i++)
    		{
    			int currentNode = 0;
    			while (currentNode<d.length)
    			{
    				for (int j=0;j<Xtest.numCols();j++)
        			{
        				if (a[j][currentNode].get(GRB.DoubleAttr.X)==1)
        				{
        					if (Xtest.get(i, j) < b[currentNode].get(GRB.DoubleAttr.X))
        						currentNode = currentNode*2 + 1;//left child
        					else
        						currentNode = currentNode*2 + 2;//right child
        					break;
        				}
        			}
    			}
    			
    			int leaf = currentNode - numTreeNodeVars/2;
    			int currPrediction = ckt[0][leaf].get(GRB.DoubleAttr.X)==1?0:1;
    			misclassified += Math.abs(currPrediction - Ytest.get(i));
    			if (currPrediction==1)
    			{
    				if (Ytest.get(i)==1)
    					tp++;
    				else if (Ytest.get(i)==0)
    					fn++;
    			}
    			else if (currPrediction==0)
    			{
    				if (Ytest.get(i)==1)
    					fp++;
    				else if (Ytest.get(i)==0)
    					tn++;
    			}
    		}
    		double precision = tp/(tp+fp);
    		double recall = tp/(tp+fn);
    		double f1 = 2*precision*recall/(precision+recall);
    		result[0]=precision;
    		result[1]=recall;
    		result[2]=f1;
    		result[3]=misclassified/Xtest.numRows();
    		return result;
    	}
    	catch(GRBException e)
    	{
    		System.out.println("Error code: " + e.getErrorCode() + ". " +
                    e.getMessage());
    	}
    	return result;
    }*/
}