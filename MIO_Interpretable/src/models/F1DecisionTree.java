package models;

import java.awt.GridBagConstraints;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Random;
import java.util.Scanner;

import org.ejml.data.DenseMatrix64F;
import org.ejml.ops.MatrixIO;
import org.ejml.simple.SimpleMatrix;

import gurobi.GRB;
import gurobi.GRBEnv;
import gurobi.GRBException;
import gurobi.GRBModel;
import gurobi.GRBLinExpr;
import gurobi.GRBVar;
import helpers.CARTTree;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;
import weka.classifiers.Evaluation;
import weka.classifiers.trees.*;

public class F1DecisionTree {
    public GRBEnv    env;
    public GRBModel  model;
    public GRBVar a[][], b[], d[], z[][], l[], Nt[] , Nkt[][], ckt[][],  e[];
    
    //Precision recall variables
    public GRBVar oneminusckt[][];
    public GRBVar tp[],tn[],fp[],fn[];
    public GRBVar total_tp, total_tn, total_fp, total_fn;
    public GRBVar h1[][], h2[][];
    
    
    DenseMatrix64F dataset;
    SimpleMatrix data, X, Y, Xtrain, Ytrain, Xtest, Ytest;
    
    int treeDepth, Nmin;
    double interpretabilityWeights[];
    double lambda,lambda2;
    String outputfile;
    
    double alpha;
    
    
    static double M = 100000;
    static double numerical_instability_epsilon = 0.0001;
    
    public F1DecisionTree(String filename, int treeDepth, int Nmin, double lambda, double interpretabilityWeights[], String outputfile, double fractionTrainSet, double alpha)
    {
    	try
    	{
    		this.alpha = alpha;
    		
    		this.treeDepth = treeDepth;
    		this.Nmin = Nmin;
    	  	this.lambda = lambda;
        	this.interpretabilityWeights = interpretabilityWeights;
        	this.outputfile = outputfile;
    		dataset = MatrixIO.loadCSV(filename);
    		data = SimpleMatrix.wrap(dataset);
    	
    		X = data.extractMatrix(0, data.numRows(), 0, data.numCols()-1);//leave last column for label
    		Y = data.extractVector(false, data.numCols()-1);///last label
    		
    		
    		ArrayList<Integer> oneIndices = new ArrayList<Integer>();
    		ArrayList<Integer> zeroIndices = new ArrayList<Integer>();
    		for (int i=0;i<X.numRows();i++)
    		{
    			if (Y.get(i)==1)
    				oneIndices.add(i);
    			else
    				zeroIndices.add(i);
    		}
    		

    		
    		int numRowsinTrainSetFromOnes = (int)(fractionTrainSet*(double)oneIndices.size());
    		int numRowsinTrainSetFromZeros = (int)(fractionTrainSet*(double)zeroIndices.size());
    		
    		
    		ArrayList<Integer> trainIndices = new ArrayList<Integer>();
    		for (int i=0;i<numRowsinTrainSetFromOnes;i++)
    		{
    			int index = (int)(Math.random()*oneIndices.size());///select random number from all rows
    			int index_val = oneIndices.remove(index);
    			trainIndices.add(index_val);
    		}
    		for (int i=0;i<numRowsinTrainSetFromZeros;i++)
    		{
    			int index = (int)(Math.random()*zeroIndices.size());///select random number from all rows
    			int index_val = zeroIndices.remove(index);
    			trainIndices.add(index_val);
    		}
    		
    		ArrayList<Integer> testIndices = new ArrayList<Integer>();
    		for (int i=0;i<oneIndices.size();i++)
    			testIndices.add(oneIndices.get(i));
    		for (int i=0;i<zeroIndices.size();i++)
    			testIndices.add(zeroIndices.get(i));
    		
    
    		Xtrain = new SimpleMatrix(0,0);
    		Ytrain = new SimpleMatrix(0,0);
    		Xtest = new SimpleMatrix(0,0);
    		Ytest = new SimpleMatrix(0,0);
    		
    		for (int i=0;i<trainIndices.size();i++)
    		{
    			Xtrain = Xtrain.combine(i,0,X.extractVector(true, trainIndices.get(i)));
    			Ytrain = Ytrain.combine(i,0,Y.extractVector(true, trainIndices.get(i)));
    			
    		}
    		
    		for (int i=0;i<testIndices.size();i++)
    		{
    			Xtest = Xtest.combine(i,0,X.extractVector(true, testIndices.get(i)));
    			Ytest = Ytest.combine(i,0,Y.extractVector(true, testIndices.get(i)));
    		}
    		
    		int numOnes=0, numZeros=0;
    		for (int i=0;i<Xtrain.numRows();i++)
    		{
    			if (Ytrain.get(i)==1)
    				numOnes++;
    			else
    				numZeros++;
    		}
    		System.out.println("Training set numbers: "+ numOnes + " ones & "+ numZeros+" zeros.");
    		
    		numOnes=0;
    		numZeros=0;
    		for (int i=0;i<Xtest.numRows();i++)
    		{
    			if (Ytest.get(i)==1)
    				numOnes++;
    			else
    				numZeros++;
    		}
    		System.out.println("Test set numbers: "+ numOnes + " ones & "+ numZeros+" zeros.");
    			
    		
    		
    		env   = new GRBEnv("mip1.log");
    		model = new GRBModel(env);
    		specifyModel();
    	}
    	catch(IOException e)
    	{
     	    System.out.println(e.getMessage());
    	}
    	catch(GRBException e)
    	{
      	    System.out.println("Error code: " + e.getErrorCode() + ". " +
                    e.getMessage());
    	}
    }
    
    public void specifyModel()
    {
    	try
    	{
    		int numTreeNodeVars = (int) (Math.pow(2, treeDepth+1) - 1);
    		
    		//equation 4
    		//create a_ij, b_j and d_j vars
    		a = new GRBVar[Xtrain.numCols()][numTreeNodeVars/2];
    		b = new GRBVar[numTreeNodeVars/2];
    		d = new GRBVar[numTreeNodeVars/2];
    		for (int i=0;i<numTreeNodeVars/2;i++)
    		{
    			b[i] = model.addVar(0.0, 1.0, 0.0, GRB.CONTINUOUS, "b_"+i);
    			d[i] = model.addVar(0.0, 1.0, 0.0, GRB.BINARY, "d_"+i);
    			for (int j=0;j<Xtrain.numCols();j++)
    				a[j][i] = model.addVar(0.0, 1.0, 0.0, GRB.BINARY, "a_"+j+"_"+i);
    		}
    		
    		//create z, L and l variables for leaves
    		z = new GRBVar[Xtrain.numRows()][numTreeNodeVars/2+1];
    		l = new GRBVar[numTreeNodeVars/2+1];
    		//L = new GRBVar[numTreeNodeVars/2+1];
    		for (int i=0; i<numTreeNodeVars/2+1; i++)
    		{
    			l[i] = model.addVar(0.0, 1.0, 0.0, GRB.BINARY, "l_"+i);
    			//L[i] = model.addVar(0.0, Xtrain.numRows(), 0.0, GRB.INTEGER, "L_"+i);
    			for (int j=0;j<Xtrain.numRows();j++)
    				z[j][i] = model.addVar(0.0, 1.0, 0.0, GRB.BINARY, "z_"+j+"_"+i);
    		}
    		
    		//create Nt and Nkt variables
       		Nkt = new GRBVar[2][numTreeNodeVars/2+1];//2 because there are only two classes
    		Nt = new GRBVar[numTreeNodeVars/2+1];
    		for (int i=0;i<numTreeNodeVars/2+1; i++)
    		{
    			Nt[i] = model.addVar(0.0, Xtrain.numRows(), 0.0, GRB.INTEGER, "Nt_"+i);
    			for (int j=0;j<2;j++)
    				Nkt[j][i] = model.addVar(0.0, Xtrain.numRows(), 0.0, GRB.INTEGER, "Nkt_"+j+"_"+i);
    		}
    		
    		//define matrix Y
    		//System.out.println("Matrix Y:");
    		int matY[][] = new int[Xtrain.numRows()][2];
    		for (int i=0;i<Xtrain.numRows();i++)
    		{
    			for (int j=0;j<2;j++)
    			{
    				if (Ytrain.get(i)==j)
    					matY[i][j]=1;
    				else
    					matY[i][j]=-1;
    				//System.out.print(matY[i][j]+" ");
    			}
    			//System.out.println();
    		}
    		
    		
    		//create ckt variables
      		ckt = new GRBVar[2][numTreeNodeVars/2+1];//2 because there are only two classes
    		for (int i=0;i<numTreeNodeVars/2+1; i++)
    		{
    			for (int j=0;j<2;j++)
    				ckt[j][i] = model.addVar(0.0, 1.0, 0.0, GRB.BINARY, "ckt_"+j+"_"+i);
    		}
    		
    		//create e variables
    		e = new GRBVar[Xtrain.numCols()];
    		for (int i=0;i<e.length;i++)
    			e[i] = model.addVar(0.0, 1.0, 0.0, GRB.BINARY, "e_"+i);
    		
    		/////addition for precision and recall computation
    		///will work only for binary classification
    		//create ckt variables
      		oneminusckt = new GRBVar[2][numTreeNodeVars/2+1];//2 because there are only two classes
    		for (int i=0;i<numTreeNodeVars/2+1; i++)
    		{
    			for (int j=0;j<2;j++)
    				oneminusckt[j][i] = model.addVar(0.0, 1.0, 0.0, GRB.BINARY, "oneminusckt_"+j+"_"+i);
    		}
    		
    		
      		h1 = new GRBVar[2][numTreeNodeVars/2+1];//2 because there are only two classes
      		h2 = new GRBVar[2][numTreeNodeVars/2+1];
    		for (int i=0;i<numTreeNodeVars/2+1; i++)
    		{
    			for (int j=0;j<2;j++)
    			{
    				h1[j][i] = model.addVar(0.0, 1.0, 0.0, GRB.BINARY, "h1_"+j+"_"+i);
    				h2[j][i] = model.addVar(0.0, 1.0, 0.0, GRB.BINARY, "h2_"+j+"_"+i);
    			}
    		}
    		
    		tp = new GRBVar[numTreeNodeVars/2+1];
    		tn = new GRBVar[numTreeNodeVars/2+1];
    		fp = new GRBVar[numTreeNodeVars/2+1];
    		fn = new GRBVar[numTreeNodeVars/2+1];
    		for (int i=0;i<numTreeNodeVars/2+1;i++)
    		{
    			tp[i] = model.addVar(0.0, Xtrain.numRows(), 0.0, GRB.INTEGER, "tp_"+i);
    			tn[i] = model.addVar(0.0, Xtrain.numRows(), 0.0, GRB.INTEGER, "tn_"+i);
    			fp[i] = model.addVar(0.0, Xtrain.numRows(), 0.0, GRB.INTEGER, "fp_"+i);
    			fn[i] = model.addVar(0.0, Xtrain.numRows(), 0.0, GRB.INTEGER, "fn_"+i);
    		}
    		
    		total_tp = model.addVar(0.0, Xtrain.numRows(), 0.0, GRB.INTEGER, "total_tp");
    		total_tn = model.addVar(0.0, Xtrain.numRows(), 0.0, GRB.INTEGER, "total_tn");
    		total_fp = model.addVar(0.0, Xtrain.numRows(), 0.0, GRB.INTEGER, "total_fp");
    		total_fn = model.addVar(0.0, Xtrain.numRows(), 0.0, GRB.INTEGER, "total_fn");
    		
    		model.update();
    		
    		///equation 2 and 3
    		for (int i=0;i<numTreeNodeVars/2;i++)
    		{
    			///equation 2
    			GRBLinExpr ex1 = new GRBLinExpr();
    			for (int j=0;j<Xtrain.numCols();j++)
    				ex1.addTerm(1.0, a[j][i]);
    			//model.addConstr(ex1, GRB.EQUAL, 1.0, "eq2_"+i);
    			ex1.addTerm(-1.0, d[i]);
    			model.addConstr(ex1, GRB.EQUAL, 0.0, "eq2_"+i);
    			
    			//equation 3
    			GRBLinExpr ex2 = new GRBLinExpr();
    			ex2.addTerm(1.0, b[i]);
    			ex2.addTerm(-1.0, d[i]);
    			model.addConstr(ex2, GRB.LESS_EQUAL, 0.0, "eq3_"+i);
    		}
    		
    		//equation 5
    		for (int i=numTreeNodeVars/2 - 1;i>0;i--)
    		{
    			GRBLinExpr ex3 = new GRBLinExpr();
    			ex3.addTerm(1.0, d[i]);
    			if (i%2==0)///look at tree with root starting from 0
    				ex3.addTerm(-1.0, d[i/2 - 1]);
    			else
    				ex3.addTerm(-1.0,  d[i/2]);
    			model.addConstr(ex3, GRB.LESS_EQUAL, 0.0, "eq5_"+i);
    		}
    	
    		//equation 6 and 7
    		for (int i=0;i<numTreeNodeVars/2+1;i++)
    		{
    			GRBLinExpr ex5 = new GRBLinExpr();
    			for (int j=0;j<Xtrain.numRows();j++)
    			{
    				ex5.addTerm(1.0, z[j][i]);
    				
    				GRBLinExpr ex6 = new GRBLinExpr();
    				ex6.addTerm(1.0, z[j][i]);
    				ex6.addTerm(-1.0, l[i]);
    				model.addConstr(ex6, GRB.LESS_EQUAL, 0.0, "eq6_"+i+"_"+j);
    			}
    			ex5.addTerm(-1.0*Nmin, l[i]);
    			model.addConstr(ex5, GRB.GREATER_EQUAL , 0.0, "eq7_"+i);
    		}
    		
    		//equation 8
    		for (int i=0;i<Xtrain.numRows();i++)
    		{
    			GRBLinExpr ex4 = new GRBLinExpr();
    			for (int j=0;j<numTreeNodeVars/2+1;j++)
    				ex4.addTerm(1.0, z[i][j]);
    			model.addConstr(ex4, GRB.EQUAL, 1.0, "eq8_"+i);
    		}
    		
    		//equations 9 and 10
    		//finding epsilon for eqn 9
    		double epsilon[] = new double[Xtrain.numCols()];
    		double feature[] = new double[Xtrain.numRows()];
    		double max_epsilon_entry=-1;
    		int min_epsilon_index=-1;
    		double min_epsilon_entry=10000;
    		for (int i=0;i<Xtrain.numCols();i++)
    		{
    			for (int j=0;j<Xtrain.numRows();j++)
    				feature[j] = Xtrain.get(j, i);
    			Arrays.sort(feature);
    			double minVal=10000;
    			for (int j=feature.length-1;j>0;j--)
    			{
    				double temp = feature[j]-feature[j-1];
    				if (temp!=0 && temp<minVal)
    					minVal = temp;
    			}
    			epsilon[i] = minVal;
    			if (epsilon[i]<min_epsilon_entry)
    			{
    				min_epsilon_index = i;
    				min_epsilon_entry = epsilon[i];
    			}
    			if (epsilon[i]>max_epsilon_entry)
    				max_epsilon_entry = epsilon[i];
    		}
    		System.out.println("Min epsilon"+ min_epsilon_entry + " "+min_epsilon_index);
    		
    		/*System.out.print("eps: ");
    		for (int i=0;i<X.numCols();i++)
    			System.out.print(" "+epsilon[i]);
    		System.out.println("\n");*/
    		
    		//no equation for root
    		///for all nodes other than root
    		for (int i=numTreeNodeVars/2;i<numTreeNodeVars;i++)
    		{
    			for (int j=0;j<Xtrain.numRows();j++)
    			{
    				int currNode = i;
    				boolean rootReached = false;
    				while (!rootReached)
    				{
    					if (currNode%2==0)///right child
    					{
    						//System.out.println(currNode);
    						int currParent = currNode/2-1;
    						GRBLinExpr ex9 = new GRBLinExpr();
    						for (int k=0;k<Xtrain.numCols();k++)
    							ex9.addTerm(Xtrain.get(j, k) ,a[k][currParent]);
    						ex9.addTerm(-1.0, b[currParent]);
    						ex9.addTerm(-1.0, z[j][i-numTreeNodeVars/2]);
    						model.addConstr(ex9, GRB.GREATER_EQUAL, -1.0, "eq10_"+(i-numTreeNodeVars/2)+"_"+j);
    						currNode = currNode/2-1;
    						if (currNode<=0)
    							rootReached = true;
    					}
    					else///left child
    					{
    						int currParent = currNode/2;
    						GRBLinExpr ex10 = new GRBLinExpr();
    						for (int k=0;k<Xtrain.numCols();k++)
    							ex10.addTerm(Xtrain.get(j, k) ,a[k][currParent]);
    							//ex10.addTerm(X.get(j, k) + epsilon[k] ,a[k][currParent]);
    						ex10.addTerm(-1.0, b[currParent]);
    						ex10.addTerm(1+max_epsilon_entry, z[j][i-numTreeNodeVars/2]);
    						model.addConstr(ex10, GRB.LESS_EQUAL, 1 + max_epsilon_entry - min_epsilon_entry, "eq9_"+(i-numTreeNodeVars/2)+"_"+j);
    						currNode = currNode/2;
    						if (currNode<=0)
    							rootReached = true;
    					}
    				}
    			}
    		}
    		
    		
    		///equation 15 and 16
    		for (int i=0;i<numTreeNodeVars/2+1;i++)
    		{
    			GRBLinExpr ex12 = new GRBLinExpr();
    			int first=0;
    			for (int j=0;j<2;j++)
    			{
    				GRBLinExpr ex11 = new GRBLinExpr();
    				for (int k=0;k<Xtrain.numRows();k++)
    				{
    					if (first==0)
    					{
    						ex12.addTerm(1.0,z[k][i]);
    					}
    					ex11.addTerm(0.5*(1+matY[k][j]), z[k][i]);
    				}
    				first++;
    				ex11.addTerm(-1.0, Nkt[j][i]);
    				model.addConstr(ex11, GRB.EQUAL, 0.0, "eq15_"+i+"_"+j);
    			}
    			ex12.addTerm(-1.0, Nt[i]);
    			model.addConstr(ex12, GRB.EQUAL, 0.0, "eq16_"+i);
    		}
    		
    		///equation 18
    		for (int i=0;i<numTreeNodeVars/2+1;i++)
    		{
    			GRBLinExpr ex13 = new GRBLinExpr();
    			for (int j=0;j<2;j++)
    				ex13.addTerm(1.0, ckt[j][i]);
    			ex13.addTerm(-1.0, l[i]);
    			model.addConstr(ex13, GRB.EQUAL, 0.0, "eq18_"+i);
    		}
    		
    		///equation 20,21 and 22
    		/*for (int i=0;i<numTreeNodeVars/2+1;i++)
    		{
    			for (int j=0;j<2;j++)
    			{
    				GRBLinExpr ex14 = new GRBLinExpr();
    				ex14.addTerm(1.0, L[i]);
    				ex14.addTerm(-1.0, Nt[i]);
    				ex14.addTerm(1.0, Nkt[j][i]);
    				ex14.addTerm(-1*Xtrain.numRows(), ckt[j][i]);
    				model.addConstr(ex14, GRB.GREATER_EQUAL, -1*Xtrain.numRows(), "eq20_"+i+"_"+j);
    				
    				GRBLinExpr ex15 = new GRBLinExpr();
    				ex15.addTerm(1.0, L[i]);
    				ex15.addTerm(-1.0, Nt[i]);
    				ex15.addTerm(1.0, Nkt[j][i]);
    				ex15.addTerm(-1*Xtrain.numRows(), ckt[j][i]);
    				model.addConstr(ex15, GRB.LESS_EQUAL, 0.0 , "eq21_"+i+"_"+j);
    			}
    		}*/
    		
    		for (int i=0;i<Xtrain.numCols();i++)
    		{
    			GRBLinExpr ex17 = new GRBLinExpr();
    			for (int j=0;j<numTreeNodeVars/2;j++)
    			{
    				ex17.addTerm(1.0, a[i][j]);
    				
    				GRBLinExpr ex16 = new GRBLinExpr();
    				ex16.addTerm(1.0, e[i]);
    				ex16.addTerm(-1.0, a[i][j]);
    				model.addConstr(ex16, GRB.GREATER_EQUAL, 0.0, "eq22_"+i+"_"+j);
    			}
    			ex17.addTerm(-1.0, e[i]);
    			model.addConstr(ex17, GRB.GREATER_EQUAL, 0.0, "eq23_"+i);
    		}
    		
    		
    		////precision and recall computation
    		///linearizing bilinear constraints
    		for (int i=0;i<numTreeNodeVars/2+1;i++)
    		{
    			for (int j=0;j<2;j++)
    			{
    				GRBLinExpr ex18 = new GRBLinExpr();
    				ex18.addTerm(1.0, oneminusckt[j][i]);
    				ex18.addTerm(1.0, ckt[j][i]);
    				model.addConstr(ex18, GRB.EQUAL, 1.0, "eq24_"+i+"_"+j);
    				
    				GRBLinExpr ex19 = new GRBLinExpr();
    				ex19.addTerm(1.0, h1[j][i]);
    				ex19.addTerm(-1.0, ckt[j][i]);
    				model.addConstr(ex19, GRB.LESS_EQUAL, 0.0, "eq25_"+i+"_"+j);
    				
    				GRBLinExpr ex20 = new GRBLinExpr();
    				ex20.addTerm(1.0, h1[j][i]);
    				ex20.addTerm(-1.0, l[i]);
    				model.addConstr(ex20, GRB.LESS_EQUAL, 0.0, "eq26_"+i+"_"+j);
    				
    				GRBLinExpr ex21 = new GRBLinExpr();
    				ex21.addTerm(1.0, h1[j][i]);
    				ex21.addTerm(-1.0, ckt[j][i]);
    				ex21.addTerm(-1.0, l[i]);
    				model.addConstr(ex21, GRB.GREATER_EQUAL, -1.0, "eq27_"+i+"_"+j);
    				
    				
    				
    				GRBLinExpr ex22 = new GRBLinExpr();
    				ex22.addTerm(1.0, h2[j][i]);
    				ex22.addTerm(-1.0, oneminusckt[j][i]);
    				model.addConstr(ex22, GRB.LESS_EQUAL, 0.0, "eq28_"+i+"_"+j);
    				
    				GRBLinExpr ex23 = new GRBLinExpr();
    				ex23.addTerm(1.0, h2[j][i]);
    				ex23.addTerm(-1.0, l[i]);
    				model.addConstr(ex23, GRB.LESS_EQUAL, 0.0, "eq29_"+i+"_"+j);
    				
    				GRBLinExpr ex24 = new GRBLinExpr();
    				ex24.addTerm(1.0, h2[j][i]);
    				ex24.addTerm(-1.0, oneminusckt[j][i]);
    				ex24.addTerm(-1.0, l[i]);
    				model.addConstr(ex24, GRB.GREATER_EQUAL, -1.0, "eq30_"+i+"_"+j);	
    			}
    		}
    		
    		for (int i=0;i<numTreeNodeVars/2+1;i++)
    		{
    			///True positives
    			GRBLinExpr ex25 = new GRBLinExpr();
    			ex25.addTerm(1.0, tp[i]);
    			ex25.addTerm(-1.0, Nkt[1][i]);
    			model.addConstr(ex25, GRB.LESS_EQUAL, 0.0, "eq31_"+i);
    			
    			GRBLinExpr ex26 = new GRBLinExpr();
    			ex26.addTerm(1.0, tp[i]);
    			ex26.addTerm(-1*M, h1[1][i]);
    			model.addConstr(ex26, GRB.LESS_EQUAL, 0.0, "eq32_"+i);
    			
       			GRBLinExpr ex27 = new GRBLinExpr();
    			ex27.addTerm(1.0, tp[i]);
    			ex27.addTerm(-1.0*(M+1), h1[1][i]);
       			ex27.addTerm(-1.0, Nkt[1][i]);
    			model.addConstr(ex27, GRB.GREATER_EQUAL, -1 - M, "eq33_"+i);
    			
    			////True negatives
    			GRBLinExpr ex28 = new GRBLinExpr();
    			ex28.addTerm(1.0, tn[i]);
    			ex28.addTerm(-1.0, Nkt[0][i]);
    			model.addConstr(ex28, GRB.LESS_EQUAL, 0.0, "eq34_"+i);
    			
    			GRBLinExpr ex29 = new GRBLinExpr();
    			ex29.addTerm(1.0, tn[i]);
    			ex29.addTerm(-1*M, h1[0][i]);
    			model.addConstr(ex29, GRB.LESS_EQUAL, 0.0, "eq35_"+i);
    			
       			GRBLinExpr ex30 = new GRBLinExpr();
    			ex30.addTerm(1.0, tn[i]);
    			ex30.addTerm(-1.0*(M+1), h1[0][i]);
       			ex30.addTerm(-1.0, Nkt[0][i]);
    			model.addConstr(ex30, GRB.GREATER_EQUAL, -1 - M, "eq36_"+i);
    			
    			////False negatives
    			GRBLinExpr ex31 = new GRBLinExpr();
    			ex31.addTerm(1.0, fn[i]);
    			ex31.addTerm(-1.0, Nkt[1][i]);
    			model.addConstr(ex31, GRB.LESS_EQUAL, 0.0, "eq37_"+i);
    			
    			GRBLinExpr ex32 = new GRBLinExpr();
    			ex32.addTerm(1.0, fn[i]);
    			ex32.addTerm(-1*M, h2[1][i]);
    			model.addConstr(ex32, GRB.LESS_EQUAL, 0.0, "eq38_"+i);
    			
       			GRBLinExpr ex33 = new GRBLinExpr();
    			ex33.addTerm(1.0, fn[i]);
    			ex33.addTerm(-1.0*(M+1), h2[1][i]);
       			ex33.addTerm(-1.0, Nkt[1][i]);
    			model.addConstr(ex33, GRB.GREATER_EQUAL, -1 - M, "eq39_"+i);
    			
    			////False positives
    			GRBLinExpr ex34 = new GRBLinExpr();
    			ex34.addTerm(1.0, fp[i]);
    			ex34.addTerm(-1.0, Nkt[0][i]);
    			model.addConstr(ex34, GRB.LESS_EQUAL, 0.0, "eq40_"+i);
    			
    			GRBLinExpr ex35 = new GRBLinExpr();
    			ex35.addTerm(1.0, fp[i]);
    			ex35.addTerm(-1*M, h2[0][i]);
    			model.addConstr(ex35, GRB.LESS_EQUAL, 0.0, "eq41_"+i);
    			
       			GRBLinExpr ex36 = new GRBLinExpr();
    			ex36.addTerm(1.0, fp[i]);
    			ex36.addTerm(-1.0*(M+1), h2[0][i]);
       			ex36.addTerm(-1.0, Nkt[0][i]);
    			model.addConstr(ex36, GRB.GREATER_EQUAL, -1 - M, "eq42_"+i);
    		}
    		
    		///total tp, tn, fp, fn
    		GRBLinExpr ex37 = new GRBLinExpr();
    		ex37.addTerm(1.0, total_tp);
    		GRBLinExpr ex38 = new GRBLinExpr();
    		ex38.addTerm(1.0, total_tn);
    		GRBLinExpr ex39 = new GRBLinExpr();
    		ex39.addTerm(1.0, total_fn);
    		GRBLinExpr ex40 = new GRBLinExpr();
    		ex40.addTerm(1.0, total_fp);
    		
    		for (int i=0;i<numTreeNodeVars/2+1;i++)
    		{
    			ex37.addTerm(-1.0, tp[i]);
    			ex38.addTerm(-1.0, tn[i]);
    			ex39.addTerm(-1.0, fn[i]);
    			ex40.addTerm(-1.0, fp[i]);
    		}
    		
    		model.addConstr(ex37, GRB.EQUAL, 0.0, "eq43");
    		model.addConstr(ex38, GRB.EQUAL, 0.0, "eq44");
    		model.addConstr(ex39, GRB.EQUAL, 0.0, "eq45");
    		model.addConstr(ex40, GRB.EQUAL, 0.0, "eq46");
    		
    		
    		
    		
    		GRBLinExpr objf1 = new GRBLinExpr();
    		objf1.addTerm(2*(1-alpha),total_tp);
    		objf1.addTerm(-1*alpha, total_fp);
    		objf1.addTerm(-1*alpha,total_fn);
    		
    		model.setObjective(objf1, GRB.MAXIMIZE);
       		//model.setObjective(obj, GRB.MINIMIZE);
       		
    	}
    	catch(GRBException e)
    	{
      	    System.out.println("Error code: " + e.getErrorCode() + ". " +
                    e.getMessage());
    	}
    }
    
    public void updateObjective(double alphaVal)
    {
    	try
    	{
   		GRBLinExpr objf1 = new GRBLinExpr();
		objf1.addTerm(2*(1-alphaVal),total_tp);
		objf1.addTerm(-1*alphaVal, total_fp);
		objf1.addTerm(-1*alphaVal,total_fn);
		
		model.setObjective(objf1, GRB.MAXIMIZE);
    	}
    	catch(GRBException e)
    	{
     	    System.out.println("Error code: " + e.getErrorCode() + ". " +
                    e.getMessage());
    	}
    }
    
    public void provideWarmStart(DecisionTree object)
    {
    	try
    	{
    		for (int i=0;i<object.d.length;i++)
    		{
    			b[i].set(GRB.DoubleAttr.Start, object.b[i].get(GRB.DoubleAttr.X));
    			d[i].set(GRB.DoubleAttr.Start, object.d[i].get(GRB.DoubleAttr.X));
    			for (int j=0;j<X.numCols();j++)
    				a[j][i].set(GRB.DoubleAttr.Start, object.a[j][i].get(GRB.DoubleAttr.X));
    		}
    		
    	}
    	catch(Exception e)
    	{
    		System.out.println(e.getMessage());
    	}
    }
    
    
    
    public void provideWarmStartCART(String filename)
    {
		try 
		{
			DataSource source = new DataSource(filename);
			Instances data = source.getDataSet();
			if (data.classIndex() == -1)
				   data.setClassIndex(data.numAttributes() - 1);
		
			
			REPTree tree = new REPTree();
			String[] options = weka.core.Utils.splitOptions("-M "+Nmin+" -L "+treeDepth);
			tree.setOptions(options);
			tree.buildClassifier(data);
			String treeOutput = tree.toString();
			
			///create a Tree
			/*CARTTree warm_tree = new CARTTree();
			Scanner scanner = new Scanner(treeOutput);
			while (scanner.hasNextLine()) {
			  String lineparts[] = scanner.nextLine().split("\\s+");
			  
			}
			scanner.close();*/
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
    }
    
    public void learnWekaDecisionTrees(String filename)
    {
    	try
    	{
    		DataSource source = new DataSource(filename);
    		Instances data = source.getDataSet();
    		if (data.classIndex() == -1)
    			data.setClassIndex(data.numAttributes() - 1);
    		int seed = 1;
    		int folds=10;
    		
    		 Random rand = new Random(seed);   // create seeded number generator
    		 Instances randData = new Instances(data);   // create copy of original data
    		 randData.randomize(rand);         // randomize data with number generator
    		 randData.stratify(folds);
    		 
    		 double averageErrorRate=0;
    		 for (int i=0;i<folds;i++)
    		 {
    			 Instances train = randData.trainCV(folds,i);
    			 System.out.println("Num attr: "+train.numAttributes());
    			 Instances test = randData.testCV(folds,i);
    			 
    			 
    			 J48 tree = new J48();
    			 //tree.buildClassifier(train);
    			 
    			// REPTree tree = new REPTree();
    			 String[] options = weka.core.Utils.splitOptions("-O -U -B -M 2");
    			 //String[] options = weka.core.Utils.splitOptions("-M "+Nmin);
    			 tree.setOptions(options);
    			 tree.buildClassifier(train);
    			 String treeOutput = tree.toString();
    			 System.out.println(treeOutput);
    			 
    			 
    			 Evaluation evaluation = new Evaluation(train);
    			 //evaluation.useNoPriors();
    			 evaluation.evaluateModel(tree, test);
    			 System.out.println("Fold: "+i);
    			 System.out.println("Precision: "+evaluation.precision(0) +" Recall: "+evaluation.recall(0)+ " f measure: "+evaluation.fMeasure(0));
    			 System.out.println(evaluation.toSummaryString());
    			 averageErrorRate += evaluation.errorRate();
    			 
    			 System.out.println(evaluation.errorRate());
    		 }
    		 System.out.println("Average error rate: "+ averageErrorRate/folds);
    	}
    	catch(Exception e)
    	{
    		e.printStackTrace();
    	}
    }
    
    public void printSolution()
    {
    	try
    	{
    		System.out.println("Solution to be printed:");
    		FileWriter fw = new FileWriter(outputfile, true);
    		System.out.println("Number of d vars: "+d.length+" Number of b vars: "+b.length);
    		
    		for (int i=0;i<b.length;i++)
    		{
    			System.out.println("D Val: "+d[i].get(GRB.DoubleAttr.X)+" "+b[i].get(GRB.DoubleAttr.X));
    			fw.write("Node "+i+" : ");
    			System.out.print("Node "+i+" : ");
    			for (int j=0;j<Xtrain.numCols();j++)
    			{
    				if (a[j][i].get(GRB.DoubleAttr.X)==1)
    				{
    					fw.write("a is "+j+" | b is "+b[i].get(GRB.DoubleAttr.X));
    					System.out.println("a is "+j+" | b is "+b[i].get(GRB.DoubleAttr.X));
    				}
    			}
    		}
    		
    		//for (int i=0;i<l.length;i++)
    		//System.out.println("ckt: "+ckt[0][0]+" "+ckt[1][0]);
    		
    		double Lhat = 0;
    		for (int i=0;i<Xtrain.numRows();i++)
    			Lhat += Ytrain.get(i);
    		
    	   	String chosenFeatures="";
        	double accuracyVal=0, interpretabilityVal=0;
        	
        	double tp = total_tp.get(GRB.DoubleAttr.X);
        	double tn = total_tn.get(GRB.DoubleAttr.X);
        	double fp = total_fp.get(GRB.DoubleAttr.X);
        	double fn = total_fn.get(GRB.DoubleAttr.X);
        	
        	double precision = tp/(tp+fp);
        	double recall = tp/(tp+fn);
        	double f1 = 2*precision*recall/(precision+recall);
        	

       		for (int i=0;i<e.length;i++)
       		{
       			if (e[i].get(GRB.DoubleAttr.X)==1)
       				chosenFeatures += i+" , ";
       			interpretabilityVal += interpretabilityWeights[i]*e[i].get(GRB.DoubleAttr.X);
       		}
       		System.out.println("Precision: "+precision+" Recall: "+recall+" F1 Score: "+f1 + " Features used: "+ chosenFeatures);

           	//System.out.println("Lambda: "+lambda+" Accuracy: "+accuracyVal+" Interpretability: "+interpretabilityVal + " Features used: "+ chosenFeatures);
           	
           	//fw.write("Lambda: "+lambda+" Objective val: "+  model.get(GRB.DoubleAttr.ObjVal) +" Objective first time: " + (accuracyVal/Lhat) +"Accuracy: "+accuracyVal+" Interpretability: "+interpretabilityVal + " Features used: "+ chosenFeatures+" Optimality Gap: "+ model.get(GRB.DoubleAttr.MIPGap) +"\n");
        	//fw.close();
    	}
    	catch(GRBException e)
    	{
        System.out.println("Error code: " + e.getErrorCode() + ". " +
                    e.getMessage());
    	}
    	catch(IOException e)
    	{
      	System.out.println(e.getMessage());
    	}
    	
    }
    
    public double[] evaluateClassifieronTestSet()
    {
    	double result[] = new double[4];
    	try
    	{
    		
    		double Lhat = 0;
    		for (int i=0;i<Xtest.numRows();i++)
    			Lhat += Ytest.get(i);
    		System.out.println("Lhat on test set: "+ Lhat);
    		int numTreeNodeVars = (int) (Math.pow(2, treeDepth+1) - 1);
    		
    		double misclassified = 0;
    		double tp=0,fp=0,tn=0,fn=0;
    		for (int i=0;i<Xtest.numRows();i++)
    		{
    			int currentNode = 0;
    			while (currentNode<d.length)
    			{
    				if (d[currentNode].get(GRB.DoubleAttr.X)<1.0)//no split
    				{
    					currentNode = currentNode*2+2;///go to right in case of no split
    				}
    				else
    				{
    				for (int j=0;j<Xtest.numCols();j++)
        			{
        				if (a[j][currentNode].get(GRB.DoubleAttr.X)>1-numerical_instability_epsilon)
        				{
        					if (Xtest.get(i, j) < b[currentNode].get(GRB.DoubleAttr.X))
        						currentNode = currentNode*2 + 1;//left child
        					else
        						currentNode = currentNode*2 + 2;//right child
        					break;
        				}
        			}
    				}
    			}
    			
    			int leaf = currentNode - numTreeNodeVars/2;
    			int currPrediction = ckt[0][leaf].get(GRB.DoubleAttr.X)>(1-numerical_instability_epsilon)?0:1;
    			misclassified += Math.abs(currPrediction - Ytest.get(i));
    			if (currPrediction==1)
    			{
    				if (Ytest.get(i)==1)
    					tp++;
    				else if (Ytest.get(i)==0)
    					fp++;
    			}
    			else if (currPrediction==0)
    			{
    				if (Ytest.get(i)==1)
    					fn++;
    				else if (Ytest.get(i)==0)
    					tn++;
    			}
    		}
    		double precision=-1;
    		if (tp+fp==0)
    			precision=0;
    		else
    			precision = tp/(tp+fp);
    		double recall = tp/(tp+fn);
    		double f1 = 2*precision*recall/(precision+recall);
    		result[0]=precision;
    		result[1]=recall;
    		result[2]=f1;
    		result[3]=misclassified/Xtest.numRows();
    		return result;
    	}
    	catch(GRBException e)
    	{
    		System.out.println("Error code: " + e.getErrorCode() + ". " +
                    e.getMessage());
    	}
    	return result;
    }
}
