package models;

import java.io.Externalizable;
import java.io.FileWriter;
import java.io.IOException;

import org.ejml.data.DenseMatrix64F;
import org.ejml.ops.MatrixIO;
import org.ejml.simple.SimpleMatrix;

import gurobi.GRB;
import gurobi.GRBEnv;
import gurobi.GRBException;
import gurobi.GRBLinExpr;
import gurobi.GRBModel;
import gurobi.GRBVar;
import gurobi.GRBQuadExpr;

public class LinearRegression {
    public GRBEnv    env;
    public GRBModel  model;
    public GRBVar z[], beta[], minusz[], objterm[];
    public GRBQuadExpr obj;
    
    
    DenseMatrix64F dataset;
    SimpleMatrix data, X, Y;
    
    
    double interpretabilityWeights[];
    double lambda,lambda2;
    String outputfile;
    
    public LinearRegression(double lambda, double lambda2, double interpretabilityWeights[], String filename, String outputfile)
    {
    	try
    	{
    	this.outputfile = outputfile;
    	this.lambda = lambda;
    	this.lambda2 = lambda2;
    	this.interpretabilityWeights = interpretabilityWeights;
    	dataset = MatrixIO.loadCSV(filename);
    	data = SimpleMatrix.wrap(dataset);
    	
    	X = data.extractMatrix(0, data.numRows(), 0, data.numCols()-1);//leave last column for label
    	Y = data.extractVector(false, data.numCols()-1);///last label
    	
		env   = new GRBEnv("mip1.log");
		model = new GRBModel(env);
		specifyModel();
    	}
    	catch(IOException e)
    	{
  	    System.out.println(e.getMessage());
    	}
    	catch(GRBException e)
    	{
  	    System.out.println("Error code: " + e.getErrorCode() + ". " +
                  e.getMessage());
    	}
    }
    
    public void specifyModel()
    {
    	try
    	{
    		//create and add binary variables
    		z = new GRBVar[X.numCols()];
    		for (int i=0;i<z.length;i++)
    			z[i] = model.addVar(0.0, 1.0, 0.0, GRB.BINARY, "z"+i);
    		//System.out.println("Coola" + z.length);
    		
    		minusz = new GRBVar[X.numCols()];///introduce dummy variables to create SOS-1 constraints
    		for (int i=0;i<minusz.length;i++)
    			minusz[i] = model.addVar(0.0, 1.0, 0.0, GRB.BINARY, "minusz"+i);
    		
    		beta = new GRBVar[X.numCols()];
    		for (int i=0;i<beta.length;i++)
    			beta[i] = model.addVar(-1e20-1.0, 1e20+1.0, 0.0, GRB.CONTINUOUS, "beta"+i);//unconstrained objective variables
    		
    		objterm = new GRBVar[X.numRows()];
    		for (int i=0;i<objterm.length;i++)
    			objterm[i] = model.addVar(-1e20-1.0, 1e20+1.0, 0.0, GRB.CONTINUOUS, "objterm"+i);
    		
    		//Add Constraints
    		for (int i=0;i<z.length;i++)////constraint minusz = 1 - z
    		{
    			GRBLinExpr constr = new GRBLinExpr();
    			constr.addTerm(1.0, z[i]);
    			constr.addTerm(1.0, minusz[i]);
    			model.addConstr(constr, GRB.EQUAL, 1.0, "c"+i);
    		}
    		
    		//constraints for objterms
    		for (int i=0;i<objterm.length;i++)
    		{
    			GRBLinExpr L2term = new GRBLinExpr();
    			for (int j=0;j<X.numCols();j++)
    				L2term.addTerm(X.get(i,j), beta[j]);
    			L2term.addConstant(-1*Y.get(i));
    			
    			L2term.addTerm(-1.0, objterm[i]);
    			
    			model.addConstr(L2term, GRB.EQUAL, 0.0, "obj"+i);
    		}
    		
    		//SOS-1 constraints
    		double soswt[] = {1,2};
    		for (int i=0;i<z.length;i++)
    		{
    			GRBVar sosv1[] = {minusz[i], beta[i]};
    			model.addSOS(sosv1, soswt, GRB.SOS_TYPE1);
    		}
    		
    		
    		///objective function
    		obj = new GRBQuadExpr();
    		//term for accuracy
    		for (int i=0;i<X.numRows();i++)
    			//obj.addTerm(0, objterm[i], objterm[i]);
    			obj.addTerm(0.5, objterm[i], objterm[i]);
 
    		//term for interpretability
    		GRBLinExpr interpretabilityterm = new GRBLinExpr();
    		for (int i=0;i<z.length;i++)
    			interpretabilityterm.addTerm(-1*lambda*interpretabilityWeights[i], z[i]);
    		
    		obj.add(interpretabilityterm);
    		
    		GRBLinExpr ter = new GRBLinExpr();
    		for (int i=0;i<z.length;i++)
    			ter.addTerm(lambda2, z[i]);
    		
    		obj.add(ter);
    		model.setObjective(obj, GRB.MINIMIZE);
    	}
    	catch(GRBException e)
    	{
      	System.out.println("Error code: " + e.getErrorCode() + ". " +
                    e.getMessage());	
    	}
    }
    
    public void printSolution()
    {
    	try
    	{
    	String chosenFeatures="";
    	double accuracyVal=0, interpretabilityVal=0;
    	for (int i=0;i<z.length;i++)
    	{
    		interpretabilityVal += interpretabilityWeights[i]*(z[i].get(GRB.DoubleAttr.X));
    		if (z[i].get(GRB.DoubleAttr.X)==1.0)
    			chosenFeatures += i+" , ";
    		
    		System.out.println("Lambda: "+lambda);
    		System.out.println(z[i].get(GRB.StringAttr.VarName)
                + " " +z[i].get(GRB.DoubleAttr.X));
    		System.out.println(beta[i].get(GRB.StringAttr.VarName)
                + " " +beta[i].get(GRB.DoubleAttr.X));
    	}
       	System.out.println("Obj: " + model.get(GRB.DoubleAttr.ObjVal));
    	
       	
       	for (int i=0;i<objterm.length;i++)
       	{
       		accuracyVal += objterm[i].get(GRB.DoubleAttr.X) * objterm[i].get(GRB.DoubleAttr.X);
       	}
       
       	///write to file
       	System.out.println("Lambda: "+lambda+" Lambda2: "+ lambda2 +" Accuracy: "+accuracyVal+" Interpretability: "+interpretabilityVal + " Features used: "+ chosenFeatures);
       	FileWriter fw = new FileWriter(outputfile, true);
       	fw.write("Lambda: "+lambda+" Objective val: "+  model.get(GRB.DoubleAttr.ObjVal) +" Accuracy: "+accuracyVal+" Interpretability: "+interpretabilityVal + " Features used: "+ chosenFeatures+"\n");
    	fw.close();
    	}
    	catch(GRBException e)
    	{
        System.out.println("Error code: " + e.getErrorCode() + ". " +
                    e.getMessage());
    	}
    	catch(IOException e)
    	{
      	System.out.println(e.getMessage());
    	}
    }
}
