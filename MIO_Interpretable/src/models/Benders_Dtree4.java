package models;

import gurobi.GRBException;
import gurobi.GRBVar;

import java.io.IOException;

import org.ejml.data.DenseMatrix64F;
import org.ejml.ops.MatrixIO;
import org.ejml.simple.SimpleMatrix;

import gurobi.GRB;
import gurobi.GRBCallback;


public class Benders_Dtree4 extends GRBCallback {
	private GRBVar[] vars;
	
	public Benders_Dtree4(GRBVar[] var)
	{
		vars=var;
	}
	
	protected void callback()
	{
		try
		{
			if (where == GRB.Callback.MIPNODE)
			{
				System.out.println("Explored node count: "+getDoubleInfo(GRB.Callback.MIPNODE_NODCNT));
			}
		}
		catch(GRBException e)
		{
			e.printStackTrace();
		}
	}
	
	public static void main(String args[])
	{
		String basedir = "/Users/amulyayadav/Dropbox/Work/PostAAMAS/PhebeWork/Bottleneck/";
	 	int Depth=2;
	 	double timeLimit = 1800;
	 	DenseMatrix64F dataset;
	    SimpleMatrix originalData, data, train, test;
	    
        try
        {
            
            /*String basedir = args[0];
            int Depth=Integer.parseInt(args[1]);
            double timeLimit = Double.parseDouble(args[2]);*/
            
        	
            
            dataset = MatrixIO.loadCSV(basedir+"masterdataset-mip.csv");

            originalData = SimpleMatrix.wrap(dataset);


            data = originalData;


            train = data;
            ////train and test constructed
            
            
            ///running code  
            Dtree4 actual = new Dtree4(Depth, train);
            actual.provideWarmStartCART(basedir+"masterdataset-mip-weka.csv");
            actual.model.getEnv().set(GRB.IntParam.Method, 0);
            actual.model.getEnv().set(GRB.IntParam.BranchDir, 1);
            actual.model.getEnv().set(GRB.IntParam.NodeMethod, 0);
            
            GRBVar[] vars = actual.model.getVars();
            Benders_Dtree4 cb   = new Benders_Dtree4(vars);
            actual.model.setCallback(cb);
            
            actual.model.optimize();
        }
        catch(IOException e)
        {
        	e.printStackTrace();
        } catch (GRBException e) {
          // TODO Auto-generated catch block
		  e.printStackTrace();
		}
	}
	
	
	
	
	
}