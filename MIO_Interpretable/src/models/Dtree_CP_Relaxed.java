package models;

import java.util.ArrayList;

import org.ejml.simple.SimpleMatrix;


import ilog.cp.*;
import ilog.concert.*;

import helpers.CPProblem;

public class Dtree_CP_Relaxed extends CPProblem{
	
	IloIntVar lin[][], x[][], z[], a[][], w[][], tp[], tn[];
	
	IloNumVar yplus[][], yminus[][], b[];
	
    SimpleMatrix train, test;
    SimpleMatrix  Xtrain, Ytrain, Xtest, Ytest;
    
    int treeDepth, Nmin;
    double interpretabilityWeights[];
    double lambda,lambda2;
    String outputfile;
    
    static double smallM = 0.001;
    static double bigM;
    
    static double numerical_instability_epsilon = 0.001;

	public Dtree_CP_Relaxed(String filename, int treeDepth, int Nmin, double lambda, double interpretabilityWeights[], String outputfile, double fractionTrainSet, SimpleMatrix train, SimpleMatrix test)
	{
    	try
    	{	
    		this.treeDepth = treeDepth;
    		this.Nmin = Nmin;
    	  	this.lambda = lambda;
        	this.interpretabilityWeights = interpretabilityWeights;
        	this.outputfile = outputfile;
        	
        	
        	
        	this.train = train;
        	this.test = test;
       		Xtrain = train.extractMatrix(0, train.numRows(), 1, train.numCols()-1);//leave first col..last column for label
    		Ytrain = train.extractVector(false, train.numCols()-1);///last label
    		
    		bigM = Xtrain.numRows();
    		
      		/*Xtest = test.extractMatrix(0, test.numRows(), 1, test.numCols()-1);//leave first col..leave last column for label
    		Ytest = test.extractVector(false, test.numCols()-1);///last label*/
    		
    		//env   = new GRBEnv("mip1.log");
    		//model = new GRBModel(env);
    		
    		
    		
    		specifyModel();
    	}
    	catch(Exception e)
    	{
    		System.out.println(e.getMessage());
    	}
	}
	
    public void specifyModel()
    {
    	try
    	{
    		int numTreeNodeVars = (int) (Math.pow(2, treeDepth+1) - 1);
    		
    		lin = new IloIntVar[Xtrain.numRows()][numTreeNodeVars/2+1];
    		x = new IloIntVar[Xtrain.numRows()][numTreeNodeVars/2+1];
    		z = new IloIntVar[numTreeNodeVars/2+1];
    		for (int l=0; l<numTreeNodeVars/2+1; l++)
    		{
    			z[l] = this.addDecisionVar("z_"+l, 0, 1);
    			for (int i=0;i<Xtrain.numRows();i++)
    			{
    				x[i][l] = this.addDecisionVar("x_"+i+"_"+l, 0, 1); 
    				lin[i][l] = this.addDecisionVar("lin_"+i+"_"+l, 0, 1);
    			}
    		}
    		
    		yplus = new IloNumVar[Xtrain.numRows()][numTreeNodeVars/2];
    		yminus = new IloNumVar[Xtrain.numRows()][numTreeNodeVars/2];
    		for (int i=0;i<Xtrain.numRows();i++)
    		{
    			for (int n=0;n<numTreeNodeVars/2;n++)
    			{
    				
    				yplus[i][n] = this.addNumericDecisionVar("yplus_"+i+"_"+n, 0.0, 1.0);
    				yminus[i][n] = this.addNumericDecisionVar("yminus_"+i+"_"+n, 0.0, 1.0);
    			}
    		}
    		
    		
    		a = new IloIntVar[numTreeNodeVars/2][Xtrain.numCols()];
    		b = new IloNumVar[numTreeNodeVars/2];
    		
    		for (int n=0;n<numTreeNodeVars/2;n++)
    		{
    			b[n] =  this.addNumericDecisionVar("b_"+n, 0.0, 1.0);
    			for (int f=0;f<Xtrain.numCols();f++)
    			{
    				a[n][f] = this.addDecisionVar("a_"+n+"_"+f, 0, 1); 
    			}
    		}	
    		
    		w = new IloIntVar[Xtrain.numRows()][numTreeNodeVars/2];
    		for (int i=0;i<Xtrain.numRows();i++)
			{
    			for (int n=0;n<numTreeNodeVars/2;n++)
    			{
    				w[i][n] = this.addDecisionVar("w_"+i+"_"+n, 0, 1); 
    			}
    		}
    		
    		tp = new IloIntVar[Xtrain.numRows()];
    		tn = new IloIntVar[Xtrain.numRows()];
    		
    		for (int i=0;i<Xtrain.numRows();i++)
    		{
    			tp[i] = this.addDecisionVar("tp_"+i, 0, 1);  
    			tn[i] = this.addDecisionVar("tn_"+i, 0, 1);
    		}
    		
    		//model.update();
    		
    		for (int i=0;i<Xtrain.numRows();i++)
    		{
    			//EQ: each datapoint has to be assigned to exactly one leaf
    			///EQUATION 2
    			IloNumExpr ex1 = this.sumVarArray(x[i]);
    			System.out.println(ex1.toString());
    			this.addConstraint(ex1, 1);
    		}
    		
    		for (int n=0;n<numTreeNodeVars/2;n++)
    		{
    			///EQ: at each branch node...branch only on one variable
    			///EQUATION 2
    			IloNumExpr ex2 = this.sumVarArray(a[n]);
    			System.out.println(ex2.toString());
    			this.addConstraint(ex2, 1);
    			
    			
    			int rightNode = 2*n+2;
    			ArrayList<Integer> rightIndices = new ArrayList<Integer>();
    			ArrayList<Integer> leftIndices = new ArrayList<Integer>();
    			for (int l=0;l<numTreeNodeVars/2+1;l++)
    			{
    				int currInd = l+numTreeNodeVars/2;
    				while(currInd>rightNode)
    				{
    					if (currInd%2==0)
    						currInd = currInd/2-1;
    					else
    						currInd = currInd/2;
    					
    					if (currInd==rightNode || currInd==rightNode-1)
    						break;
    				}
    				if (currInd==rightNode)
    					rightIndices.add(l);
    				else if (currInd == rightNode - 1)
    					leftIndices.add(l);
    			}
    			
    			for (int i=0;i<Xtrain.numRows();i++)
    			{
    				///EQUATION 6
    				IloNumExpr ex11 = this.cp.sum(yminus[i][n], cp.sum(yplus[i][n], cp.prod(smallM, w[i][n])));
    				System.out.println(ex11.toString());
    				this.addLBoundConstraint(ex11, smallM);
    				

    				for (int l=0;l<rightIndices.size();l++)
    				{
    					//EQUATION 4
    					IloNumExpr ex6 = this.cp.sum(x[i][rightIndices.get(l)], w[i][n]);
    					this.addLEConstraint(ex6, 1);
    				}
    				
    				for (int l=0;l<leftIndices.size();l++)
    				{
    					//EQUATION 5
    					IloNumExpr ex12 = this.cp.sum(x[i][leftIndices.get(l)], cp.prod(-1, w[i][n]));
    					System.out.println(ex12.toString());
    					this.addLEConstraint(ex12, 0);
    				}
    			}
    		}
    		
    		for (int i=0;i<Xtrain.numRows();i++)
    		{
    			for (int n=0;n<numTreeNodeVars/2;n++)
    			{	
    				SimpleMatrix row = Xtrain.extractVector(true, i);
    				double rowVals[] = new double[Xtrain.numCols()];
    				for (int g=0;g<Xtrain.numCols();g++)
    					rowVals[g] = Xtrain.get(i,g);
    				
    				IloNumExpr ex3_part = this.scalProdSumArray(rowVals, a[n]);
    				IloNumExpr ex3 = this.cp.sum(ex3_part, cp.diff(yplus[i][n], cp.sum(b[n], yminus[i][n])));
    				System.out.println(ex3.toString());
    				///EQUATION 7
    				this.addConstraint(ex3,  0);
    				
    				
    				IloNumExpr ex4 = this.cp.diff(yplus[i][n], w[i][n]);
    				///EQUATION 8
    				this.addLEConstraint(ex4, 0);
    				
    				
    				IloNumExpr ex5 = this.cp.sum(yminus[i][n], w[i][n]);
    				///EQUATION 9
    				this.addLEConstraint(ex5, 1);
    			}
    		}
    		
    		for (int i=0;i<Xtrain.numRows();i++)
    		{
    			for (int l=0;l<numTreeNodeVars/2+1;l++)
    			{
    				IloNumExpr ex7 = this.cp.diff(lin[i][l], z[l]);
    				this.addLEConstraint(ex7, 0);
    				
    				IloNumExpr ex8 = this.cp.diff(lin[i][l], x[i][l]);
    				this.addLEConstraint(ex8, 0);
    				
    				IloNumExpr ex9 = this.cp.diff(lin[i][l], cp.sum(x[i][l], z[l]));
    				this.addLBoundConstraint(ex9, -1);
    				//EQUATION 10
    			}
    			
    			
    			////TO DO: CHECK NUMERICAL
    			double val;
    			if (Ytrain.get(i)>1-numerical_instability_epsilon)
    			{	
    				IloNumExpr ex10_part = this.sumVarArray(lin[i]);
    				IloNumExpr ex10 = this.cp.diff(tp[i], ex10_part);
    				
    				this.addConstraint(ex10, 0);
    			}
    			
    			if (Ytrain.get(i)<numerical_instability_epsilon)
    			{
    				IloNumExpr ex31_part1 = this.sumVarArray(lin[i]);
    				IloNumExpr ex31_part2 = this.sumVarArray(x[i]);
    				
    				IloNumExpr ex31_part3 = this.cp.diff(ex31_part1, ex31_part2);
    				
    				IloNumExpr ex31 = this.cp.sum(tn[i], ex31_part3);
    				
    				this.addConstraint(ex31,0);
    			}
    		}
    		

        		
    		///TO DO:CHECK NUMERICAL
    		this.objectiveType=OBJECTIVE_TYPE.MAX;
    		
    		IloIntExpr objf1 = this.cp.constant(0);
    		for (int i=0;i<Xtrain.numRows();i++)
    		{
    			if (Ytrain.get(i)>1-numerical_instability_epsilon)
    				objf1 = this.cp.sum(objf1, tp[i]);
    			else if (Ytrain.get(i)<numerical_instability_epsilon)
    				objf1 = this.cp.sum(objf1, tn[i]);
    		}
    		System.out.println(objf1.toString());
    		
    		this.updateObjective(objf1);
    	}
    	catch(IloException e)
    	{
    		System.out.println(e.getMessage());
    		e.printStackTrace();
    	}
    }
}
