package models;

import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

import org.ejml.simple.SimpleMatrix;

import gurobi.GRBVar;
import weka.classifiers.trees.REPTree;
import weka.classifiers.trees.REPTree.Tree;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;
import gurobi.GRB;
import gurobi.GRBEnv;
import gurobi.GRBException;
import gurobi.GRBLinExpr;
import gurobi.GRBQuadExpr;
import gurobi.GRBModel;

public class SingleMIP {
    public GRBEnv    env;
    public GRBModel  model;
    
    public GRBVar x[][], a[][], b[], z[], w[][], yplus[][], yminus[][], tp[], tn[];// fp[], fn[], lin[][];// hplus[], hminus[];
    public GRBVar lin[][];
    
    //Ghanem and wu
    public GRBVar y[][];
    
    SimpleMatrix train, test;
    SimpleMatrix  Xtrain, Ytrain, Xtest, Ytest;
    
    int treeDepth, Nmin;
    double interpretabilityWeights[];
    double lambda,lambda2;
    String outputfile;
    
    static double smallM;
    static double bigM;
    
    static ArrayList<Integer> splitFeat = new ArrayList<Integer>();
    
    static double numerical_instability_epsilon = 0.001;
    
    public SingleMIP(String filename, int treeDepth, int Nmin, double lambda, double interpretabilityWeights[], String outputfile, double fractionTrainSet, SimpleMatrix train, SimpleMatrix test, ArrayList<Integer> arr)
    {
    	try
    	{	
    		this.treeDepth = treeDepth;
    		this.Nmin = Nmin;
    	  	this.lambda = lambda;
        	this.interpretabilityWeights = interpretabilityWeights;
        	this.outputfile = outputfile;
        	
        	for (int i=0;i<arr.size();i++)
        	{
        		this.splitFeat.add(arr.get(i));
        	}
        	
        	
        	
        	this.train = train;
        	this.test = test;
       		Xtrain = train.extractMatrix(0, train.numRows(), 1, train.numCols()-1);//leave first col..last column for label
    		Ytrain = train.extractVector(false, train.numCols()-1);///last label
    		
    		bigM = Xtrain.numRows();
    		
      		/*Xtest = test.extractMatrix(0, test.numRows(), 1, test.numCols()-1);//leave first col..leave last column for label
    		Ytest = test.extractVector(false, test.numCols()-1);///last label*/
    		
    		env   = new GRBEnv("mip1.log");
    		model = new GRBModel(env);
    		
    		
    		
    		specifyModel();
    	}
    	catch(GRBException e)
    	{
    		System.out.println("Error code: " + e.getErrorCode() + ". " +
                    e.getMessage());
    	}
    }
    
    public void specifyModel()
    {
    	try
    	{
    		int numTreeNodeVars = (int) (Math.pow(2, treeDepth+1) - 1);
    		
            double epsilon[] = new double[Xtrain.numCols()];
            double feature[] = new double[Xtrain.numRows()];
            double max_epsilon_entry=-1;
            int min_epsilon_index=-1;
            double min_epsilon_entry=10000;
            for (int i=0;i<Xtrain.numCols();i++)
            {
                    for (int j=0;j<Xtrain.numRows();j++)
                            feature[j] = Xtrain.get(j, i);
                    Arrays.sort(feature);
                    double minVal=10000;
                    for (int j=feature.length-1;j>0;j--)
                    {
                            double temp = feature[j]-feature[j-1];
                            if (temp!=0 && temp<minVal)
                                    minVal = temp;
                    }
                    epsilon[i] = minVal;
                    if (epsilon[i]<min_epsilon_entry)
                    {
                            min_epsilon_index = i;
                            min_epsilon_entry = epsilon[i];
                    }
                    if (epsilon[i]>max_epsilon_entry)
                            max_epsilon_entry = epsilon[i];
            }
            smallM = min_epsilon_entry;
            System.out.println("Min epsilon"+ min_epsilon_entry + " "+min_epsilon_index);
    		
    		lin = new GRBVar[Xtrain.numRows()][numTreeNodeVars/2+1];
    		x = new GRBVar[Xtrain.numRows()][numTreeNodeVars/2+1];
    		z = new GRBVar[numTreeNodeVars/2+1];
    		for (int l=0; l<numTreeNodeVars/2+1; l++)
    		{
    			z[l] = model.addVar(0.0, 1.0, 0.0, GRB.BINARY, "z_"+l);
    			z[l].set(GRB.IntAttr.BranchPriority, 7);
    			for (int i=0;i<Xtrain.numRows();i++)
    			{
    				x[i][l] = model.addVar(0.0, 1.0, 0.0, GRB.CONTINUOUS, "x_"+i+"_"+l);
    				//x[i][l] = model.addVar(0.0, 1.0, 0.0, GRB.BINARY, "x_"+i+"_"+l);
    				//x[i][l].set(GRB.IntAttr.BranchPriority, 8);
    				lin[i][l] = model.addVar(0.0, 1.0, 0.0, GRB.BINARY, "lin_"+i+"_"+l);
    				lin[i][l].set(GRB.IntAttr.BranchPriority, 6);
    				//x[j][i].set(GRB.IntAttr.BranchPriority, 1);
    			}
    		}
    		
    		yplus = new GRBVar[Xtrain.numRows()][numTreeNodeVars/2];
    		yminus = new GRBVar[Xtrain.numRows()][numTreeNodeVars/2];
    		for (int i=0;i<Xtrain.numRows();i++)
    		{
    			for (int n=0;n<numTreeNodeVars/2;n++)
    			{
    				yplus[i][n] = model.addVar(0.0, 1.0, 0.0, GRB.CONTINUOUS, "yplus_"+i+"_"+n);
    				yminus[i][n] = model.addVar(0.0, 1.0, 0.0, GRB.CONTINUOUS, "yminus_"+i+"_"+n);
    			}
    		}
    		
    		/*hplus = new GRBVar[numTreeNodeVars/2+1];
    		hminus = new GRBVar[numTreeNodeVars/2+1];
    		for (int i=0; i<numTreeNodeVars/2+1; i++)
    		{
    			hplus[i] = model.addVar(0.0, Xtrain.numRows(), 0.0, GRB.INTEGER, "hplus_"+i);
    			hminus[i] = model.addVar(0.0, Xtrain.numRows(), 0.0, GRB.INTEGER, "hminus_"+i);
    		}*/
    		
    		
    		a = new GRBVar[numTreeNodeVars/2][Xtrain.numCols()];
    		//y = new GRBVar[numTreeNodeVars/2][Xtrain.numCols()];
    		b = new GRBVar[numTreeNodeVars/2];
    		
    		for (int n=0;n<numTreeNodeVars/2;n++)
    		{	
    			b[n] = model.addVar(0.0, 1.0, 0.0, GRB.CONTINUOUS, "b_"+n);
    			//b[i].set(GRB.IntAttr.BranchPriority, Integer.MAX_VALUE - 1);
    			for (int f=0;f<Xtrain.numCols();f++)
    			{
    				if (splitFeat.get(n)==f)
    					a[n][f] = model.addVar(1.0, 1.0, 0.0, GRB.INTEGER, "a_"+n+"_"+f);
    				else
    					a[n][f] = model.addVar(0.0, 0.0, 0.0, GRB.INTEGER, "a_"+n+"_"+f);
    				//a[n][f].set(GRB.IntAttr.BranchPriority, 10);
    				
    				//y[n][f] = model.addVar(0.0, 1.0, 0.0, GRB.CONTINUOUS, "y_"+n+"_"+f);
    			}
    		}
    		
    		
    		
    		/*b[0] = model.addVar(0.62, 0.62, 0.0, GRB.CONTINUOUS, "b_"+0);
    		b[1] = model.addVar(0.75, 0.75, 0.0, GRB.CONTINUOUS, "b_"+1);
    		b[2] = model.addVar(0.5, 0.5, 0.0, GRB.CONTINUOUS, "b_"+2);
    		
    		for (int i=0;i<Xtrain.numCols();i++)
    		{
    			if (i==4)
    				a[i][0] = model.addVar(1.0, 1.0, 0.0, GRB.BINARY, "a_"+i+"_0");
    			else
    				a[i][0] = model.addVar(0.0, 0.0, 0.0, GRB.BINARY, "a_"+i+"_0");
    		}
    		
    		for (int i=0;i<Xtrain.numCols();i++)
    		{
    			if (i==0)
    				a[i][1] = model.addVar(1.0, 1.0, 0.0, GRB.BINARY, "a_"+i+"_1");
    			else
    				a[i][1] = model.addVar(0.0, 0.0, 0.0, GRB.BINARY, "a_"+i+"_1");
    		}
    		
    		for (int i=0;i<Xtrain.numCols();i++)
    		{
    			if (i==19)
    				a[i][2] = model.addVar(1.0, 1.0, 0.0, GRB.BINARY, "a_"+i+"_2");
    			else
    				a[i][2] = model.addVar(0.0, 0.0, 0.0, GRB.BINARY, "a_"+i+"_2");
    		}*/
    		
    		
    		w = new GRBVar[Xtrain.numRows()][numTreeNodeVars/2];
    		for (int i=0;i<Xtrain.numRows();i++)
			{
    			for (int n=0;n<numTreeNodeVars/2;n++)
    			{
    			
    				w[i][n] = model.addVar(0.0, 1.0, 0.0, GRB.BINARY, "w_"+i+"_"+n);
    				w[i][n].set(GRB.IntAttr.BranchPriority, 9);
    			}
    		}
    		
    		tp = new GRBVar[Xtrain.numRows()];
    		tn = new GRBVar[Xtrain.numRows()];
    		
    		for (int i=0;i<Xtrain.numRows();i++)
    		{
    			tp[i] = model.addVar(0.0, 1.0, 0.0, GRB.BINARY, "tp_"+i);
    			tp[i].set(GRB.IntAttr.BranchPriority, 5);
    			tn[i] = model.addVar(0.0, 1.0, 0.0, GRB.BINARY, "tn_"+i);
    			tn[i].set(GRB.IntAttr.BranchPriority, 5);
    		}
    		
    		//////////////
    		/*fp = new GRBVar[Xtrain.numRows()];
    		fn = new GRBVar[Xtrain.numRows()];
    		
    		for (int i=0;i<Xtrain.numRows();i++)
    		{
    			fp[i] = model.addVar(0.0, 1.0, 0.0, GRB.BINARY, "fp_"+i);
    			fn[i] = model.addVar(0.0, 1.0, 0.0, GRB.BINARY, "fn_"+i);
    		}*/
    		///////////////////
    		
    		model.update();
    		
    		///EQUATIONS LEFT OUT:
    		///eq21
    		
    		//////////////////////NEW EQUATIONS Ghanem and Wu
    		/*for (int n=0;n<numTreeNodeVars/2;n++)
    		{
    			for (int f=0;f<Xtrain.numCols();f++)
    			{
    				GRBLinExpr exq1 = new GRBLinExpr();
    				exq1.addTerm(1.0, a[n][f]);
    				exq1.addTerm(-1.0, y[n][f]);
    				model.addConstr(exq1, GRB.LESS_EQUAL , 0.5, "eq-wu_"+n+"_"+f);
    				
    				GRBLinExpr exq3 = new GRBLinExpr();
    				exq3.addTerm(1.0, a[n][f]);
    				exq3.addTerm(1.0, y[n][f]);
    				model.addConstr(exq3, GRB.GREATER_EQUAL , 0.5, "eq-wu3_"+n+"_"+f);
    			}
    		}*/
    		

    		/*for (int n=0;n<numTreeNodeVars/2;n++)
    		{
    			GRBQuadExpr exq2 = new GRBQuadExpr();
    			for (int f=0;f<Xtrain.numCols();f++)
    			{
    				exq2.addTerm(1.0, a[n][f], a[n][f]);
    				exq2.addTerm(-1.0, a[n][f]);
    			}
				model.addQConstr(exq2, GRB.LESS_EQUAL , smallM, "eq-wu2_"+n);
    		}*/
    		
    		//////////////////////////////////
    		
    		
    		for (int i=0;i<Xtrain.numRows();i++)
    		{
    			//EQ: each datapoint has to be assigned to exactly one leaf
    			GRBLinExpr ex1 = new GRBLinExpr();
    			for (int l=0;l<numTreeNodeVars/2+1;l++)
    				ex1.addTerm(1.0, x[i][l]);
    			
    			//EQUATION 2
    			model.addConstr(ex1, GRB.EQUAL, 1.0, "eq1_"+i);
    		}
    		
    		for (int n=0;n<numTreeNodeVars/2;n++)
    		{
    			///EQ: at each branch node...branch only on one variable
    			GRBLinExpr ex2 = new GRBLinExpr();
    			//GRBLinExpr ex21 = new GRBLinExpr();
    			for (int f=0;f<Xtrain.numCols();f++)
    			{
    				ex2.addTerm(1.0, a[n][f]);
    				//ex21.addTerm(1.0, a[j][i]);
    			}
    			///EQUATION 3
    			model.addConstr(ex2, GRB.EQUAL, 1.0, "eq2_"+n);
    			
    			/*ex21.addTerm(-1.0, b[i]);
    			model.addConstr(ex21, GRB.GREATER_EQUAL, 0.0, "eq21_"+i);*/
    			
    			int rightNode = 2*n+2;
    			ArrayList<Integer> rightIndices = new ArrayList<Integer>();
    			ArrayList<Integer> leftIndices = new ArrayList<Integer>();
    			for (int l=0;l<numTreeNodeVars/2+1;l++)
    			{
    				int currInd = l+numTreeNodeVars/2;
    				while(currInd>rightNode)
    				{
    					if (currInd%2==0)
    						currInd = currInd/2-1;
    					else
    						currInd = currInd/2;
    					
    					if (currInd==rightNode || currInd==rightNode-1)
    						break;
    				}
    				if (currInd==rightNode)
    					rightIndices.add(l);
    				else if (currInd == rightNode - 1)
    					leftIndices.add(l);
    			}
    			
    			for (int i=0;i<Xtrain.numRows();i++)
    			{
    				GRBLinExpr ex11 = new GRBLinExpr();
    				ex11.addTerm(1.0, yminus[i][n]);
    				ex11.addTerm(1.0, yplus[i][n]);
    				ex11.addTerm(smallM, w[i][n]);
    				///EQUATION 6
    				model.addConstr(ex11, GRB.GREATER_EQUAL, smallM, "eq11_"+i+"_"+n);
    				
    				for (int l=0;l<rightIndices.size();l++)
    				{
    					GRBLinExpr ex6 = new GRBLinExpr();
    					ex6.addTerm(1.0, x[i][rightIndices.get(l)]);
    					ex6.addTerm(1.0, w[i][n]);
    					///EQUATION 4
    					model.addConstr(ex6, GRB.LESS_EQUAL, 1.0, "eq6_"+n+"_"+i+"_"+l);
    				}
    				
    				for (int l=0;l<leftIndices.size();l++)
    				{
    					GRBLinExpr ex12 = new GRBLinExpr();
    					ex12.addTerm(1.0, x[i][leftIndices.get(l)]);
    					ex12.addTerm(-1.0, w[i][n]);
    					///EQUATION 5
    					model.addConstr(ex12, GRB.LESS_EQUAL, 0.0, "eq12_"+n+"_"+i+"_"+l);
    				}
    			}
    			
    			
    		}
    		
    		for (int i=0;i<Xtrain.numRows();i++)
    		{
    			for (int n=0;n<numTreeNodeVars/2;n++)
    			{
    				
    				GRBLinExpr ex3 = new GRBLinExpr();
    				for (int f=0;f<Xtrain.numCols();f++)
    					ex3.addTerm(Xtrain.get(i, f), a[n][f]);
    				
    				ex3.addTerm(-1.0, b[n]);
    				ex3.addTerm(1.0, yplus[i][n]);
    				ex3.addTerm(-1.0, yminus[i][n]);
    				///EQUATION 7
    				model.addConstr(ex3, GRB.EQUAL, 0.0, "eq3_"+i+"_"+n);
    				
    				GRBLinExpr ex4 = new GRBLinExpr();
    				ex4.addTerm(1.0, yplus[i][n]);
    				ex4.addTerm(-1.0, w[i][n]);
    				///EQUATION 8
    				model.addConstr(ex4, GRB.LESS_EQUAL, 0.0, "eq4_"+i+"_"+n);
    				
    				GRBLinExpr ex5 = new GRBLinExpr();
    				ex5.addTerm(1.0, yminus[i][n]);
    				ex5.addTerm(1.0, w[i][n]);
    				///EQUATION 9
    				model.addConstr(ex5, GRB.LESS_EQUAL, 1.0, "eq5_"+i+"_"+n);
    			}
    		}
    		
    		for (int i=0;i<Xtrain.numRows();i++)
    		{
    			for (int l=0;l<numTreeNodeVars/2+1;l++)
    			{
    				GRBLinExpr ex7 = new GRBLinExpr();
    				ex7.addTerm(1.0, lin[i][l]);
    				ex7.addTerm(-1.0, z[l]);
    				model.addConstr(ex7, GRB.LESS_EQUAL, 0.0, "eq7_"+i+"_"+l);
    				
    				GRBLinExpr ex8 = new GRBLinExpr();
    				ex8.addTerm(1.0, lin[i][l]);
    				ex8.addTerm(-1.0, x[i][l]);
    				model.addConstr(ex8, GRB.LESS_EQUAL, 0.0, "eq8_"+i+"_"+l);
    				
    				GRBLinExpr ex9 = new GRBLinExpr();
    				ex9.addTerm(1.0, lin[i][l]);
    				ex9.addTerm(-1.0, x[i][l]);
    				ex9.addTerm(-1.0, z[l]);
    				model.addConstr(ex9, GRB.GREATER_EQUAL, -1.0, "eq9_"+i+"_"+l);
    				//EQUATION 10
    			}
    			
    			
    			////TO DO: CHECK NUMERICAL
    			double val;
    			if (Ytrain.get(i)>1-numerical_instability_epsilon)
    			{
    				GRBLinExpr ex10 = new GRBLinExpr();
    				ex10.addTerm(1.0, tp[i]);
    				
    				for (int l=0;l<numTreeNodeVars/2+1;l++)
        				ex10.addTerm(-1.0, lin[i][l]);
    				model.addConstr(ex10, GRB.EQUAL, 0.0, "eq10_"+i);
    			}
    			
    			if (Ytrain.get(i)<numerical_instability_epsilon)
    			{
    				GRBLinExpr ex31 = new GRBLinExpr();
    				ex31.addTerm(1.0, tn[i]);
    				
    				for (int l=0;l<numTreeNodeVars/2+1;l++)
    				{	
    					ex31.addTerm(-1.0, x[i][l]);
        				
        				ex31.addTerm(1.0, lin[i][l]);
    				}
    				model.addConstr(ex31, GRB.EQUAL, 0.0, "eq31_"+i);
    			}
    			
    			
    			/*GRBLinExpr ex10 = new GRBLinExpr();
    			ex10.addTerm(Ytrain.get(i), tp[i]);
    			
    			ex10.addTerm(1-Ytrain.get(i), tn[i]);
    			for (int l=0;l<numTreeNodeVars/2+1;l++)
    			{
    				ex10.addTerm(-1.0*Ytrain.get(i), lin[i][l]);
    				
    				ex10.addTerm(-1.0*(1-Ytrain.get(i)), x[i][l]);
    				
    				ex10.addTerm(1.0*(1-Ytrain.get(i)), lin[i][l]);
    			}
    			///EQUATION 11
    			model.addConstr(ex10, GRB.EQUAL, 0.0, "eq10_"+i);*/
    		}
    		
    		
    		
    		/*for (int l=0;l<numTreeNodeVars/2+1;l++)
    		{
    			///EQ: each leaf is non empty
    			GRBLinExpr ex20 = new GRBLinExpr();
    			for (int i=0;i<Xtrain.numRows();i++)
    				ex20.addTerm(1.0, x[i][l]);
    			///EQUATION 12
    			model.addConstr(ex20, GRB.GREATER_EQUAL, 1.0, "eq20_"+l);
    		}*/
    		
    		///equations on zl
    		/*for (int i=0;i<numTreeNodeVars/2+1;i++)
    		{
    						
    			GRBLinExpr ex13 = new GRBLinExpr();
    			for (int j=0;j<Xtrain.numRows();j++)
    				ex13.addTerm(2*Ytrain.get(j)-1, x[j][i]);
    			
    			ex13.addTerm(1.0, hminus[i]);
    			ex13.addTerm(-1.0, hplus[i]);
    			model.addConstr(ex13,  GRB.EQUAL, 0.0, "eq13_"+i);
    			
    			GRBLinExpr ex14 = new GRBLinExpr();
    			ex14.addTerm(1.0, hplus[i]);
    			ex14.addTerm(-1.0*bigM, z[i]);
    			model.addConstr(ex14,  GRB.LESS_EQUAL, 0.0, "eq14_"+i);
    			
    			GRBLinExpr ex15 = new GRBLinExpr();
    			ex15.addTerm(1.0, hminus[i]);
    			ex15.addTerm(bigM, z[i]);
    			model.addConstr(ex15,  GRB.LESS_EQUAL, bigM, "eq15_"+i);
    			
				GRBLinExpr ex16 = new GRBLinExpr();
				ex16.addTerm(1.0, hminus[i]);
				ex16.addTerm(1.0, hplus[i]);
				ex16.addTerm(smallM, z[i]);
				model.addConstr(ex16, GRB.GREATER_EQUAL, smallM, "eq16_"+i);
    		}*/
    		
    		///fp and fn equations
    		/*for (int i=0;i<Xtrain.numRows();i++)
    		{
    			GRBLinExpr ex17 = new GRBLinExpr();
    			ex17.addTerm(1-Ytrain.get(i), fp[i]);
    			ex17.addTerm(1-Ytrain.get(i), tn[i]);
    			model.addConstr(ex17, GRB.EQUAL, 1-Ytrain.get(i), "eq17_"+i);
    			
    			GRBLinExpr ex18 = new GRBLinExpr();
    			ex18.addTerm(Ytrain.get(i), fn[i]);
    			ex18.addTerm(Ytrain.get(i), tp[i]);
    			model.addConstr(ex18, GRB.EQUAL, Ytrain.get(i), "eq18_"+i);
    		}*/
    		
    	    /////probability equations
    	    /*for (int i=0;i<Xtrain.numRows();i++)
    	    {
    	    	for (int j=0;j<numTreeNodeVars/2+1;j++)
    	    	{	
    	    		GRBLinExpr ex41 = new GRBLinExpr();
    	    		ex41.addTerm(1.0, linearize[i][j]);
    	    		ex41.addTerm(-1.0, ckt[1][j]);
    	    		model.addConstr(ex41, GRB.LESS_EQUAL, 0.0, "eq47_"+i+"_"+j);

    	    		GRBLinExpr ex42 = new GRBLinExpr();
    	    		ex42.addTerm(1.0, linearize[i][j]);
    	    		ex42.addTerm(-1.0, z[i][j]);
    	    		model.addConstr(ex42, GRB.LESS_EQUAL, 0.0, "eq48_"+i+"_"+j);

    	    
    	    		GRBLinExpr ex43 = new GRBLinExpr();
    	    		ex43.addTerm(1.0, linearize[i][j]);
    	    		ex43.addTerm(-1.0, ckt[1][j]);
    	    		ex43.addTerm(-1.0, z[i][j]);
    	    		model.addConstr(ex43, GRB.GREATER_EQUAL, -1.0, "eq49_"+i+"_"+j);
    	    	}
    	    }

    	    for (int i=0;i<x.length;i++)
    	    {
    	    	GRBLinExpr ex44 = new GRBLinExpr();
    	    	for (int j=0;j<numTreeNodeVars/2+1;j++)
    	    	{
    	    		ex44.addTerm(1.0, linearize[i][j]);
    	    	}
    	    	
    	    	ex44.addTerm(-1.0, x[i]);
    	    	model.addConstr(ex44, GRB.EQUAL, 0.0, "eq50_"+i);
    	    }

    	   
    	    for (int i=0;i<Xtrain.numRows();)
    	    {
    	    	double currNet = train.get(i, 0);
    	    	GRBLinExpr ex45 = new GRBLinExpr();
    	    	while(i<Xtrain.numRows() && train.get(i,0)==currNet)
    	    	{
    	    		ex45.addTerm(1.0, x[i]);
    	    		i++;
    	    	}
    	    	model.addConstr(ex45, GRB.EQUAL, 1.0, "eq51_"+i);
    	    }*/

    	    //////////////probability equations end
    		
    		
    		///TO DO:CHECK NUMERICAL
    		GRBLinExpr objf1 = new GRBLinExpr();
    		for (int i=0;i<Xtrain.numRows();i++)
    		{
    			if (Ytrain.get(i)>1-numerical_instability_epsilon)
    				objf1.addTerm(1.0, tp[i]);
    			else if (Ytrain.get(i)<numerical_instability_epsilon)
    				objf1.addTerm(1.0, tn[i]);
    			//objf1.addTerm(1-Ytrain.get(i), tn[i]);
    			//objf1.addTerm(-1.0*(1-Ytrain.get(i)), fp[i]);
    			//objf1.addTerm(-1.0*Ytrain.get(i), fn[i]);
    		}
    		
    		
    		model.setObjective(objf1, GRB.MAXIMIZE);
    		
    	}
    	catch(GRBException e)
    	{
    		System.out.println("Error code: " + e.getErrorCode() + ". " +
                    e.getMessage());
    	}
    }
    
    public void provideWarmStartCART(String filename)
    {
		try 
		{
			DataSource source = new DataSource(filename);
			Instances data = source.getDataSet();
			if (data.classIndex() == -1)
				   data.setClassIndex(data.numAttributes() - 1);
			data.deleteAttributeAt(0);
		
			
			REPTree tree = new REPTree();
			String[] options = weka.core.Utils.splitOptions("-M "+Nmin+" -V 0.001 -N 3 -S 1 -L "+treeDepth+" -P -I 0.0");
			//String[] options = weka.core.Utils.splitOptions("-M "+Nmin+" -L "+treeDepth);
			tree.setOptions(options);
			tree.buildClassifier(data);
			
			System.out.println(tree.toString());
			
			Tree root = tree.m_Tree;
			
			Queue queue = new LinkedList();
			queue.add(root);
			
			
			int index=0;
			while(index<b.length && !queue.isEmpty())
			{
				Tree node = (Tree)queue.remove();
				if (node.m_Attribute==-1)	
				{
					b[index].set(GRB.DoubleAttr.Start, 1.0);
				}
				else
				{
					b[index].set(GRB.DoubleAttr.Start, node.m_SplitPoint);
				}
				int tamp = node.m_Attribute;
				if (tamp==-1)///need to revisit
					tamp=0;
				for (int j=0;j<Xtrain.numCols();j++)
				{
					
					if (j==tamp)
						a[j][index].set(GRB.DoubleAttr.Start, 1.0);
					else
						a[j][index].set(GRB.DoubleAttr.Start, 0.0);
				}
				index++;
				
				//add children
				if (node.m_Attribute!=-1)
				{
					for (int j=0;j<node.m_Successors.length;j++)
						queue.add(node.m_Successors[j]);
				}
				else//construct binary tree
				{
					for (int j=0;j<2;j++)
					{
						queue.add(tree.dummyTree);
					}
				}
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
    }
    
    public double[] evaluateClassifieronTestSet()
    {
    	double result[] = new double[4];
    	try
    	{
    		System.out.println("Test set size: "+Xtest.numRows());
    		double Lhat = 0;
    		for (int i=0;i<Xtest.numRows();i++)
    			Lhat += Ytest.get(i);
    		
    		int numTreeNodeVars = (int) (Math.pow(2, treeDepth+1) - 1);
    		
    		double misclassified = 0;
    		double tp=0,fp=0,tn=0,fn=0;
    		for (int i=0;i<Xtest.numRows();i++)
    		{
    			int currentNode = 0;
    			while (currentNode<b.length)
    			{
/*    				if (d[currentNode].get(GRB.DoubleAttr.X)<1.0)//no split
    				{
    					currentNode = currentNode*2+2;///go to right in case of no split
    				}
    				else
    				{*/
    				for (int j=0;j<Xtest.numCols();j++)
        			{
        				if (a[currentNode][j].get(GRB.DoubleAttr.X)>1-numerical_instability_epsilon)
        				{
        					if (Xtest.get(i, j) < b[currentNode].get(GRB.DoubleAttr.X))
        						currentNode = currentNode*2 + 1;//left child
        					else
        						currentNode = currentNode*2 + 2;//right child
        					break;
        				}
        			}
    				/*}*/
    			}
    			
    			int leaf = currentNode - numTreeNodeVars/2;
    			int currPrediction = z[leaf].get(GRB.DoubleAttr.X)>(1-numerical_instability_epsilon)?1:0;
    			misclassified += Math.abs(currPrediction - Ytest.get(i));
    			if (currPrediction==1)
    			{
    				if (Ytest.get(i)==1)
    					tp++;
    				else if (Ytest.get(i)==0)
    					fp++;
    			}
    			else if (currPrediction==0)
    			{
    				if (Ytest.get(i)==1)
    					fn++;
    				else if (Ytest.get(i)==0)
    					tn++;
    			}
    		}
    		System.out.println("Confusion Matrix on test set: ");
    		System.out.println(tp+" "+fn);
    		System.out.println(fp+" "+tn);
    		
    		double precision = tp/(tp+fp);
    		if (Double.isNaN(precision))
    			precision=0;
    		double recall = tp/(tp+fn);
    		
    		double f1;
    		if (precision==0&&recall==0)
    			f1=0;
    		else
    			f1 = 2*precision*recall/(precision+recall);
    		
    		result[0]=precision;
    		result[1]=recall;
    		result[2]=f1;
    		result[3]=misclassified/Xtest.numRows();
    		return result;
    	}
    	catch(GRBException e)
    	{
    		System.out.println("Error code: " + e.getErrorCode() + ". " +
                    e.getMessage());
    	}
    	return result;
    }
    
    public void printSolution()
    {
    	try
    	{
    		int numTreeNodeVars = (int) (Math.pow(2, treeDepth+1) - 1);
    		
    		
    		System.out.println("Training set size: "+Xtrain.numRows());
    		System.out.println("Solution to be printed:");
    		FileWriter fw = new FileWriter(outputfile, true);
    		
    		for (int i=0;i<b.length;i++)
    		{
    			System.out.println("B Val: "+b[i].get(GRB.DoubleAttr.X));
    			fw.write("Node "+i+" : ");
    			System.out.print("Node "+i+" : ");
    			for (int j=0;j<Xtrain.numCols();j++)
    			{
    				if (a[i][j].get(GRB.DoubleAttr.X)>1-numerical_instability_epsilon)
    				{
    					fw.write("a is "+j+" | b is "+b[i].get(GRB.DoubleAttr.X));
    					System.out.println("a is "+j+" | b is "+b[i].get(GRB.DoubleAttr.X));
    				}
    			}
    		}
    		
    		for (int i=0;i<z.length;i++)
    			System.out.println("z("+i+") is: "+z[i].get(GRB.DoubleAttr.X));
    		
    		for (int l=0;l<numTreeNodeVars/2+1;l++)
			{
    			System.out.print("Leaf number: "+l+" | Datapoints");
    			for (int i=0;i<Xtrain.numRows();i++)
    			{
    				if (x[i][l].get(GRB.DoubleAttr.X)>1-numerical_instability_epsilon)
    					System.out.print(i+ " ");
    			}
    			System.out.println();
    		}
    		

    		/*for (int i=0;i<Xtrain.numRows();i++)
    		{
    			if (w[i][0].get(GRB.DoubleAttr.X)>1-numerical_instability_epsilon)
    				System.out.println("Point "+i+"sent left");
    			else
    				System.out.println("Point "+i+"sent right");
    		}
    		
    		for (int i=0;i<Xtrain.numRows();i++)
    		{
    			double val=b[0].get(GRB.DoubleAttr.X);
    			
       			for (int j=0;j<Xtrain.numCols();j++)
    			{
    				if (a[0][j].get(GRB.DoubleAttr.X)>1-numerical_instability_epsilon)
    				{
    					val = val - Xtrain.get(i,j);
    					break;
    				}
    			}
       			System.out.print("LHS: "+ val+" ");
       			System.out.print("y+in = "+yplus[i][0].get(GRB.DoubleAttr.X)+" y-in = "+yminus[i][0].get(GRB.DoubleAttr.X));
       			System.out.println();
    		}*/
    		
    		
    		
    		
    		/*System.out.println("W    Xleft    Xright    point-feature");
    		for (int i=0;i<Xtrain.numRows();i++)
    		{
    			System.out.println(w[i][0].get(GRB.DoubleAttr.X)+" "+x[i][0].get(GRB.DoubleAttr.X)+" "+x[i][1].get(GRB.DoubleAttr.X)+" "+Xtrain.get(i, 17) + " "+yplus[i][0].get(GRB.DoubleAttr.X)+" "+yminus[i][0].get(GRB.DoubleAttr.X));
    		}*/
    		
    		double TP=0;
    		double FP=0;
    		double TN=0;
    		double FN=0;
    		
    		for (int i=0;i<Xtrain.numRows();i++)
    		{
    			TP += Ytrain.get(i)*tp[i].get(GRB.DoubleAttr.X);
    			TN += (1-Ytrain.get(i))*tn[i].get(GRB.DoubleAttr.X);
    			
    		}
    		
    		double TotalP=0;
    		double TotalN=0;
    		for (int i=0;i<Ytrain.numRows();i++)
    			TotalP +=Ytrain.get(i);
    		TotalN = Ytrain.numRows() - TotalP;
    		
    		
    		FP = TotalN -  TN;
    		FN = TotalP - TP;
    		
    		System.out.println("Confusion Matrix on training set: ");
    		System.out.println(TP+" "+FN);
    		System.out.println(FP+" "+TN);
    	}
    	catch(Exception e)
    	{
    		e.printStackTrace();
    	}
    }
}
