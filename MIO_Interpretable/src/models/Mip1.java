package models;

import gurobi.GRB;
import gurobi.GRBEnv;
import gurobi.GRBException;
import gurobi.GRBLinExpr;
import gurobi.GRBModel;
import gurobi.GRBVar;

public class Mip1 {
    public GRBEnv    env;
    public GRBModel  model;
    GRBVar x,y,z;
    
    public Mip1 ()
    {
    	try
    	{
    		env   = new GRBEnv("mip1.log");
    		model = new GRBModel(env);
    		specifyModel();
    	}
    	catch(GRBException e)
    	{
    	      System.out.println("Error code: " + e.getErrorCode() + ". " +
                      e.getMessage());
    	}
    }
    
    public void specifyModel()
    {
    	try
    	{
    	// Create variables
        x = model.addVar(0.0, 1.0, 0.0, GRB.BINARY, "x");
        y = model.addVar(0.0, 1.0, 0.0, GRB.BINARY, "y");
        z = model.addVar(0.0, 1.0, 0.0, GRB.BINARY, "z");

        // Set objective: maximize x + y + 2 z

        GRBLinExpr expr = new GRBLinExpr();
        expr.addTerm(1.0, x); expr.addTerm(1.0, y); expr.addTerm(2.0, z);
        model.setObjective(expr, GRB.MAXIMIZE);

        // Add constraint: x + 2 y + 3 z <= 4

        expr = new GRBLinExpr();
        expr.addTerm(1.0, x); expr.addTerm(2.0, y); expr.addTerm(3.0, z);
        model.addConstr(expr, GRB.LESS_EQUAL, 4.0, "c0");

        // Add constraint: x + y >= 1

        expr = new GRBLinExpr();
        expr.addTerm(1.0, x); expr.addTerm(1.0, y);
        model.addConstr(expr, GRB.GREATER_EQUAL, 1.0, "c1");
    	}
    	catch(GRBException e)
    	{
    	      System.out.println("Error code: " + e.getErrorCode() + ". " +
                      e.getMessage());
    	}
    }
    
    public void printSolution()
    {
    	try
    	{
    	System.out.println(x.get(GRB.StringAttr.VarName)
                + " " +x.get(GRB.DoubleAttr.X));
    	System.out.println(y.get(GRB.StringAttr.VarName)
                + " " +y.get(GRB.DoubleAttr.X));
    	System.out.println(z.get(GRB.StringAttr.VarName)
                + " " +z.get(GRB.DoubleAttr.X));

    	System.out.println("Obj: " + model.get(GRB.DoubleAttr.ObjVal));
    	}
    	catch(GRBException e)
    	{
    		System.out.println("Error code: " + e.getErrorCode() + ". " +
                    e.getMessage());
    	}
    }
}
