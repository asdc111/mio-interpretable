package models;

import java.io.FileWriter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

import org.ejml.simple.SimpleMatrix;

import gurobi.GRB;
import gurobi.GRBEnv;
import gurobi.GRBException;
import gurobi.GRBLinExpr;
import gurobi.GRBModel;
import gurobi.GRBVar;
import weka.classifiers.trees.REPTree;
import weka.classifiers.trees.REPTree.Tree;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;

public class Relaxed_NewDtree {
    public GRBEnv    env;
    public GRBModel  model;
    
    public GRBVar x[][], a[][], b[], z[], w[][], yplus[][], yminus[][], tp[], tn[], fp[], fn[], lin[][], hplus[], hminus[];
    
    SimpleMatrix train, test;
    SimpleMatrix  Xtrain, Ytrain, Xtest, Ytest;
    
    int treeDepth, Nmin;
    double interpretabilityWeights[];
    double lambda,lambda2;
    String outputfile;
    
    static double smallM = 0.001;
    static double bigM;
    
    static double numerical_instability_epsilon = 0.0001;
    
    public Relaxed_NewDtree(String filename, int treeDepth, int Nmin, double lambda, double interpretabilityWeights[], String outputfile, double fractionTrainSet, SimpleMatrix train, SimpleMatrix test)
    {
    	try
    	{	
    		this.treeDepth = treeDepth;
    		this.Nmin = Nmin;
    	  	this.lambda = lambda;
        	this.interpretabilityWeights = interpretabilityWeights;
        	this.outputfile = outputfile;
        	
        	
        	
        	this.train = train;
        	this.test = test;
       		Xtrain = train.extractMatrix(0, train.numRows(), 1, train.numCols()-1);//leave first col..last column for label
    		Ytrain = train.extractVector(false, train.numCols()-1);///last label
    		
    		bigM = Xtrain.numRows();
    		
      		Xtest = test.extractMatrix(0, test.numRows(), 1, test.numCols()-1);//leave first col..leave last column for label
    		Ytest = test.extractVector(false, test.numCols()-1);///last label*/
    		
    		env   = new GRBEnv("mip1.log");
    		model = new GRBModel(env);
    		
    		
    		
    		specifyModel();
    	}
    	catch(GRBException e)
    	{
    		System.out.println("Error code: " + e.getErrorCode() + ". " +
                    e.getMessage());
    	}
    }
    
    public void specifyModel()
    {
    	try
    	{
    		int numTreeNodeVars = (int) (Math.pow(2, treeDepth+1) - 1);
    		
    		lin = new GRBVar[Xtrain.numRows()][numTreeNodeVars/2+1];
    		x = new GRBVar[Xtrain.numRows()][numTreeNodeVars/2+1];
    		z = new GRBVar[numTreeNodeVars/2+1];
    		for (int i=0; i<numTreeNodeVars/2+1; i++)
    		{
    			z[i] = model.addVar(0.0, 1.0, 0.0, GRB.CONTINUOUS, "z_"+i);
    			for (int j=0;j<Xtrain.numRows();j++)
    			{
    				x[j][i] = model.addVar(0.0, 1.0, 0.0, GRB.CONTINUOUS, "x_"+j+"_"+i);
    				lin[j][i] = model.addVar(0.0, 1.0, 0.0, GRB.CONTINUOUS, "lin_"+j+"_"+i);
    				//x[j][i].set(GRB.IntAttr.BranchPriority, 1);
    			}
    		}
    		
    		yplus = new GRBVar[Xtrain.numRows()][numTreeNodeVars/2];
    		yminus = new GRBVar[Xtrain.numRows()][numTreeNodeVars/2];
    		for (int i=0;i<Xtrain.numRows();i++)
    		{
    			for (int j=0;j<numTreeNodeVars/2;j++)
    			{
    				yplus[i][j] = model.addVar(0.0, 1.0, 0.0, GRB.CONTINUOUS, "yplus_"+i+"_"+j);
    				yminus[i][j] = model.addVar(0.0, 1.0, 0.0, GRB.CONTINUOUS, "yminus_"+i+"_"+j);
    			}
    		}
    		
    		hplus = new GRBVar[numTreeNodeVars/2+1];
    		hminus = new GRBVar[numTreeNodeVars/2+1];
    		for (int i=0; i<numTreeNodeVars/2+1; i++)
    		{
    			hplus[i] = model.addVar(0.0, Xtrain.numRows(), 0.0, GRB.CONTINUOUS, "hplus_"+i);
    			hminus[i] = model.addVar(0.0, Xtrain.numRows(), 0.0, GRB.CONTINUOUS, "hminus_"+i);
    		}
    		
    		
    		a = new GRBVar[Xtrain.numCols()][numTreeNodeVars/2];
    		b = new GRBVar[numTreeNodeVars/2];
    		
    		for (int i=0;i<numTreeNodeVars/2;i++)
    		{	
    			b[i] = model.addVar(0.0, 1.0, 0.0, GRB.CONTINUOUS, "b_"+i);
    			b[i].set(GRB.IntAttr.BranchPriority, Integer.MAX_VALUE - 1);
    			for (int j=0;j<Xtrain.numCols();j++)
    			{
    				a[j][i] = model.addVar(0.0, 1.0, 0.0, GRB.BINARY, "a_"+j+"_"+i);
    				a[j][i].set(GRB.IntAttr.BranchPriority, Integer.MAX_VALUE);
    			}
    		}
    		
    		/*b[0] = model.addVar(0.62, 0.62, 0.0, GRB.CONTINUOUS, "b_"+0);
    		b[1] = model.addVar(0.75, 0.75, 0.0, GRB.CONTINUOUS, "b_"+1);
    		b[2] = model.addVar(0.5, 0.5, 0.0, GRB.CONTINUOUS, "b_"+2);
    		
    		for (int i=0;i<Xtrain.numCols();i++)
    		{
    			if (i==4)
    				a[i][0] = model.addVar(1.0, 1.0, 0.0, GRB.BINARY, "a_"+i+"_0");
    			else
    				a[i][0] = model.addVar(0.0, 0.0, 0.0, GRB.BINARY, "a_"+i+"_0");
    		}
    		
    		for (int i=0;i<Xtrain.numCols();i++)
    		{
    			if (i==0)
    				a[i][1] = model.addVar(1.0, 1.0, 0.0, GRB.BINARY, "a_"+i+"_1");
    			else
    				a[i][1] = model.addVar(0.0, 0.0, 0.0, GRB.BINARY, "a_"+i+"_1");
    		}
    		
    		for (int i=0;i<Xtrain.numCols();i++)
    		{
    			if (i==19)
    				a[i][2] = model.addVar(1.0, 1.0, 0.0, GRB.BINARY, "a_"+i+"_2");
    			else
    				a[i][2] = model.addVar(0.0, 0.0, 0.0, GRB.BINARY, "a_"+i+"_2");
    		}*/
    		
    		
    		w = new GRBVar[Xtrain.numRows()][numTreeNodeVars/2];
    		for (int i=0;i<numTreeNodeVars/2;i++)
    		{
    			for (int j=0;j<Xtrain.numRows();j++)
    			{
    				w[j][i] = model.addVar(0.0, 1.0, 0.0, GRB.CONTINUOUS, "w_"+j+"_"+i);
    			}
    		}
    		
    		tp = new GRBVar[Xtrain.numRows()];
    		tn = new GRBVar[Xtrain.numRows()];
    		
    		for (int i=0;i<Xtrain.numRows();i++)
    		{
    			tp[i] = model.addVar(0.0, 1.0, 0.0, GRB.CONTINUOUS, "tp_"+i);
    			tn[i] = model.addVar(0.0, 1.0, 0.0, GRB.CONTINUOUS, "tn_"+i);
    		}
    		
    		//////////////
    		fp = new GRBVar[Xtrain.numRows()];
    		fn = new GRBVar[Xtrain.numRows()];
    		
    		for (int i=0;i<Xtrain.numRows();i++)
    		{
    			fp[i] = model.addVar(0.0, 1.0, 0.0, GRB.CONTINUOUS, "fp_"+i);
    			fn[i] = model.addVar(0.0, 1.0, 0.0, GRB.CONTINUOUS, "fn_"+i);
    		}
    		///////////////////
    		
    		model.update();
    		
    		for (int i=0;i<Xtrain.numRows();i++)
    		{
    			GRBLinExpr ex1 = new GRBLinExpr();
    			for (int j=0;j<numTreeNodeVars/2+1;j++)
    				ex1.addTerm(1.0, x[i][j]);
    			model.addConstr(ex1, GRB.EQUAL, 1.0, "eq1_"+i);
    		}
    		
    		for (int i=0;i<numTreeNodeVars/2;i++)
    		{
    			
    			GRBLinExpr ex2 = new GRBLinExpr();
    			GRBLinExpr ex21 = new GRBLinExpr();
    			for (int j=0;j<Xtrain.numCols();j++)
    			{
    				ex2.addTerm(1.0, a[j][i]);
    				ex21.addTerm(1.0, a[j][i]);
    			}
    	
    			model.addConstr(ex2, GRB.EQUAL, 1.0, "eq2_"+i);
    			
    			ex21.addTerm(-1.0, b[i]);
    			model.addConstr(ex21, GRB.GREATER_EQUAL, 0.0, "eq21_"+i);
    			
    			int rightNode = 2*i+2;
    			ArrayList<Integer> rightIndices = new ArrayList<Integer>();
    			ArrayList<Integer> leftIndices = new ArrayList<Integer>();
    			for (int j=0;j<numTreeNodeVars/2+1;j++)
    			{
    				int currInd = j+numTreeNodeVars/2;
    				while(currInd>rightNode)
    				{
    					if (currInd%2==0)
    						currInd = currInd/2-1;
    					else
    						currInd = currInd/2;
    					
    					if (currInd==rightNode || currInd==rightNode-1)
    						break;
    				}
    				if (currInd==rightNode)
    					rightIndices.add(j);
    				else if (currInd == rightNode - 1)
    					leftIndices.add(j);
    			}
    			
    			for (int j=0;j<Xtrain.numRows();j++)
    			{
    				GRBLinExpr ex11 = new GRBLinExpr();
    				ex11.addTerm(1.0, yminus[j][i]);
    				ex11.addTerm(1.0, yplus[j][i]);
    				ex11.addTerm(smallM, w[j][i]);
    				model.addConstr(ex11, GRB.GREATER_EQUAL, smallM, "eq11_"+i+"_"+j);
    				
    				for (int k=0;k<rightIndices.size();k++)
    				{
    					GRBLinExpr ex6 = new GRBLinExpr();
    					ex6.addTerm(1.0, x[j][rightIndices.get(k)]);
    					ex6.addTerm(1.0, w[j][i]);
    					model.addConstr(ex6, GRB.LESS_EQUAL, 1.0, "eq6_"+i+"_"+j+"_"+k);
    				}
    				
    				for (int k=0;k<leftIndices.size();k++)
    				{
    					GRBLinExpr ex12 = new GRBLinExpr();
    					ex12.addTerm(1.0, x[j][leftIndices.get(k)]);
    					ex12.addTerm(-1.0, w[j][i]);
    					model.addConstr(ex12, GRB.LESS_EQUAL, 0.0, "eq12_"+i+"_"+j+"_"+k);
    				}
    			}
    			
    			
    		}
    		
    		for (int i=0;i<Xtrain.numRows();i++)
    		{
    			for (int j=0;j<numTreeNodeVars/2;j++)
    			{
    				
    				GRBLinExpr ex3 = new GRBLinExpr();
    				for (int k=0;k<Xtrain.numCols();k++)
    					ex3.addTerm(Xtrain.get(i, k), a[k][j]);
    				
    				ex3.addTerm(-1.0, b[j]);
    				ex3.addTerm(1.0, yplus[i][j]);
    				ex3.addTerm(-1.0, yminus[i][j]);
    				model.addConstr(ex3, GRB.EQUAL, 0.0, "eq3_"+i+"_"+j);
    				
    				GRBLinExpr ex4 = new GRBLinExpr();
    				ex4.addTerm(1.0, yplus[i][j]);
    				ex4.addTerm(-1.0, w[i][j]);
    				model.addConstr(ex4, GRB.LESS_EQUAL, 0.0, "eq4_"+i+"_"+j);
    				
    				GRBLinExpr ex5 = new GRBLinExpr();
    				ex5.addTerm(1.0, yminus[i][j]);
    				ex5.addTerm(1.0, w[i][j]);
    				model.addConstr(ex5, GRB.LESS_EQUAL, 1.0, "eq5_"+i+"_"+j);
    			}
    		}
    		
    		for (int i=0;i<Xtrain.numRows();i++)
    		{
    			for (int j=0;j<numTreeNodeVars/2+1;j++)
    			{
    				GRBLinExpr ex7 = new GRBLinExpr();
    				ex7.addTerm(1.0, lin[i][j]);
    				ex7.addTerm(-1.0, z[j]);
    				model.addConstr(ex7, GRB.LESS_EQUAL, 0.0, "eq7_"+i+"_"+j);
    				
    				GRBLinExpr ex8 = new GRBLinExpr();
    				ex8.addTerm(1.0, lin[i][j]);
    				ex8.addTerm(-1.0, x[i][j]);
    				model.addConstr(ex8, GRB.LESS_EQUAL, 0.0, "eq8_"+i+"_"+j);
    				
    				GRBLinExpr ex9 = new GRBLinExpr();
    				ex9.addTerm(1.0, lin[i][j]);
    				ex9.addTerm(-1.0, x[i][j]);
    				ex9.addTerm(-1.0, z[j]);
    				model.addConstr(ex9, GRB.GREATER_EQUAL, -1.0, "eq9_"+i+"_"+j);
    			}
    			
    			GRBLinExpr ex10 = new GRBLinExpr();
    			ex10.addTerm(Ytrain.get(i), tp[i]);
    			for (int j=0;j<numTreeNodeVars/2+1;j++)
    			{
    				ex10.addTerm(-1.0*Ytrain.get(i), lin[i][j]);
    				
    				ex10.addTerm(-1.0*(1-Ytrain.get(i)), x[i][j]);
    				
    				ex10.addTerm(1.0*(1-Ytrain.get(i)), lin[i][j]);
    			}
    			ex10.addTerm(1-Ytrain.get(i), tn[i]);
    			model.addConstr(ex10, GRB.EQUAL, 0.0, "eq10_"+i);
    		}
    		
    		///equations on zl
    		for (int i=0;i<numTreeNodeVars/2+1;i++)
    		{
    			GRBLinExpr ex20 = new GRBLinExpr();
    			for (int j=0;j<Xtrain.numRows();j++)
    				ex20.addTerm(1.0, x[j][i]);
    			model.addConstr(ex20, GRB.GREATER_EQUAL, 1.0, "eq20_"+i);
    			
    			
    			GRBLinExpr ex13 = new GRBLinExpr();
    			for (int j=0;j<Xtrain.numRows();j++)
    				ex13.addTerm(2*Ytrain.get(j)-1, x[j][i]);
    			
    			ex13.addTerm(1.0, hminus[i]);
    			ex13.addTerm(-1.0, hplus[i]);
    			model.addConstr(ex13,  GRB.EQUAL, 0.0, "eq13_"+i);
    			
    			GRBLinExpr ex14 = new GRBLinExpr();
    			ex14.addTerm(1.0, hplus[i]);
    			ex14.addTerm(-1.0*bigM, z[i]);
    			model.addConstr(ex14,  GRB.LESS_EQUAL, 0.0, "eq14_"+i);
    			
    			GRBLinExpr ex15 = new GRBLinExpr();
    			ex15.addTerm(1.0, hminus[i]);
    			ex15.addTerm(bigM, z[i]);
    			model.addConstr(ex15,  GRB.LESS_EQUAL, bigM, "eq15_"+i);
    			
				GRBLinExpr ex16 = new GRBLinExpr();
				ex16.addTerm(1.0, hminus[i]);
				ex16.addTerm(1.0, hplus[i]);
				ex16.addTerm(smallM, z[i]);
				model.addConstr(ex16, GRB.GREATER_EQUAL, smallM, "eq16_"+i);
    		}
    		
    		///fp and fn equations
    		for (int i=0;i<Xtrain.numRows();i++)
    		{
    			GRBLinExpr ex17 = new GRBLinExpr();
    			ex17.addTerm(1-Ytrain.get(i), fp[i]);
    			ex17.addTerm(1-Ytrain.get(i), tn[i]);
    			model.addConstr(ex17, GRB.EQUAL, 1-Ytrain.get(i), "eq17_"+i);
    			
    			GRBLinExpr ex18 = new GRBLinExpr();
    			ex18.addTerm(Ytrain.get(i), fn[i]);
    			ex18.addTerm(Ytrain.get(i), tp[i]);
    			model.addConstr(ex18, GRB.EQUAL, Ytrain.get(i), "eq18_"+i);
    		}
    		
    	    /////probability equations
    	    /*for (int i=0;i<Xtrain.numRows();i++)
    	    {
    	    	for (int j=0;j<numTreeNodeVars/2+1;j++)
    	    	{	
    	    		GRBLinExpr ex41 = new GRBLinExpr();
    	    		ex41.addTerm(1.0, linearize[i][j]);
    	    		ex41.addTerm(-1.0, ckt[1][j]);
    	    		model.addConstr(ex41, GRB.LESS_EQUAL, 0.0, "eq47_"+i+"_"+j);

    	    		GRBLinExpr ex42 = new GRBLinExpr();
    	    		ex42.addTerm(1.0, linearize[i][j]);
    	    		ex42.addTerm(-1.0, z[i][j]);
    	    		model.addConstr(ex42, GRB.LESS_EQUAL, 0.0, "eq48_"+i+"_"+j);

    	    
    	    		GRBLinExpr ex43 = new GRBLinExpr();
    	    		ex43.addTerm(1.0, linearize[i][j]);
    	    		ex43.addTerm(-1.0, ckt[1][j]);
    	    		ex43.addTerm(-1.0, z[i][j]);
    	    		model.addConstr(ex43, GRB.GREATER_EQUAL, -1.0, "eq49_"+i+"_"+j);
    	    	}
    	    }

    	    for (int i=0;i<x.length;i++)
    	    {
    	    	GRBLinExpr ex44 = new GRBLinExpr();
    	    	for (int j=0;j<numTreeNodeVars/2+1;j++)
    	    	{
    	    		ex44.addTerm(1.0, linearize[i][j]);
    	    	}
    	    	
    	    	ex44.addTerm(-1.0, x[i]);
    	    	model.addConstr(ex44, GRB.EQUAL, 0.0, "eq50_"+i);
    	    }

    	   
    	    for (int i=0;i<Xtrain.numRows();)
    	    {
    	    	double currNet = train.get(i, 0);
    	    	GRBLinExpr ex45 = new GRBLinExpr();
    	    	while(i<Xtrain.numRows() && train.get(i,0)==currNet)
    	    	{
    	    		ex45.addTerm(1.0, x[i]);
    	    		i++;
    	    	}
    	    	model.addConstr(ex45, GRB.EQUAL, 1.0, "eq51_"+i);
    	    }*/

    	    //////////////probability equations end
    		
    		
    		GRBLinExpr objf1 = new GRBLinExpr();
    		for (int i=0;i<Xtrain.numRows();i++)
    		{
    			objf1.addTerm(Ytrain.get(i), tp[i]);
    			//objf1.addTerm(1-Ytrain.get(i), tn[i]);
    			objf1.addTerm(-1.0*(1-Ytrain.get(i)), fp[i]);
    			objf1.addTerm(-1.0*Ytrain.get(i), fn[i]);
    		}
    		
    		
    		model.setObjective(objf1, GRB.MAXIMIZE);
    		
    	}
    	catch(GRBException e)
    	{
    		System.out.println("Error code: " + e.getErrorCode() + ". " +
                    e.getMessage());
    	}
    }
    
    public void provideWarmStartCART(String filename)
    {
		try 
		{
			DataSource source = new DataSource(filename);
			Instances data = source.getDataSet();
			if (data.classIndex() == -1)
				   data.setClassIndex(data.numAttributes() - 1);
			data.deleteAttributeAt(0);
		
			
			REPTree tree = new REPTree();
			String[] options = weka.core.Utils.splitOptions("-M "+Nmin+" -V 0.001 -N 3 -S 1 -L "+treeDepth+" -P -I 0.0");
			//String[] options = weka.core.Utils.splitOptions("-M "+Nmin+" -L "+treeDepth);
			tree.setOptions(options);
			tree.buildClassifier(data);
			
			System.out.println(tree.toString());
			
			Tree root = tree.m_Tree;
			
			Queue queue = new LinkedList();
			queue.add(root);
			
			
			int index=0;
			while(index<b.length && !queue.isEmpty())
			{
				Tree node = (Tree)queue.remove();
				if (node.m_Attribute==-1)	
				{
					b[index].set(GRB.DoubleAttr.Start, 1.0);
				}
				else
				{
					b[index].set(GRB.DoubleAttr.Start, node.m_SplitPoint);
				}
				int tamp = node.m_Attribute;
				if (tamp==-1)///need to revisit
					tamp=0;
				for (int j=0;j<Xtrain.numCols();j++)
				{
					
					if (j==tamp)
						a[j][index].set(GRB.DoubleAttr.Start, 1.0);
					else
						a[j][index].set(GRB.DoubleAttr.Start, 0.0);
				}
				index++;
				
				//add children
				if (node.m_Attribute!=-1)
				{
					for (int j=0;j<node.m_Successors.length;j++)
						queue.add(node.m_Successors[j]);
				}
				else//construct binary tree
				{
					for (int j=0;j<2;j++)
					{
						queue.add(tree.dummyTree);
					}
				}
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
    }
    
    public double[] evaluateClassifieronTestSet()
    {
    	double result[] = new double[4];
    	try
    	{
    		System.out.println("Test set size: "+Xtrain.numRows());
    		double Lhat = 0;
    		for (int i=0;i<Xtest.numRows();i++)
    			Lhat += Ytest.get(i);
    		
    		int numTreeNodeVars = (int) (Math.pow(2, treeDepth+1) - 1);
    		
    		double misclassified = 0;
    		double tp=0,fp=0,tn=0,fn=0;
    		for (int i=0;i<Xtest.numRows();i++)
    		{
    			int currentNode = 0;
    			while (currentNode<b.length)
    			{
/*    				if (d[currentNode].get(GRB.DoubleAttr.X)<1.0)//no split
    				{
    					currentNode = currentNode*2+2;///go to right in case of no split
    				}
    				else
    				{*/
    				for (int j=0;j<Xtest.numCols();j++)
        			{
        				if (a[j][currentNode].get(GRB.DoubleAttr.X)>1-numerical_instability_epsilon)
        				{
        					if (Xtest.get(i, j) < b[currentNode].get(GRB.DoubleAttr.X))
        						currentNode = currentNode*2 + 1;//left child
        					else
        						currentNode = currentNode*2 + 2;//right child
        					break;
        				}
        			}
    				/*}*/
    			}
    			
    			int leaf = currentNode - numTreeNodeVars/2;
    			int currPrediction = z[leaf].get(GRB.DoubleAttr.X)>(1-numerical_instability_epsilon)?1:0;
    			misclassified += Math.abs(currPrediction - Ytest.get(i));
    			if (currPrediction==1)
    			{
    				if (Ytest.get(i)==1)
    					tp++;
    				else if (Ytest.get(i)==0)
    					fp++;
    			}
    			else if (currPrediction==0)
    			{
    				if (Ytest.get(i)==1)
    					fn++;
    				else if (Ytest.get(i)==0)
    					tn++;
    			}
    		}
    		System.out.println("Confusion Matrix on test set: ");
    		System.out.println(tp+" "+fn);
    		System.out.println(fp+" "+tn);
    		
    		double precision = tp/(tp+fp);
    		if (Double.isNaN(precision))
    			precision=0;
    		double recall = tp/(tp+fn);
    		
    		double f1;
    		if (precision==0&&recall==0)
    			f1=0;
    		else
    			f1 = 2*precision*recall/(precision+recall);
    		
    		result[0]=precision;
    		result[1]=recall;
    		result[2]=f1;
    		result[3]=misclassified/Xtest.numRows();
    		return result;
    	}
    	catch(GRBException e)
    	{
    		System.out.println("Error code: " + e.getErrorCode() + ". " +
                    e.getMessage());
    	}
    	return result;
    }
    
    public void printSolution()
    {
    	try
    	{
    		System.out.println("Training set size: "+Xtrain.numRows());
    		System.out.println("Solution to be printed:");
    		FileWriter fw = new FileWriter(outputfile, true);
    		
    		for (int i=0;i<b.length;i++)
    		{
    			System.out.println("B Val: "+b[i].get(GRB.DoubleAttr.X));
    			fw.write("Node "+i+" : ");
    			System.out.print("Node "+i+" : ");
    			for (int j=0;j<Xtrain.numCols();j++)
    			{
    				if (a[j][i].get(GRB.DoubleAttr.X)==1)
    				{
    					fw.write("a is "+j+" | b is "+b[i].get(GRB.DoubleAttr.X));
    					System.out.println("a is "+j+" | b is "+b[i].get(GRB.DoubleAttr.X));
    				}
    			}
    		}
    		
    		for (int i=0;i<z.length;i++)
    			System.out.println("z("+i+") is: "+z[i].get(GRB.DoubleAttr.X));
    		
    		/*System.out.println("W    Xleft    Xright    point-feature");
    		for (int i=0;i<Xtrain.numRows();i++)
    		{
    			System.out.println(w[i][0].get(GRB.DoubleAttr.X)+" "+x[i][0].get(GRB.DoubleAttr.X)+" "+x[i][1].get(GRB.DoubleAttr.X)+" "+Xtrain.get(i, 17) + " "+yplus[i][0].get(GRB.DoubleAttr.X)+" "+yminus[i][0].get(GRB.DoubleAttr.X));
    		}*/
    		
    		double TP=0;
    		double FP=0;
    		double TN=0;
    		double FN=0;
    		
    		for (int i=0;i<Xtrain.numRows();i++)
    		{
    			TP += Ytrain.get(i)*tp[i].get(GRB.DoubleAttr.X);
    			TN += (1-Ytrain.get(i))*tn[i].get(GRB.DoubleAttr.X);
    			
    		}
    		
    		double TotalP=0;
    		double TotalN=0;
    		for (int i=0;i<Ytrain.numRows();i++)
    			TotalP +=Ytrain.get(i);
    		TotalN = Ytrain.numRows() - TotalP;
    		
    		
    		FP = TotalN -  TN;
    		FN = TotalP - TP;
    		
    		System.out.println("Confusion Matrix on training set: ");
    		System.out.println(TP+" "+FN);
    		System.out.println(FP+" "+TN);
    	}
    	catch(Exception e)
    	{
    		e.printStackTrace();
    	}
    }
}
