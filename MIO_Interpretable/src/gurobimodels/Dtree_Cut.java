package gurobimodels;

import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.stream.IntStream;

import org.ejml.simple.SimpleMatrix;

import gurobi.GRBVar;
import weka.classifiers.trees.REPTree;
import weka.classifiers.trees.REPTree.Tree;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;
import gurobi.GRB;
import gurobi.GRBConstr;
import gurobi.GRBEnv;
import gurobi.GRBException;
import gurobi.GRBLinExpr;
import gurobi.GRBModel;

//Dtree_Cut implementation in PDF
public class Dtree_Cut {
    public GRBEnv    env;
    public GRBModel  model;
    
    public GRBVar a[][], b[], w[][], x[][], z[];
    public GRBVar vp[], vn[];
    public GRBVar tp, tn;
    
  
    
    SimpleMatrix train, test;
    public SimpleMatrix  Xtrain, Ytrain, Xtest, Ytest;
    
    int treeDepth, Nmin;
    double interpretabilityWeights[];
    double lambda,lambda2;
    String outputfile;
    
    static double smallM[];
    
    static double smallMVal;
    
    static double M_P, M_N;
    
    static double numerical_instability_epsilon = 0.001;
    
    public Dtree_Cut(int treeDepth, SimpleMatrix train)
    {
    	try
    	{	
    		this.treeDepth = treeDepth;
        	
    		
        	this.train = train;
       		Xtrain = train.extractMatrix(0, train.numRows(), 0, train.numCols()-1);//leave first col..last column for label
    		Ytrain = train.extractVector(false, train.numCols()-1);///last label
    		
    		//Xtrain = train.extractMatrix(0, train.numRows(), 1, 5);
    	
    		smallM = new double[Xtrain.numCols()];
    		System.out.println(smallM.length);
    		
    		env   = new GRBEnv("mip1.log");
    		model = new GRBModel(env);
    	
    		specifyModel();
    	}
    	catch(GRBException e)
    	{
    		System.out.println("Error code: " + e.getErrorCode() + ". " +
                    e.getMessage());
    	}
    }
    
    public void specifyModel()
    {
    	try
    	{
    		int numTreeNodeVars = (int) (Math.pow(2, treeDepth+1) - 1);
    		
    		double epsilon[] = new double[Xtrain.numCols()];
    		double feature[] = new double[Xtrain.numRows()];
    		double max_epsilon_entry=-1;
    		int min_epsilon_index=-1;
    		double min_epsilon_entry=10000;
    		for (int i=0;i<Xtrain.numCols();i++)
    		{
    			for (int j=0;j<Xtrain.numRows();j++)
    				feature[j] = Xtrain.get(j, i);
    			Arrays.sort(feature);
    			double minVal=10000;
    			for (int j=feature.length-1;j>0;j--)
    			{
    				double temp = feature[j]-feature[j-1];
    				if (temp!=0 && temp<minVal)
    					minVal = temp;
    			}
    			epsilon[i] = minVal;
    			if (epsilon[i]<min_epsilon_entry)
    			{
    				min_epsilon_index = i;
    				min_epsilon_entry = epsilon[i];
    			}
    			if (epsilon[i]>max_epsilon_entry)
    				max_epsilon_entry = epsilon[i];
    		}
    		for (int i=0;i<Xtrain.numCols();i++)
    			smallM[i] = epsilon[i];
    		smallMVal = min_epsilon_entry;
    		//System.out.println("Min epsilon"+ min_epsilon_entry + " "+min_epsilon_index);
    		
    		M_P=0;
    		M_N=0;
    		for (int i=0;i<Ytrain.numRows();i++)
    		{
    			if (Ytrain.get(i)>1-numerical_instability_epsilon)
    				M_P +=1;
    		}
    		M_N = Ytrain.numRows() - M_P;
    		
    		
    		
    		a = new GRBVar[numTreeNodeVars/2][Xtrain.numCols()];
    		b = new GRBVar[numTreeNodeVars/2];
    		
    		for (int n=0;n<numTreeNodeVars/2;n++)
    		{	
    			b[n] = model.addVar(0.0, 1.0, 0.0, GRB.CONTINUOUS, "b_"+n);
    			for (int f=0;f<Xtrain.numCols();f++)
    			{
    				a[n][f] = model.addVar(0.0, 1.0, 0.0, GRB.BINARY, "a_"+n+"_"+f);
    			}
    		}
    		
    		
    		
    		x = new GRBVar[Xtrain.numRows()][numTreeNodeVars/2+1];
    		z = new GRBVar[numTreeNodeVars/2+1];
    		for (int l=0; l<numTreeNodeVars/2+1; l++)
    		{
    			z[l] = model.addVar(0.0, 1.0, 0.0, GRB.BINARY, "z_"+l);
    			for (int i=0;i<Xtrain.numRows();i++)
    			{
    				x[i][l] = model.addVar(0.0, 1.0, 0.0, GRB.CONTINUOUS, "x_"+i+"_"+l);
    			}
    		}
    		
    		w = new GRBVar[Xtrain.numRows()][numTreeNodeVars/2];
    		for (int i=0;i<Xtrain.numRows();i++)
			{
    			for (int n=0;n<numTreeNodeVars/2;n++)
    			{
    				w[i][n] = model.addVar(0.0, 1.0, 0.0, GRB.BINARY, "w_"+i+"_"+n);
    			}
    		}
    		
    		vp = new GRBVar[numTreeNodeVars/2+1];
    		vn = new GRBVar[numTreeNodeVars/2+1];
    		for (int l=0;l<numTreeNodeVars/2+1;l++)
    		{
    			vp[l] = model.addVar(0.0, M_P, 0.0, GRB.CONTINUOUS, "vp_"+l);
    			vn[l] = model.addVar(0.0, M_N, 0.0, GRB.CONTINUOUS, "vn_"+l);
    		}
    		
    		tp = model.addVar(0.0, M_P, 0.0, GRB.CONTINUOUS, "tp");
			tn = model.addVar(0.0, M_N, 0.0, GRB.CONTINUOUS, "tn");
			
    		
    		
    		model.update();
    		
    		////debug mip start infeasiblity
    		/*GRBLinExpr dbg1 = new GRBLinExpr();
    		dbg1.addTerm(1.0, a[0][0]);
    		model.addConstr(dbg1, GRB.EQUAL, 1, "dbg1");
    		
    		GRBLinExpr b1 = new GRBLinExpr();
    		b1.addTerm(1.0, b[0]);
    		model.addConstr(b1, GRB.EQUAL, 0.5, "b1");
    		
    		////////////////////////////
    		
    		GRBLinExpr dbg2 = new GRBLinExpr();
    		dbg2.addTerm(1.0, a[1][1]);
    		model.addConstr(dbg2, GRB.EQUAL, 1, "dbg2");
    		
    		GRBLinExpr b2 = new GRBLinExpr();
    		b2.addTerm(1.0, b[1]);
    		model.addConstr(b2, GRB.EQUAL, 0.27, "b2");
    		
    		/////////////////////////////////
    		
    		GRBLinExpr dbg3 = new GRBLinExpr();
    		dbg3.addTerm(1.0, a[2][10]);
    		model.addConstr(dbg3, GRB.EQUAL, 1, "dbg3");
    		
    		GRBLinExpr b3 = new GRBLinExpr();
    		b3.addTerm(1.0, b[2]);
    		model.addConstr(b3, GRB.EQUAL, 0.75, "b3");*/
    		///debug mip start infeasibility
    		
    		

    		
    		int branch_priority = Integer.MAX_VALUE;
    		/*for (int i=0;i<Xtrain.numRows();i++)
    		{
    			for (int l=0;l<numTreeNodeVars/2+1;l++)
    			{
    				x[i][l].set(GRB.IntAttr.BranchPriority, branch_priority);
    			}
    		}*/
    		
    		/*for (int n=0;n<numTreeNodeVars/2;n++)
    		{
    			for (int i=0;i<Xtrain.numRows();i++)
    			{
    			
    				w[i][n].set(GRB.IntAttr.BranchPriority, branch_priority);
    			}
    			for (int f=0;f<Xtrain.numCols();f++)
    			{
    				a[n][f].set(GRB.IntAttr.BranchPriority, branch_priority);
    			}
    			branch_priority--;
    		}*/
    		
    		/*for (int n=0;n<numTreeNodeVars/2;n++)
    		{	
    			for (int i=0;i<Xtrain.numRows();i++)
    			{
    			
    				w[i][n].set(GRB.IntAttr.BranchPriority, branch_priority);
    			}
    			branch_priority--;
    		}*/
    		
    		//OBJECTIVE
    		GRBLinExpr objf1 = new GRBLinExpr();
    		objf1.addTerm(1.0, tp);
    		objf1.addTerm(1.0, tn);
    		model.setObjective(objf1, GRB.MAXIMIZE);
    		
    		
    		///CONSTRAINTS
    		for (int i=0;i<Xtrain.numRows();i++)
    		{
    			//EQ: each datapoint has to be assigned to exactly one leaf
    			GRBLinExpr ex1 = new GRBLinExpr();
    			for (int l=0;l<numTreeNodeVars/2+1;l++)
    				ex1.addTerm(1.0, x[i][l]);
    			
    			//EQUATION 2
    			model.addConstr(ex1, GRB.EQUAL, 1.0, "eq1_"+i);
    		}
    		
    		for (int n=0;n<numTreeNodeVars/2;n++)
    		{
    			///EQ: at each branch node...branch only on one variable
    			GRBLinExpr ex2 = new GRBLinExpr();
    			for (int f=0;f<Xtrain.numCols();f++)
    			{
    				ex2.addTerm(1.0, a[n][f]);
    			}
    			///EQUATION 3
    			model.addConstr(ex2, GRB.EQUAL, 1.0, "eq2_"+n);
    			
    			
    			int rightNode = 2*n+2;
    			ArrayList<Integer> rightIndices = new ArrayList<Integer>();
    			ArrayList<Integer> leftIndices = new ArrayList<Integer>();
    			for (int l=0;l<numTreeNodeVars/2+1;l++)
    			{
    				int currInd = l+numTreeNodeVars/2;
    				while(currInd>rightNode)
    				{
    					if (currInd%2==0)
    						currInd = currInd/2-1;
    					else
    						currInd = currInd/2;
    					
    					if (currInd==rightNode || currInd==rightNode-1)
    						break;
    				}
    				if (currInd==rightNode)
    					rightIndices.add(l);
    				else if (currInd == rightNode - 1)
    					leftIndices.add(l);
    			}
    			
    			for (int i=0;i<Xtrain.numRows();i++)
    			{
    				GRBLinExpr ex6 = new GRBLinExpr();
    				for (int l=0;l<rightIndices.size();l++)
    					ex6.addTerm(1.0, x[i][rightIndices.get(l)]);
    				ex6.addTerm(1.0, w[i][n]);
    				model.addConstr(ex6, GRB.LESS_EQUAL, 1.0, "eq6_"+n+"_"+i);
    				
    				GRBLinExpr ex7 = new GRBLinExpr();
    				for (int l=0;l<leftIndices.size();l++)
    					ex7.addTerm(1.0, x[i][leftIndices.get(l)]);
    				ex7.addTerm(-1.0, w[i][n]);
    				model.addConstr(ex7, GRB.LESS_EQUAL, 0.0, "eq7_"+n+"_"+i);
    				
    				GRBLinExpr ex3 = new GRBLinExpr();
    				for (int f=0;f<Xtrain.numCols();f++)
    					ex3.addTerm(Xtrain.get(i, f) + smallMVal, a[n][f]);
    					//ex3.addTerm(Xtrain.get(i, f) + smallM[f], a[n][f]);
    				ex3.addTerm(-1.0, b[n]);
    				
    				GRBLinExpr ex35 = new GRBLinExpr();
    				for (int f=0;f<Xtrain.numCols();f++)
    					ex35.addTerm(Xtrain.get(i, f) , a[n][f]);
    				ex35.addTerm(-1.0, b[n]);
    				model.addGenConstrIndicator(w[i][n], 1, ex3, GRB.LESS_EQUAL, 0.0, "eq_indic1_"+i+"_"+n);
    				model.addGenConstrIndicator(w[i][n], 0, ex35, GRB.GREATER_EQUAL, 0.0, "eq_indic2_"+i+"_"+n);
    			}
    		}
    		
    		GRBLinExpr ex8 = new GRBLinExpr();
    		GRBLinExpr ex9 = new GRBLinExpr();
    		for (int l=0;l<numTreeNodeVars/2+1;l++)
    		{
    			ex8.addTerm(1.0, vp[l]);
    			ex9.addTerm(1.0, vn[l]);
    		}
    		ex8.addTerm(-1.0, tp);
    		ex9.addTerm(-1.0, tn);
    		model.addConstr(ex8, GRB.EQUAL, 0.0, "eq8");
    		model.addConstr(ex9, GRB.EQUAL, 0.0, "eq9");
    		
    		for (int l=0;l<numTreeNodeVars/2+1;l++)
    		{
    			GRBLinExpr ex10 = new GRBLinExpr();
    			ex10.addTerm(1.0, vp[l]);
    			ex10.addTerm(-1*M_P, z[l]);
    			model.addConstr(ex10, GRB.LESS_EQUAL, 0.0, "eq10_"+l);
    			
    			GRBLinExpr ex12 = new GRBLinExpr();
    			ex12.addTerm(1.0, vn[l]);
    			ex12.addTerm(M_N, z[l]);
    			model.addConstr(ex12, GRB.LESS_EQUAL, M_N, "eq12_"+l);
    			
    			GRBLinExpr ex11 = new GRBLinExpr();
    			GRBLinExpr ex13 = new GRBLinExpr();
    			ex11.addTerm(1.0, vp[l]);
    			ex13.addTerm(1.0, vn[l]);
    			for (int i=0;i<Xtrain.numRows();i++)
    			{
    				if (Ytrain.get(i)>1-numerical_instability_epsilon)
    					ex11.addTerm(-1.0, x[i][l]);
    				if (Ytrain.get(i)<numerical_instability_epsilon)
    					ex13.addTerm(-1.0, x[i][l]);
    				
    			}
    			model.addConstr(ex11, GRB.LESS_EQUAL, 0.0, "eq11_"+l);
    			model.addConstr(ex13, GRB.LESS_EQUAL, 0.0, "eq13_"+l);    			
    		}
    		
    		//CUT 1
    		for (int l=0;l<numTreeNodeVars/2+1;l++)
    		{
    			ArrayList<Integer> rightAncestors = new ArrayList<Integer>();
    			ArrayList<Integer> leftAncestors = new ArrayList<Integer>();
    			
    			int currIndex = numTreeNodeVars/2+l;
    			
    			int numLeftAncestors = 0;
    			int numRightAncestors = 0;
    			while (currIndex>0)
    			{
    				if (currIndex%2==1)
    				{
    					currIndex = currIndex/2;
    					leftAncestors.add(currIndex);
    					numLeftAncestors++;
    				}
    				else if (currIndex%2==0)
    				{
    					currIndex = currIndex/2 - 1;
    					rightAncestors.add(currIndex);
    					numRightAncestors++;
    				}
    			}
    			
    			
    			for (int i=0;i<Xtrain.numRows();i++)
    			{
    				GRBLinExpr ex11 = new GRBLinExpr();
        			GRBLinExpr ex12 = new GRBLinExpr();
        			GRBLinExpr ex13 = new GRBLinExpr();
    				for (int n=0;n<rightAncestors.size();n++)
    				{
    					ex11.addTerm(1.0, w[i][rightAncestors.get(n)]);
    					ex12.addTerm(1.0, w[i][rightAncestors.get(n)]);
    				}
    				
    				int ex11_constant=0;
    				for (int n=0;n<leftAncestors.size();n++)
    				{
    					ex11_constant++;
    					ex11.addTerm(-1.0, w[i][leftAncestors.get(n)]);
    					ex13.addTerm(1.0, w[i][leftAncestors.get(n)]);
    				}
    				
    				ex11.addTerm(1.0, x[i][l]);
    				GRBConstr const1 = model.addConstr(ex11, GRB.GREATER_EQUAL, 1 - ex11_constant, "eq11_"+i+"_"+l);
    				//const1.set(GRB.IntAttr.Lazy, 3);
    				
    				
    				if (numRightAncestors>0)
    				{
    					ex12.addTerm(numRightAncestors, x[i][l]);
    					GRBConstr const2 = model.addConstr(ex12, GRB.LESS_EQUAL, numRightAncestors, "eq12_"+i+"_"+l);
    					//const2.set(GRB.IntAttr.Lazy, 3);
    				}
    				

    				if (numLeftAncestors>0)
    				{
    					ex13.addTerm(-1*numLeftAncestors, x[i][l]);
    					GRBConstr const3 = model.addConstr(ex13, GRB.GREATER_EQUAL, 0, "eq13_"+i+"_"+l);
    					//const3.set(GRB.IntAttr.Lazy, 3);
    				}
    				
    				
    			}
    			
    			//CUT 2
    			GRBLinExpr ex14 = new GRBLinExpr();
    			GRBLinExpr ex15 = new GRBLinExpr();
    			for (int i=0;i<Xtrain.numRows();i++)
    			{
    				if (Ytrain.get(i)>1-numerical_instability_epsilon)
    				{
    					ex14.addTerm(1.0, x[i][l]);
    					ex15.addTerm(-1.0, x[i][l]);
    				}
    				if (Ytrain.get(i)<numerical_instability_epsilon)
    				{
    					ex14.addTerm(-1.0, x[i][l]);
    					ex15.addTerm(1.0, x[i][l]);
    				}
    			}
    			ex14.addTerm(-1*M_N, z[l]);
    			ex15.addTerm(M_P, z[l]);
    			
    			GRBConstr const1 = model.addConstr(ex14, GRB.GREATER_EQUAL, -1*M_N, "eq14_"+l);
    			GRBConstr const2 = model.addConstr(ex15, GRB.GREATER_EQUAL, 0, "eq15_"+l);
    			//const1.set(GRB.IntAttr.Lazy, 3);
    			//const2.set(GRB.IntAttr.Lazy, 3);
    		}
    		
    		//CUT 3...Implemented Lazily
    		SimpleMatrix featVals;
    		for (int f=0;f<Xtrain.numCols();f++)
    		{
    			featVals = Xtrain.extractVector(false, f);
    			
    			for (int n=0;n<numTreeNodeVars/2;n++)
    			{
    				for (int i=0;i<Xtrain.numRows();i++)
    				{
    					for (int istar=0;istar<Xtrain.numRows();istar++)
    					{
    						if (Xtrain.get(i,f)>Xtrain.get(istar,f))
    						{
    							GRBLinExpr ex16 = new GRBLinExpr();
    							ex16.addTerm(1.0, w[i][n]);
    							ex16.addTerm(-1.0, w[istar][n]);
    							ex16.addTerm(1.0, a[n][f]);
    							GRBConstr const1 = model.addConstr(ex16, GRB.LESS_EQUAL, 1.0, "eq16_"+n+"_"+f+"_"+i+"_"+istar);
    							const1.set(GRB.IntAttr.Lazy, 3);
    						}
    						else if (Xtrain.get(i,f)<=Xtrain.get(istar,f))
    						{
    							GRBLinExpr ex17 = new GRBLinExpr();
    							ex17.addTerm(1.0, w[istar][n]);
    							ex17.addTerm(-1.0, w[i][n]);
    							ex17.addTerm(1.0, a[n][f]);
    							GRBConstr const1 = model.addConstr(ex17, GRB.LESS_EQUAL, 1.0, "eq17_"+n+"_"+f+"_"+i+"_"+istar);
    							const1.set(GRB.IntAttr.Lazy, 3);
    						}
    					}
    				}
    			}
    		}
    			
		model.update();
    		
    	}
    	catch(GRBException e)
    	{
    		System.out.println("Error code: " + e.getErrorCode() + ". " +
                    e.getMessage());
    	}
    }

    public void printSolution()
    {
    	try
    	{
    	int numTreeNodeVars = (int) (Math.pow(2, treeDepth+1) - 1);
		for (int n=0;n<b.length;n++)
		{
			System.out.println("B Val: "+b[n].get(GRB.DoubleAttr.X));
			System.out.print("Node "+n+" : ");
			for (int f=0;f<Xtrain.numCols();f++)
			{
				if (a[n][f].get(GRB.DoubleAttr.X)>1-numerical_instability_epsilon)
				{
					System.out.println("a is "+f+" | b is "+b[n].get(GRB.DoubleAttr.X));
				}
			}
		}
		
		/*System.out.println("TP: "+ tp.get(GRB.DoubleAttr.X)+" TN: "+ tn.get(GRB.DoubleAttr.X));
		
		for (int i=0;i<z.length;i++)
        {
			System.out.println("z("+i+") is: "+z[i].get(GRB.DoubleAttr.X));
			System.out.println("vp("+i+") is: "+vp[i].get(GRB.DoubleAttr.X));
			System.out.println("vn("+i+") is: "+vn[i].get(GRB.DoubleAttr.X));
        }
		
		for (int l=0;l<numTreeNodeVars/2+1;l++)
		{
			int numPositives  = 0, numNegatives=0;
			for (int i=0;i<Xtrain.numRows();i++)
			{
				if (Ytrain.get(i)>1-numerical_instability_epsilon && x[i][l].get(GRB.DoubleAttr.X)>1-numerical_instability_epsilon)
					numPositives++;
				else if (Ytrain.get(i)<numerical_instability_epsilon && x[i][l].get(GRB.DoubleAttr.X)>1-numerical_instability_epsilon)
					numNegatives++;
			}
			System.out.println("Leaf "+l+" : +'s " + numPositives+" , -'s "+ numNegatives);
		}

               for (int i=0;i<Xtrain.numRows();i++)
                {
		    System.out.println("Datapoint "+i+".....");
                    System.out.print("w["+i+"]["+0+"]" + w[i][0].get(GRB.DoubleAttr.X)+" ");
		    System.out.print("w["+i+"]["+1+"] "+ w[i][1].get(GRB.DoubleAttr.X)+" ");
		    System.out.println("w["+i+"]["+2+"] "+ w[i][2].get(GRB.DoubleAttr.X));
                    System.out.print("x["+i+"]["+0+"] "+ x[i][0].get(GRB.DoubleAttr.X)+" ");
                    System.out.print("x["+i+"]["+1+"] "+ x[i][1].get(GRB.DoubleAttr.X)+" ");
                    System.out.print("x["+i+"]["+2+"] "+ x[i][2].get(GRB.DoubleAttr.X)+" ");
		    System.out.println("x["+i+"]["+3+"] "+ x[i][3].get(GRB.DoubleAttr.X)+" ");
                }*/
                
    	}
    	catch(GRBException e)
    	{
    		e.printStackTrace();
    	}
    }
    
    public void printRootRelaxationSolution()
    {
    	try
    	{
    	int numTreeNodeVars = (int) (Math.pow(2, treeDepth+1) - 1);
		for (int n=0;n<numTreeNodeVars/2;n++)
		{
			System.out.println("Node: "+n);
			System.out.println("B Val: "+b[n].get(GRB.DoubleAttr.X));
			System.out.print("A Val: ");
			for (int f=0;f<Xtrain.numCols();f++)
			{
				System.out.print(a[n][f].get(GRB.DoubleAttr.X)+" ");
			}
			System.out.println();
		}
		
		for (int l=0;l<numTreeNodeVars/2+1;l++)
			System.out.println("Z Val: "+z[l].get(GRB.DoubleAttr.X)+" ");
		
		
		for (int i=0;i<Xtrain.numRows();i++)
		{
			for (int l=0; l<numTreeNodeVars/2+1; l++)
			{
				System.out.print("X Val & Xhat Val: ");
			
				System.out.print(x[i][l].get(GRB.DoubleAttr.X)+" ");
				//System.out.print(lin[i][l].get(GRB.DoubleAttr.X)+" ,");		
			}
			System.out.println();
		}
		
		System.out.println("W Val");
		for (int i=0;i<Xtrain.numRows();i++)
		{
			for (int n=0;n<numTreeNodeVars/2;n++)
			{
				System.out.print(w[i][n].get(GRB.DoubleAttr.X)+" ");
			}
		}
		
		System.out.println();
		/*for (int i=0;i<Xtrain.numRows();i++)
		{
			if (Ytrain.get(i)==1)
				System.out.print("Positive datapoint "+(i+1)+" ");
			System.out.print(tp[i].get(GRB.DoubleAttr.X)+" ");
			System.out.println(tn[i].get(GRB.DoubleAttr.X)+" ");
		}*/
                
    	}
    	catch(GRBException e)
    	{
    		e.printStackTrace();
    	}
    }
    
    public void provideWarmStartCART(String filename)
    {
    	
		try 
		{
			DataSource source = new DataSource(filename);
			Instances data = source.getDataSet();
			if (data.classIndex() == -1)
				   data.setClassIndex(data.numAttributes() - 1);
			//data.deleteAttributeAt(0);
		
			
			REPTree tree = new REPTree();
			String[] options = weka.core.Utils.splitOptions("-M 0 -V 0.001 -N 3 -S 1 -L "+treeDepth+" -P -I 0.0");
			//String[] options = weka.core.Utils.splitOptions("-M "+Nmin+" -V 0.001 -N 3 -S 1 -L "+treeDepth+" -P -I 0.0");
			//String[] options = weka.core.Utils.splitOptions("-M "+Nmin+" -L "+treeDepth);
			tree.setOptions(options);
			tree.buildClassifier(data);
			
			System.out.println(tree.toString());
			
			Tree root = tree.m_Tree;
			
			TreeNode rootNode = new TreeNode(root, null);
			
			Queue queue = new LinkedList();
			queue.add(rootNode);
			
			
			int index=0;
			while(index<b.length && !queue.isEmpty())
			{
				TreeNode treenode = (TreeNode)queue.remove();
				Tree node = treenode.node;
				Tree parent = treenode.parentNode;
				if (node.m_Attribute==-1)	
				{
					b[index].set(GRB.DoubleAttr.Start, parent.m_SplitPoint);
				}
				else
				{
					b[index].set(GRB.DoubleAttr.Start, node.m_SplitPoint);
				}
				int tamp = node.m_Attribute;
				if (tamp==-1)///need to revisit
					tamp = parent.m_Attribute;
				for (int j=0;j<Xtrain.numCols();j++)
				{
					
					if (j==tamp)
						a[index][j].set(GRB.DoubleAttr.Start, 1.0);
					else
						a[index][j].set(GRB.DoubleAttr.Start, 0.0);
				}
				index++;
				
				//add children
				if (node.m_Attribute!=-1)
				{
					for (int j=0;j<node.m_Successors.length;j++)
					{
						Tree successor = node.m_Successors[j];
						Tree parentVal = node;
						TreeNode child = new TreeNode(successor, parentVal);
						queue.add(child);
					}
				}
				else//construct binary tree
				{
					for (int j=0;j<2;j++)
					{
						Tree successor = tree.dummyTree;
						Tree parentVal = parent;
						TreeNode child = new TreeNode(successor, parentVal);
						queue.add(child);
					}
				}
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
    }
    
    class TreeNode
    {
    	public Tree node;
    	public Tree parentNode;
    	
    	public TreeNode(Tree node, Tree parentNode)
    	{
    		this.node = node;
    		this.parentNode = parentNode;
    	}
    }
}

