package runners;

import gurobi.GRB;
import models.DecisionTree;

public class DecisionTreeRun {
	 public static void main(String[] args) {
		 	double interpretabilityWeights[] = {0.9, 0.6, 0.2, 0.5, 0.5, 0.4, 0.8, 0.3, 0.1, 0.2, 0.3, 0.1, 0.2, 0.5, 0.1, 0.4, 0.7, 0.1, 0.2, 0.4, 0.2};
		 	double optimalityGap=0.2;
		 	int Depth=3;
		 	int Nmin=1;
		 	double numFolds = 10;
		 	
		 	
		 	//String basedir = "/Users/amulyayadav/Dropbox/Work/PostAAMAS/EceProject/Clustering/5PhebeRegression/ForOneNodeOnly/Post-Change/normalized/"; 
		 	String basedir = "/Users/amulyayadav/Dropbox/Work/PostAAMAS/EceProject/Clustering/5PhebeRegression/Only2NodeGame/normalized-extended/";
		    try {
		    	
		    	///warm start solution using depth 1
		    	DecisionTree obj = new DecisionTree(basedir+"class-threshold6-newnormalize-20feat.csv", 1, Nmin, 0.0, interpretabilityWeights, basedir+"tradeoff-decisiontree-warmstart.txt", 1);
		    	obj.learnWekaDecisionTrees(basedir+"classification-weka.csv");
		    	//obj.model.getEnv().set(GRB.DoubleParam.MIPGap, optimalityGap);
				//obj.model.getEnv().set(GRB.DoubleParam.TimeLimit, 60);
		    	/*obj.model.optimize();
		    	
		    	
		    	
		    	
		    	DecisionTree actual[] = new DecisionTree[10];
		    	int i=0;
		    	for (double lambda = 0.0; lambda<0.1; lambda=lambda+0.1)
		    	{
		    		double averageError[] = new double[4];
		    		if (i<actual.length)
		    		{
		    			for (double j=0;j<1;j++)
		    			{
		    				actual[i] = new DecisionTree(basedir+"class-threshold6-newnormalize-20feat.csv", Depth, Nmin, lambda, interpretabilityWeights, basedir+"tradeoff-decisiontree.txt", 1 - numFolds/100);
		    				//actual[i].model.write(basedir+"model.lp");
		    		
		    				//if (i==0)
		    					actual[i].provideWarmStart(obj);
		    				//else
		    				//	actual[i].provideWarmStart(actual[i-1]);
		    		
		    				actual[i].model.getEnv().set(GRB.DoubleParam.MIPGap, optimalityGap);
		    				actual[i].model.getEnv().set(GRB.DoubleParam.TimeLimit, 60);
		    				//actual[i].model.getEnv().set(GRB.IntParam.NumericFocus, 3);
		    				actual[i].model.optimize();
		    				 
		    		
		    		
		    				if (actual[i].model.get(GRB.IntAttr.SolCount)>0)
		    				{
		    				actual[i].printSolution();
		    				double res[] = actual[i].evaluateClassifieronTestSet();
		    				for (int k=0;k<averageError.length;k++)
		    					averageError[k]+=res[k];
				    		System.out.println("Precision: "+res[0]/numFolds);
				    		System.out.println("Recall: "+res[1]/numFolds);
				    		System.out.println("F1: "+res[2]/numFolds);
				    		System.out.println("Misclassification rate: "+res[3]/numFolds);
		    				}
		    				else if (actual[i].model.get(GRB.IntAttr.Status)==9 && actual[i].model.get(GRB.IntAttr.SolCount)==0)
		    					System.out.println("Zero solutions found in the time limit. Need to increase time limit. No other option.");
		    				i++;
		    			}
		    		}
		    		System.out.println("Average precision (10 fold): "+averageError[0]/numFolds);
		    		System.out.println("Average recall (10 fold): "+averageError[1]/numFolds);
		    		System.out.println("Average f1 (10 fold): "+averageError[2]/numFolds);
		    		System.out.println("Average misclassification rate (10 fold): "+averageError[3]/numFolds);
		    	}
	
	    		//obj.model.reset();
	    		//obj.model.dispose();
	    		//obj.env.dispose();
	    		
	    		for (int j=0;j<actual.length;j++)
	    		{
		    		actual[j].model.reset();
		    		actual[j].model.dispose();
		    		actual[j].env.dispose();
	    		}*/
		    }
		    catch(Exception e)
		    {
		    	e.printStackTrace();
		    	System.out.println("Message: " + e.getMessage());
		    }
	 }
}
