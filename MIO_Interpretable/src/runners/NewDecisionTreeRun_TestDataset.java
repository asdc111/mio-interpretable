package runners;

import java.util.ArrayList;

import org.ejml.data.DenseMatrix64F;
import org.ejml.ops.MatrixIO;
import org.ejml.simple.SimpleMatrix;

import gurobi.GRB;
import gurobi.GRBConstr;
import models.NewDecisionTree;
import models.NewDecisionTree_Revamped;

public class NewDecisionTreeRun_TestDataset {
	 public static void main(String[] args) {
		 	double interpretabilityWeights[] = {0.9, 0.6, 0.2, 0.5, 0.5, 0.4, 0.8, 0.3, 0.1, 0.2, 0.3, 0.1, 0.2, 0.5, 0.1, 0.4, 0.7, 0.3, 0.5, 0.5};
		 	double optimalityGap=0.2;
		 	int Depth=2;
		 	int Nmin=1;
		 	double numFolds = 1;
		 	
		 	double fraction = 1;
		 	
		 	int numFeatures=22, numPoints=2000;
		 	
		 
		 	
		 	DenseMatrix64F dataset;
		    SimpleMatrix originalData, data, train, test;
		    
		    String basedir = "/Users/amulyayadav/Dropbox/Work/PostAAMAS/PhebeWork/Bottleneck/";
		    try 
		    {	
	    		dataset = MatrixIO.loadCSV(basedir+"masterdataset-mip.csv");
		    	//dataset = MatrixIO.loadCSV(basedir+"bcancer-normalize.csv");
	    		
	    		originalData = SimpleMatrix.wrap(dataset);
	    	
	    		
	    		//data = originalData;
	    		data = originalData.extractMatrix( 0, numPoints, originalData.numCols() - numFeatures , originalData.numCols());
	    		
	    		//data.print();
	    		//data.printDimensions();
	    		
	    		///renormalize data
	    		/*for (int i=0;i<data.numCols()-1;i++)
	    		{
	    			double maxVal=-1, minVal=10000;
	    			for (int j=0;j<data.numRows();j++)
	    			{
	    				if (data.get(j,i)<minVal)
	    					minVal=data.get(j,i);
	    				if (data.get(j,i)>maxVal)
	    					maxVal=data.get(j,i);
	    			}
	    			
	    			if (maxVal-minVal!=0)
	    			{
	    				for (int j=0;j<data.numRows();j++)
	    				{
	    					data.set(j, i, (data.get(j,i)-minVal)/(maxVal-minVal));
	    				}
	    			}
	    		}
	    		///submatrix renormalized
	    		
	    		
	    		
	    		ArrayList<Integer> ones = new ArrayList<Integer>();
	    		ArrayList<Integer> zeros = new ArrayList<Integer>();
	    		
	    		for (int i=0;i<data.numRows();i++)
	    		{
	    			if (data.get(i, data.numCols()-1)==1)
	    				ones.add(i);
	    			else
	    				zeros.add(i);
	    		}
	    		
	    		
	    		ArrayList<Integer> trainIndices = new ArrayList<Integer>();
	    		ArrayList<Integer> testIndices = new ArrayList<Integer>();
	    		
	    		int numberOfOnes = (int)(fraction*ones.size());
	    		int numberOfZeros = (int)(fraction*zeros.size());
	    		
	    		for (int i=0;i<numberOfOnes; i++)
	    		{
	    			int randInt = (int)(Math.random()*ones.size());
	    			trainIndices.add(ones.get(randInt));
	    			ones.remove(randInt);
	    		}
	    		
	    		for (int i=0;i<numberOfZeros; i++)
	    		{
	    			int randInt = (int)(Math.random()*zeros.size());
	    			trainIndices.add(zeros.get(randInt));
	    			zeros.remove(randInt);
	    		}
	    		
	    		for (int i=0;i<ones.size();i++)
	    			testIndices.add(ones.get(i));
	    		for (int i=0;i<zeros.size();i++)
	    			testIndices.add(zeros.get(i));
	    		
	    		
	    		train = new SimpleMatrix(0,0);
	    		test = new SimpleMatrix(0,0);
	    		
	    		for (int i=0;i<trainIndices.size();i++)
	    			train = train.combine(SimpleMatrix.END,0,data.extractVector(true, trainIndices.get(i)));
	    		
	    		for (int i=0;i<testIndices.size();i++)
	    			test = test.combine(SimpleMatrix.END,0,data.extractVector(true, testIndices.get(i)));
	    		
	    		train.print(5,6);
	    		*/
	    		
	    		train = data;
	    		test = data;

	    		///train and test data constructed
	    		
	    		//NewDecisionTree actual;
	    		NewDecisionTree_Revamped actual;
	    		double averageError[] = new double[4];
	    		for (double i=0;i<numFolds;i++)
	    		{
	    			//actual = new NewDecisionTree(basedir+"masterdataset-mip.csv", Depth, Nmin, 0.0, interpretabilityWeights, basedir+"tradeoff-decisiontree.txt", 1 - numFolds/100, train, test );
	    			actual = new NewDecisionTree_Revamped(basedir+"masterdataset-mip.csv", Depth, Nmin, 0.0, interpretabilityWeights, basedir+"tradeoff-decisiontree.txt", 1 - numFolds/100, train, test );

	    			//actual.provideWarmStartCART(basedir+"masterdataset-mip-weka.csv");
	    			//actual.model.getEnv().set(GRB.DoubleParam.MIPGap, optimalityGap);
    				//actual.model.getEnv().set(GRB.DoubleParam.TimeLimit, 900);
    				///use primal simplex
    				actual.model.getEnv().set(GRB.IntParam.Method, 0);
    				//actual.model.getEnv().set(GRB.DoubleParam.FeasibilityTol, 1e-7);
    				//actual.model.getEnv().set(GRB.DoubleParam.IntFeasTol, 1e-7);
    				//actual.model.getEnv().set(GRB.IntParam.SubMIPNodes, Integer.MAX_VALUE);
    				
    				
    				actual.model.write(basedir+"model.lp");
    				actual.model.optimize();
    				
    				/////////////////////////////
    				/*System.out.println("The model is infeasible; computing IIS");
				      actual.model.computeIIS();
				      System.out.println("\nThe following constraint(s) "
				          + "cannot be satisfied:");
				      for (GRBConstr c : actual.model.getConstrs()) {
				        if (c.get(GRB.IntAttr.IISConstr) == 1) {
				          System.out.println(c.get(GRB.StringAttr.ConstrName));
				        }
				      }
				      
				      actual.model.write(basedir+"model.ilp");*/
    				/////////////////////////////////////////////
				      
				      
    				System.out.println("Fold number: "+i);
    				System.out.println("Status: "+actual.model.get(GRB.IntAttr.Status));
    				System.out.println("Runtime: "+actual.model.get(GRB.DoubleAttr.Runtime));
    				
    				if (actual.model.get(GRB.IntAttr.SolCount)>0)
    				{
    					actual.printSolution();
    					System.out.println(actual.model.get(GRB.DoubleAttr.ObjVal)+", "+ actual.model.get(GRB.DoubleAttr.Runtime));
    					//System.out.println();
    					/*double res[] = actual.evaluateClassifieronTestSet();
    					for (int k=0;k<averageError.length;k++)
    						averageError[k]+=res[k];
    					System.out.println("Precision: "+res[0]);
    					System.out.println("Recall: "+res[1]);
    					System.out.println("F1: "+res[2]);
    					System.out.println("Misclassification rate: "+res[3]);*/
    				}
    				else if (actual.model.get(GRB.IntAttr.Status)==9 && actual.model.get(GRB.IntAttr.SolCount)==0)
    					System.out.println("Zero solutions found in the time limit for this fold of data. Need to increase time limit. No other option.");
    				
	    				/*actual[i].model.getEnv().set(GRB.DoubleParam.MIPGap, optimalityGap);
	    				actual[i].model.getEnv().set(GRB.DoubleParam.TimeLimit, 60);
	    				System.out.println("Method: "+actual[i].model.get(GRB.IntParam.Method));
	    				//actual[i].model.getEnv().set(GRB.IntParam.NumericFocus, 3);
	    				actual[i].model.optimize();
	    				 
	    		
	    				System.out.println("Status: "+actual[i].model.get(GRB.IntAttr.Status));
	    				System.out.println("Number of solutions: "+actual[i].model.get(GRB.IntAttr.SolCount));
	    	
	    				if (actual[i].model.get(GRB.IntAttr.SolCount)>0)
	    				{
	    				actual[i].printSolution();
	    				double res[] = actual[i].evaluateClassifieronTestSet();
	    				for (int k=0;k<averageError.length;k++)
	    					averageError[k]+=res[k];
			    		System.out.println("Precision: "+res[0]);
			    		System.out.println("Recall: "+res[1]);
			    		System.out.println("F1: "+res[2]);
			    		System.out.println("Misclassification rate: "+res[3]);
	    				}
	    				else if (actual[i].model.get(GRB.IntAttr.Status)==9 && actual[i].model.get(GRB.IntAttr.SolCount)==0)
	    					System.out.println("Zero solutions found in the time limit. Need to increase time limit. No other option.");*/
	    				
	    		}
	    		
	    		/*System.out.println("Average precision (10 fold): "+averageError[0]/numFolds);
	    		System.out.println("Average recall (10 fold): "+averageError[1]/numFolds);
	    		System.out.println("Average f1 (10 fold): "+averageError[2]/numFolds);
	    		System.out.println("Average misclassification rate (10 fold): "+averageError[3]/numFolds);*/
	    	
	    		
		    }
		    catch(Exception e)
		    {
		    	e.printStackTrace();
		    }
	 }
}
