package runners;

import java.util.Random;

import weka.classifiers.Evaluation;
import weka.classifiers.trees.J48_Interpretable;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;

public class C45_InterpretableRun {
	 public static void main(String[] args) {
		 	double interpretabilityWeights[] = {0.9, 0.6, 0.2, 0.5};
		 	String basedir = "/Users/amulyayadav/Dropbox/Work/PostAAMAS/EceProject/Clustering/5PhebeRegression/For2NodesOnly/normalized/";
	    	try
	    	{
	    		DataSource source = new DataSource(basedir+"classification-weka.csv");
	    		Instances data = source.getDataSet();
	    		if (data.classIndex() == -1)
	    			data.setClassIndex(data.numAttributes() - 1);
	    		int seed = 1;
	    		int folds=10;
	    		
	    		 Random rand = new Random(seed);   // create seeded number generator
	    		 Instances randData = new Instances(data);   // create copy of original data
	    		 randData.randomize(rand);         // randomize data with number generator
	    		 randData.stratify(folds);
	    		 
	    		 double averageErrorRate=0;
	    		 for (int i=0;i<10;i++)
	    		 {
	    			 Instances train = randData.trainCV(folds,i);
	    			 Instances test = randData.testCV(folds,i);
	    			 
	    			 
	    			 J48_Interpretable tree = new J48_Interpretable(interpretabilityWeights,0);
	    			 String[] options = weka.core.Utils.splitOptions("-B");
	    			 tree.setOptions(options);
	    			 tree.buildClassifier(train);
	    			 
	    			// REPTree tree = new REPTree();
	    			 //String[] options = weka.core.Utils.splitOptions("-M "+Nmin+" -L "+treeDepth);
	    			 //String[] options = weka.core.Utils.splitOptions("-M "+Nmin);
	    			 //tree.setOptions(options);
	    			 //tree.buildClassifier(train);
	    			 String treeOutput = tree.toString();
	    			 
	    			 
	    			 Evaluation evaluation = new Evaluation(train);
	    			 evaluation.evaluateModel(tree, test);
	    			 System.out.println("Fold: "+i);
	    			 System.out.println(evaluation.toSummaryString());
	    			 averageErrorRate += evaluation.errorRate();
	    			 System.out.println(evaluation.errorRate());
	    		 }
	    		 System.out.println("Average error rate: "+ averageErrorRate/folds);
	    	}
	    	catch(Exception e)
	    	{
	    		e.printStackTrace();
	    	}
	 }
}
