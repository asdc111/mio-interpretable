package runners;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import org.ejml.data.DenseMatrix64F;
import org.ejml.ops.MatrixIO;
import org.ejml.simple.SimpleMatrix;

import gurobi.GRB;
import gurobi.GRBCallback;
import gurobi.GRBException;
import gurobi.GRBLinExpr;
import models.Dtree4;

public class Dtree4Run extends GRBCallback {
	
	public static Dtree4 actual;
	public static String basedir;
	public static int Depth;
 	public static double timeLimit;
 	static double numerical_instability_epsilon = 0.001;

	 public static void main(String[] args) {
		 
		 	
		 	basedir = "/Users/amulyayadav/Dropbox/Work/PostAAMAS/PhebeWork/Bottleneck/";
		 	Depth=2;
		 	timeLimit = 600;
		 	
		 	
		 
		 	
		 	DenseMatrix64F dataset;
		    SimpleMatrix originalData, data, train, test;
		    
            try
            {
                
                /*String basedir = args[0];
                int Depth=Integer.parseInt(args[1]);
                double timeLimit = Double.parseDouble(args[2]);*/
                
            	
                
                //dataset = MatrixIO.loadCSV(basedir+"masterdataset-mip.csv");
            	dataset = MatrixIO.loadCSV(basedir+"BinaryDatasets/german_numer/german_numer_normalized_pres2.csv");
            	
                originalData = SimpleMatrix.wrap(dataset);


                data = originalData;


                train = data;
                ////train and test constructed
                
                
                ///running code  
                actual = new Dtree4(Depth, train);
                //actual.model.set(GRB.IntParam.LazyConstraints, 1);
                actual.model.getEnv().set(GRB.IntParam.Method, 0);
                actual.model.getEnv().set(GRB.IntParam.BranchDir, 1);
                actual.model.getEnv().set(GRB.IntParam.NodeMethod, 0);
                //actual.model.getEnv().set(GRB.DoubleParam.TimeLimit, timeLimit);
                //actual.model.getEnv().set(GRB.IntParam.MIPFocus, 1);
                
                //Dtree4Run cb = new Dtree4Run();
                
               //actual.model.setCallback(cb);
                //actual.provideWarmStartCART(basedir+"masterdataset-mip-weka.csv");
                //actual.model.write(basedir+"model.lp");
                actual.model.optimize();
				
	    	
		actual.printRootRelaxationSolution();	
		System.out.println("Solution Quality: "+actual.model.get(GRB.DoubleAttr.ObjVal));
		System.out.println("Runtime: "+actual.model.get(GRB.DoubleAttr.Runtime));
				
 
            }
            catch(IOException e)
            {
            	e.printStackTrace();
            } catch (GRBException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		    

	 }

	@Override
	protected void callback() {
		try
		{
			if (where==GRB.Callback.MIPNODE && getIntInfo(GRB.Callback.MIPNODE_STATUS)==GRB.Status.OPTIMAL)
			{	
				int numTreeNodeVars = (int) (Math.pow(2, Depth+1) - 1);
				double aVals[][] = new double[numTreeNodeVars/2][actual.Xtrain.numCols()];
				double bVals[] = new double[numTreeNodeVars/2];
				double zVals[] = new double[numTreeNodeVars/2+1];
				double wVals[][] = new double[actual.Xtrain.numRows()][numTreeNodeVars/2];
				aVals = getNodeRel(actual.a);
				bVals = getNodeRel(actual.b);
				wVals = getNodeRel(actual.w);
				zVals = getNodeRel(actual.z);
				
				double relaxation_objval=0;
				for (int i=0;i<actual.Xtrain.numRows();i++)
    			{
    				if (actual.Ytrain.get(i)>1-numerical_instability_epsilon)
    					relaxation_objval += getNodeRel(actual.tp[i]);
    				else if (actual.Ytrain.get(i)<numerical_instability_epsilon)
    					relaxation_objval += getNodeRel(actual.tn[i]);
    			}
				System.out.println("LP relaxation objective: "+ relaxation_objval);
				
				for (int n=0;n<numTreeNodeVars/2;n++)
				{
					System.out.print("Node "+n+ " "+bVals[n]+" --> ");
					for (int f=0;f<actual.Xtrain.numCols();f++)
						System.out.print(aVals[n][f]+ " ");
					System.out.println();
				}
				
				for (int i=0;i<actual.Xtrain.numRows();i++)
				{
					for (int n=0;n<numTreeNodeVars/2;n++)
						System.out.print(wVals[i][n]+" ");
					
				}
				System.out.println("Z Vals:");
				for (int l=0;l<numTreeNodeVars/2+1;l++)
					System.out.print(zVals[l]+" ");
				System.out.println();
				
				int finalN1=-2;
	    		for (int i=0;i<actual.Xtrain.numRows();i++)
	    		{
	    			boolean isIntegral=true;
	    			for (int n=0;n<numTreeNodeVars/2;n++)
	    			{
	    				if (wVals[i][n]>numerical_instability_epsilon && wVals[i][n]<1-numerical_instability_epsilon)
	    				{
	    					isIntegral = false;
	    					//System.out.println("FRACTIONAL W: "+ i + n);
	    					break;
	    				}
	    			}
	    			if (isIntegral==false)
	    			{
	    				finalN1=i-1;
	    				break;
	    			}
	    		}
	    		
	    		if (finalN1!=-2)
	    			System.out.println("The W's are not integral at this node");
				
				
				int finalN=-2;
	    		for (int n=0;n<numTreeNodeVars/2;n++)
	    		{
	    			boolean isIntegral=true;
	    			for (int f=0;f<actual.Xtrain.numCols();f++)
	    			{
	    				if (aVals[n][f]>numerical_instability_epsilon && aVals[n][f]<1-numerical_instability_epsilon)
	    				{
	    					isIntegral=false;
	    					break;
	    				}
	    			}
	    			if (isIntegral==false)
	    			{
	    				finalN=n-1;
	    				break;
	    			}
	    		}
				
	    		//System.out.println("The best bound at this node is: "+getDoubleInfo(GRB.Callback.MIPNODE_OBJBND));
				
	    		if (finalN==-2)
	    		{
	    			///upper bound
	    			/*double upperBound = predict(aVals, bVals);
	    			double currentBestObj = getDoubleInfo(GRB.Callback.MIPNODE_OBJBST);
	    			//if (upperBound > currentBestObj)
	    			//{
	    				System.out.println("The upper bound added on t is: "+upperBound);
	    				GRBLinExpr cut1 = new GRBLinExpr();
	    				for (int i=0;i<actual.Xtrain.numRows();i++)
		    			{
		    				if (actual.Ytrain.get(i)>1-numerical_instability_epsilon)
		    					cut1.addTerm(1.0, actual.tp[i]);
		    				else if (actual.Ytrain.get(i)<numerical_instability_epsilon)
		    					cut1.addTerm(1.0, actual.tn[i]);
		    			}
	    				//cut1.addTerm(1.0, master.t);
	    				addCut(cut1, GRB.LESS_EQUAL, upperBound);*/
	    			//}
	    			//upper bound end
	    		}
	    		
			}
			/*if (where==GRB.Callback.MIPNODE && getIntInfo(GRB.Callback.MIPNODE_STATUS)==GRB.Status.OPTIMAL)
			{	
				int numTreeNodeVars = (int) (Math.pow(2, Depth+1) - 1);
				double aVals[][] = new double[numTreeNodeVars/2][actual.Xtrain.numCols()];
				double bVals[] = new double[numTreeNodeVars/2];
				aVals = getNodeRel(actual.a);
				bVals = getNodeRel(actual.b);
				
				int finalN=-1;
	    		for (int n=0;n<numTreeNodeVars/2;n++)
	    		{
	    			boolean isIntegral=true;
	    			for (int f=0;f<actual.Xtrain.numCols();f++)
	    			{
	    				if (aVals[n][f]>numerical_instability_epsilon && aVals[n][f]<1-numerical_instability_epsilon)
	    				{
	    					isIntegral=false;
	    					break;
	    				}
	    			}
	    			if (isIntegral==false)
	    			{
	    				finalN=n-1;
	    				break;
	    			}
	    		}
				
	    		
				
	    		if (finalN!=-1)
	    		{
	    			///upper bound
	    			double upperBound = predict(aVals, bVals);
	    			GRBLinExpr cut1 = new GRBLinExpr();
	    			for (int i=0;i<actual.Xtrain.numRows();i++)
	    			{
	    				if (actual.Ytrain.get(i)>1-numerical_instability_epsilon)
	    					cut1.addTerm(1.0, actual.tp[i]);
	    				else if (actual.Ytrain.get(i)<numerical_instability_epsilon)
	    					cut1.addTerm(1.0, actual.tn[i]);
	    			}
	    			addCut(cut1, GRB.LESS_EQUAL, upperBound);
	    			//upper bound end
	    		
	    			///lower bound
	    			//complete the remaining tree with integral values
		    		for (int n=finalN+1;n<numTreeNodeVars/2;n++)
		    		{
		    			int parent;
		    			if (n%2==0)///look at tree with root starting from 0
		    				parent = n/2 - 1;
		    			else
		    				parent = n/2;
		    			
		    			for (int f=0;f<actual.Xtrain.numCols();f++)
		    				aVals[n][f] = aVals[parent][f];
		    			bVals[n] = bVals[parent];
		    		}
		    	
		    		double lowerBound = predict(aVals, bVals);
		    		GRBLinExpr cut2 = new GRBLinExpr();
		    		for (int i=0;i<actual.Xtrain.numRows();i++)
		    		{
		    			if (actual.Ytrain.get(i)>1-numerical_instability_epsilon)
		    				cut2.addTerm(1.0, actual.tp[i]);
		    			else if (actual.Ytrain.get(i)<numerical_instability_epsilon)
		    				cut2.addTerm(1.0, actual.tn[i]);
		    		}
		    		addCut(cut2, GRB.GREATER_EQUAL, lowerBound);
		    		//lower bound end
				}
	    		
			}*/
				
				//System.out.println("Number of nodes in node: "+getDoubleInfo(GRB.Callback.MIPNODE_NODCNT));
			
		}
		catch(GRBException e)
		{
			e.printStackTrace();
		}
		
	}

	public double predict(double a[][], double b[])
	{
		int numTreeNodeVars = (int) (Math.pow(2, Depth+1) - 1);
		
		int leaves[][] = new int[numTreeNodeVars/2+1][2];
		for (int l=0;l<numTreeNodeVars/2+1;l++)
			for (int j=0;j<2;j++)
				leaves[l][j]=0;
		
		int currNode;
		for (int i=0;i<actual.Xtrain.numRows();i++)
		{
			currNode=0;
			while(currNode<numTreeNodeVars/2)
			{
				double signAtNode=0;
				for (int f=0;f<actual.Xtrain.numCols();f++)
					signAtNode += actual.Xtrain.get(i, f)*a[currNode][f];
				signAtNode -= b[currNode];
			
				if (signAtNode<0)
					currNode=2*currNode+1;
				else if (signAtNode>=0)
					currNode=2*currNode+2;
			}
			if (actual.Ytrain.get(i)<numerical_instability_epsilon)
				leaves[currNode-numTreeNodeVars/2][0]++;
			else if (actual.Ytrain.get(i)>1-numerical_instability_epsilon)
				leaves[currNode-numTreeNodeVars/2][1]++;
		}
		
		double correctClassifications=actual.Xtrain.numRows();
		for (int l=0;l<numTreeNodeVars/2+1;l++)
		{
			if (leaves[l][0]>leaves[l][1])
				correctClassifications -= leaves[l][1];///false negatives
			else if (leaves[l][0]<=leaves[l][1])
				correctClassifications -= leaves[l][0];//false positives
		}
		return correctClassifications;
	}
}

