package runners;

import java.util.ArrayList;

import org.ejml.data.DenseMatrix64F;
import org.ejml.ops.MatrixIO;
import org.ejml.simple.SimpleMatrix;

import gurobi.GRB;
import gurobi.GRBConstr;
import models.NewDecisionTree;

public class NewDecisionTreeRun {
	
	 public static void main(String[] args) {
		 	double interpretabilityWeights[] = {0.9, 0.6, 0.2, 0.5, 0.5, 0.4, 0.8, 0.3, 0.1, 0.2, 0.3, 0.1, 0.2, 0.5, 0.1, 0.4, 0.7, 0.3, 0.5, 0.5};
		 	double optimalityGap=0.2;
		 	int Depth=1;
		 	int Nmin=1;
		 	double numFolds = 10;
		 	
		 	int numberOfNetworks=100;
		 	
		    DenseMatrix64F dataset;
		    SimpleMatrix data, train, test;
		 
		    SimpleMatrix networks[] = new SimpleMatrix[numberOfNetworks];
		    
		    
		 	//String basedir = "/Users/amulyayadav/Dropbox/Work/PostAAMAS/EceProject/Clustering/5PhebeRegression/FinalDataset/normalized-extended/v1NetNum/";
		    String basedir = "/Users/amulyayadav/Dropbox/Work/PostAAMAS/PhebeWork/BottleNeck/";
		    try {
		    	
	    		dataset = MatrixIO.loadCSV(basedir+"masterdataset-mip.csv");
	    		data = SimpleMatrix.wrap(dataset);
	    	
	    		///extract different networks into different matrices
	    		int startIndex=0, finalIndex=0;
	    		for (int i=1;i<=numberOfNetworks;i++)
	    		{
	    			startIndex=finalIndex;
	    			double val = data.get(finalIndex,0);
	    			for (;finalIndex<data.numRows() && data.get(finalIndex,0)==val;finalIndex++);
	    			//finalIndex--;
	    			
	    			networks[i-1] = data.extractMatrix(startIndex, finalIndex, 0, data.numCols());
	    		}
	    		
	    		ArrayList<Integer> trainIndices = new ArrayList<Integer>();
	    		
	    		ArrayList<Integer> testIndices = new ArrayList<Integer>();
	    		for (int i=1;i<=numberOfNetworks;i++)
	    			testIndices.add(i);
	    		
	    		
	    		for (int i=0;i<100;i++)
	    		{
	    			int randInt=-1;
	    			int index=-1;
	    			while(index==-1)
	    			{
	    				randInt = (int)(Math.random()*100 +1);
	    			
	    				index = testIndices.indexOf(randInt);
	    			}
	    			testIndices.remove(index);
	    			
	    			trainIndices.add(randInt);
	    			
	    		}
	    		
	    		System.out.println("Number of networks in train set: "+trainIndices.size());
	    		System.out.println("Number of networks in test set: "+testIndices.size());
	    		
	    		
	    		train = new SimpleMatrix(0,0);
	    		test = new SimpleMatrix(0,0);
	    		
	    		
	    		
	    		for (int i=0;i<trainIndices.size();i++)
	    		{
	    			train = train.combine(SimpleMatrix.END,0,networks[trainIndices.get(i)-1]);
	    			/*if (i==0)
	    				train = train.combine(0,0,networks[trainIndices.get(i)-1]);
	    			else
	    				train = train.combine(train.numRows()-1,0,networks[trainIndices.get(i)-1]);*/
	    		}
	    		
	    		for (int i=0;i<testIndices.size();i++)
	    		{
	    			test = test.combine(SimpleMatrix.END,0,networks[testIndices.get(i)-1]);
	    			/*if (i==0)
	    				test = test.combine(0,0,networks[testIndices.get(i)-1]);
	    			else
	    				test = test.combine(test.numRows()-1,0,networks[testIndices.get(i)-1]);*/
	    		}
	    		
	    		
	    		
	    		
	    		///train and test data constructed
	    		
		    	
		    	
		    	
		    	NewDecisionTree actual;
		    	for (double lambda = 0.0; lambda<0.1; lambda=lambda+0.1)
		    	{
		    		double averageError[] = new double[4];

		    		for (double j=0;j<1;j++)
		    		{
		    			actual = new NewDecisionTree(basedir+"masterdataset-mip.csv", Depth, Nmin, lambda, interpretabilityWeights, basedir+"tradeoff-decisiontree.txt", 1 - numFolds/100, train, test );
		    		
		    			//actual.provideWarmStartCART(basedir+"classification-weka.csv");
		    			//actual.model.getEnv().set(GRB.DoubleParam.MIPGap, optimalityGap);
	    				//actual.model.getEnv().set(GRB.DoubleParam.TimeLimit, 900);
	    				///use primal simplex
	    				actual.model.getEnv().set(GRB.IntParam.Method, 0);
	    				//actual.model.getEnv().set(GRB.IntParam.SubMIPNodes, Integer.MAX_VALUE);
	    				
	    				
	    				actual.model.write(basedir+"model.lp");
	    				actual.model.optimize();
	    				
	    				System.out.println("Status: "+actual.model.get(GRB.IntAttr.Status));
	    				System.out.println("Number of solutions: "+actual.model.get(GRB.IntAttr.SolCount));
	    				
	    				   /*System.out.println("The model is infeasible; computing IIS");
	    				      actual.model.computeIIS();
	    				      System.out.println("\nThe following constraint(s) "
	    				          + "cannot be satisfied:");
	    				      for (GRBConstr c : actual.model.getConstrs()) {
	    				        if (c.get(GRB.IntAttr.IISConstr) == 1) {
	    				          System.out.println(c.get(GRB.StringAttr.ConstrName));
	    				        }
	    				      }
	    				      
	    				      actual.model.write(basedir+"model.ilp");*/
	    	
	    				if (actual.model.get(GRB.IntAttr.SolCount)>0)
	    				{
	    				//actual.printSolution();
	    				double res[] = actual.evaluateClassifieronTestSet();
	    				for (int k=0;k<averageError.length;k++)
	    					averageError[k]+=res[k];
			    		System.out.println("Precision: "+res[0]);
			    		System.out.println("Recall: "+res[1]);
			    		System.out.println("F1: "+res[2]);
			    		System.out.println("Misclassification rate: "+res[3]);
	    				}
	    				else if (actual.model.get(GRB.IntAttr.Status)==9 && actual.model.get(GRB.IntAttr.SolCount)==0)
	    					System.out.println("Zero solutions found in the time limit. Need to increase time limit. No other option.");
	    				
		    				/*actual[i].model.getEnv().set(GRB.DoubleParam.MIPGap, optimalityGap);
		    				actual[i].model.getEnv().set(GRB.DoubleParam.TimeLimit, 60);
		    				System.out.println("Method: "+actual[i].model.get(GRB.IntParam.Method));
		    				//actual[i].model.getEnv().set(GRB.IntParam.NumericFocus, 3);
		    				actual[i].model.optimize();
		    				 
		    		
		    				System.out.println("Status: "+actual[i].model.get(GRB.IntAttr.Status));
		    				System.out.println("Number of solutions: "+actual[i].model.get(GRB.IntAttr.SolCount));
		    	
		    				if (actual[i].model.get(GRB.IntAttr.SolCount)>0)
		    				{
		    				actual[i].printSolution();
		    				double res[] = actual[i].evaluateClassifieronTestSet();
		    				for (int k=0;k<averageError.length;k++)
		    					averageError[k]+=res[k];
				    		System.out.println("Precision: "+res[0]);
				    		System.out.println("Recall: "+res[1]);
				    		System.out.println("F1: "+res[2]);
				    		System.out.println("Misclassification rate: "+res[3]);
		    				}
		    				else if (actual[i].model.get(GRB.IntAttr.Status)==9 && actual[i].model.get(GRB.IntAttr.SolCount)==0)
		    					System.out.println("Zero solutions found in the time limit. Need to increase time limit. No other option.");*/
		    				
		    			}
		    		
		    		System.out.println("Average precision (10 fold): "+averageError[0]/numFolds);
		    		System.out.println("Average recall (10 fold): "+averageError[1]/numFolds);
		    		System.out.println("Average f1 (10 fold): "+averageError[2]/numFolds);
		    		System.out.println("Average misclassification rate (10 fold): "+averageError[3]/numFolds);
		    	}
	
	    		//obj.model.reset();
	    		//obj.model.dispose();
	    		//obj.env.dispose();
	    		
	    		/*for (int j=0;j<actual.length;j++)
	    		{
		    		actual[j].model.reset();
		    		actual[j].model.dispose();
		    		actual[j].env.dispose();
	    		}*/
		    }
		    catch(Exception e)
		    {
		    	e.printStackTrace();
		    	System.out.println("Message: " + e.getMessage());
		    }
	 }
}



