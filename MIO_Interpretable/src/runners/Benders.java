package runners;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import org.ejml.data.DenseMatrix64F;
import org.ejml.ops.MatrixIO;
import org.ejml.simple.SimpleMatrix;

import gurobi.GRB;
import gurobi.GRBConstr;
import gurobi.GRBException;
import benders.Master;

public class Benders {

	 public static void main(String[] args) {
		 
		 	
		 	String basedir = "/Users/amulyayadav/Dropbox/Work/PostAAMAS/PhebeWork/Bottleneck/";
		 	int Depth=2;
		 	double timeLimit = 1800;
		 	
		 	
		 
		 	
		 	DenseMatrix64F dataset;
		    SimpleMatrix originalData, data, train, test;
		    
            try
            {
                
                /*String basedir = args[0];
                int Depth=Integer.parseInt(args[1]);
                double timeLimit = Double.parseDouble(args[2]);*/
                
            	
                
                dataset = MatrixIO.loadCSV(basedir+"masterdataset-mip.csv");

                originalData = SimpleMatrix.wrap(dataset);


                data = originalData;


                train = data;
                ////train and test constructed
                
                
                ///running code  
                Master actual = new Master(Depth, train);
                actual.provideWarmStartCART(basedir+"masterdataset-mip-weka.csv");
                actual.model.getEnv().set(GRB.IntParam.Method, 0);
                actual.model.getEnv().set(GRB.IntParam.BranchDir, 1);
                actual.model.getEnv().set(GRB.IntParam.NodeMethod, 0);
                actual.model.write(basedir+"model.lp");
                
                actual.model.setCallback(actual);
                
                actual.model.optimize();
                
                System.out.println("The model is infeasible; computing IIS");
			      actual.model.computeIIS();
			      System.out.println("\nThe following constraint(s) "
			          + "cannot be satisfied:");
			      for (GRBConstr c : actual.model.getConstrs()) {
			        if (c.get(GRB.IntAttr.IISConstr) == 1) {
			          System.out.println(c.get(GRB.StringAttr.ConstrName));
			        }
			      }
			      
			      actual.model.write(basedir+"model.ilp");
				
	    	
		//actual.printRootRelaxationSolution();	
		System.out.println("Solution Quality: "+actual.model.get(GRB.DoubleAttr.ObjVal));
		System.out.println("Runtime: "+actual.model.get(GRB.DoubleAttr.Runtime));
				
 
            }
            catch(IOException e)
            {
            	e.printStackTrace();
            } catch (GRBException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		    

	 }
}

