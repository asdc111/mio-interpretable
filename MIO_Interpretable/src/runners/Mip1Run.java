package runners;

/* Copyright 2016, Gurobi Optimization, Inc. */

/* This example formulates and solves the following simple MIP model:

     maximize    x +   y + 2 z
     subject to  x + 2 y + 3 z <= 4
                 x +   y       >= 1
     x, y, z binary
*/

import gurobi.*;
import models.Mip1;

public class Mip1Run {
  public static void main(String[] args) {
    try {
    	
    	Mip1 obj = new Mip1();
    	

    	obj.model.optimize();
    	
    	obj.printSolution();

    	// Dispose of model and environment

    	obj.model.dispose();
    	obj.env.dispose();

    } catch (GRBException e) {
      System.out.println("Error code: " + e.getErrorCode() + ". " +
                         e.getMessage());
    }
  }
}

