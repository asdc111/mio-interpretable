package runners;

import java.util.ArrayList;

import org.ejml.data.DenseMatrix64F;
import org.ejml.ops.MatrixIO;
import org.ejml.simple.SimpleMatrix;

import gurobi.GRB;
import gurobi.GRBConstr;
import models.NewDecisionTree;
import models.NewDecisionTree_Revamped;
import models.SingleMIP;

public class MultipleMIP {
	 public static void main(String[] args) {
		 	double interpretabilityWeights[] = {0.9, 0.6, 0.2, 0.5, 0.5, 0.4, 0.8, 0.3, 0.1, 0.2, 0.3, 0.1, 0.2, 0.5, 0.1, 0.4, 0.7, 0.3, 0.5, 0.5};
		 	double optimalityGap=0.2;
		 	int Depth=1;
		 	int Nmin=1;
		 	double numFolds = 1;
		 	
		 	double fraction = 1;
		 	
		 	int numFeatures=22, numPoints=2000;
		 	
		 	
		 	DenseMatrix64F dataset;
		    SimpleMatrix originalData, data, train, test;
		    
		    String basedir = "/Users/amulyayadav/Dropbox/Work/PostAAMAS/PhebeWork/Bottleneck/";
		    try 
		    {	
	    		dataset = MatrixIO.loadCSV(basedir+"masterdataset-mip.csv");
	    		
	    		originalData = SimpleMatrix.wrap(dataset);
	    	
	    		
	    		data = originalData.extractMatrix( 0, numPoints, originalData.numCols() - numFeatures , originalData.numCols());
	    		
	    		//data.print();
	    		//data.printDimensions();
	    		
	    		///renormalize data
	    		for (int i=0;i<data.numCols()-1;i++)
	    		{
	    			double maxVal=-1, minVal=10000;
	    			for (int j=0;j<data.numRows();j++)
	    			{
	    				if (data.get(j,i)<minVal)
	    					minVal=data.get(j,i);
	    				if (data.get(j,i)>maxVal)
	    					maxVal=data.get(j,i);
	    			}
	    			
	    			if (maxVal-minVal!=0)
	    			{
	    				for (int j=0;j<data.numRows();j++)
	    				{
	    					data.set(j, i, (data.get(j,i)-minVal)/(maxVal-minVal));
	    				}
	    			}
	    		}
	    		///submatrix renormalized
	    		
	    		
	    		
	    		ArrayList<Integer> ones = new ArrayList<Integer>();
	    		ArrayList<Integer> zeros = new ArrayList<Integer>();
	    		
	    		for (int i=0;i<data.numRows();i++)
	    		{
	    			if (data.get(i, data.numCols()-1)==1)
	    				ones.add(i);
	    			else
	    				zeros.add(i);
	    		}
	    		
	    		
	    		ArrayList<Integer> trainIndices = new ArrayList<Integer>();
	    		ArrayList<Integer> testIndices = new ArrayList<Integer>();
	    		
	    		int numberOfOnes = (int)(fraction*ones.size());
	    		int numberOfZeros = (int)(fraction*zeros.size());
	    		
	    		for (int i=0;i<numberOfOnes; i++)
	    		{
	    			int randInt = (int)(Math.random()*ones.size());
	    			trainIndices.add(ones.get(randInt));
	    			ones.remove(randInt);
	    		}
	    		
	    		for (int i=0;i<numberOfZeros; i++)
	    		{
	    			int randInt = (int)(Math.random()*zeros.size());
	    			trainIndices.add(zeros.get(randInt));
	    			zeros.remove(randInt);
	    		}
	    		
	    		for (int i=0;i<ones.size();i++)
	    			testIndices.add(ones.get(i));
	    		for (int i=0;i<zeros.size();i++)
	    			testIndices.add(zeros.get(i));
	    		
	    		train = new SimpleMatrix(0,0);
	    		test = new SimpleMatrix(0,0);
	    		
	    		for (int i=0;i<trainIndices.size();i++)
	    			train = train.combine(SimpleMatrix.END,0,data.extractVector(true, trainIndices.get(i)));
	    		
	    		for (int i=0;i<testIndices.size();i++)
	    			test = test.combine(SimpleMatrix.END,0,data.extractVector(true, testIndices.get(i)));
	    		
	    		train.print(5,6);
	    		
	    		///leaving first and last column
	    		double t1 = System.currentTimeMillis();
	    		double maxVal=-1;
	    		int maxFeat = -1;
	    		for (int i=1;i<train.numCols()-1;i++)
	    		{
	    			ArrayList<Integer> arr = new ArrayList<Integer>();
	    			arr.add(i-1);
	    			SingleMIP mip = new SingleMIP(basedir+"masterdataset-mip.csv", Depth, Nmin, 0.0, interpretabilityWeights, basedir+"tradeoff-decisiontree.txt", 1 - numFolds/100, train, test,arr);
	    			mip.model.getEnv().set(GRB.IntParam.Method, 0);
    				mip.model.optimize();
    				if (mip.model.get(GRB.DoubleAttr.ObjVal)>maxVal)
    				{
    					maxVal = mip.model.get(GRB.DoubleAttr.ObjVal);
    					maxFeat = i-1;
    				}
    				System.out.println(mip.model.get(GRB.DoubleAttr.ObjVal)+", "+ mip.model.get(GRB.DoubleAttr.Runtime));
	    		}
	    		double t2 = System.currentTimeMillis();
	    		System.out.println("Best solution found: "+ maxVal+" Time taken: "+ (t2-t1));
	    		

	    		
	    		
	    		/*System.out.println("Average precision (10 fold): "+averageError[0]/numFolds);
	    		System.out.println("Average recall (10 fold): "+averageError[1]/numFolds);
	    		System.out.println("Average f1 (10 fold): "+averageError[2]/numFolds);
	    		System.out.println("Average misclassification rate (10 fold): "+averageError[3]/numFolds);*/
	    	
	    		
		    }
		    catch(Exception e)
		    {
		    	e.printStackTrace();
		    }
	 }
}