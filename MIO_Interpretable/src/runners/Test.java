package runners;

public class Test {
	
	public static void printArray(int as[])
	{
		for (int i=0;i<as.length;i++)
			System.out.println(as[i]+" ");
		System.out.println();
	}
	
	public static void main(String args[])
	{
		int a[][] = new int[4][4];
		for (int i=0;i<4;i++)
		{
			for (int j=0;j<4;j++)
			{
				a[i][j] = (i+1)*(j+1);
			}
		}
		
		printArray(a[2]);
		
	}

}
