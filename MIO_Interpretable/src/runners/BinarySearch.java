package runners;

import gurobi.GRB;
import gurobi.GRBLinExpr;
import models.F1DecisionTree;

//Binary searches over alpha parameter for optimizing f1
public class BinarySearch {
	 public static void main(String[] args) {
		 try {
		 	double interpretabilityWeights[] = {0.9, 0.6, 0.2, 0.5, 0.5, 0.4, 0.8, 0.3, 0.1, 0.2, 0.3, 0.1, 0.2, 0.5, 0.1, 0.4, 0.7, 0.1, 0.2, 0.4, 0.2};
		 	double optimalityGap=0.2;
		 	int Depth=1;
		 	int Nmin=1;
		 	double numFolds = 10;
		 	double lambda=0;
		 	
		 	String basedir = "/Users/amulyayadav/Dropbox/Work/PostAAMAS/EceProject/Clustering/5PhebeRegression/For2NodesOnly/normalized-extended/";
		    
		 	double min = 0;
		 	double max = 1;
		 	double curr_alpha = (max+min)/2;
		 	F1DecisionTree tree = new F1DecisionTree(basedir+"class-threshold6-newnormalize-20feat.csv", Depth, Nmin, lambda, interpretabilityWeights, basedir+"tradeoff-decisiontree.txt", 1 - numFolds/100, curr_alpha);
		 	boolean flag=true;
		 	while (max-min>1e-2)
		 	{
		 		curr_alpha = (max+min)/2;
		   		tree.updateObjective(curr_alpha);
		 		tree.model.getEnv().set(GRB.DoubleParam.TimeLimit, 3600);
				tree.model.optimize();
				
				if (tree.model.get(GRB.IntAttr.SolCount)>0)
				{
				if (tree.model.get(GRB.DoubleAttr.ObjVal)>=0)
				{
					min = curr_alpha;
				}
				else if (tree.model.get(GRB.DoubleAttr.ObjVal)<0)
				{
					max = curr_alpha;
				}
				}
				else if (tree.model.get(GRB.IntAttr.Status)==9 && tree.model.get(GRB.IntAttr.SolCount)==0)
				{
					flag=false;
					System.out.println("Zero solutions found in the time limit. Need to increase time limit. No other option.");
					break;
				}
		 	}
		 	if (flag==false)
		 	System.out.println("Last F1 score found or tried: "+max + " "+min + " "+curr_alpha);
		 	if (flag==true)
		 	{
		 	System.out.println("Optimal F1 score found: "+ (max+min)/2);
		 	tree.printSolution();
			double res[] = tree.evaluateClassifieronTestSet();
    		System.out.println("Precision: "+res[0]);
    		System.out.println("Recall: "+res[1]);
    		System.out.println("F1: "+res[2]);
    		System.out.println("Misclassification rate: "+res[3]);
		 	}
		    }
		    catch(Exception e)
		    {
		    	e.printStackTrace();
		    	System.out.println("Message: " + e.getMessage());
		    }
	 }
}