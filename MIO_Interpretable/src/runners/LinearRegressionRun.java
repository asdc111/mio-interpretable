package runners;

import models.LinearRegression;

public class LinearRegressionRun {
	 public static void main(String[] args) {
		 	double interpretabilityWeights[] = {0.5, 0.1, 0.4};
		 	String basedir = "/Users/amulyayadav/Dropbox/Work/PostAAMAS/EceProject/Clustering/5PhebeRegression/ForOneNodeOnly/Post-Change/normalized/"; 
		    try {
		    	for (double lambda = 0.1; lambda<20 ; lambda=lambda+0.1)
		    	{
		    		for (double lambda2 = 0.1 ; lambda2<20; lambda2 = lambda2 +0.1)
		    		{
		    		LinearRegression obj = new LinearRegression(lambda, lambda2, interpretabilityWeights, basedir+"regression.csv", basedir+"tradeoff.txt");
		    	
		    		obj.model.optimize();
		    	
		    		obj.printSolution();

		    		// Dispose of model and environment
		    		obj.model.reset();
		    		obj.model.dispose();
		    		obj.env.dispose();
		    		}
		    	}
		    }
		    catch(Exception e)
		    {
		    	e.printStackTrace();
		    	System.out.println("Message: " + e.getMessage());
		    }
	 }
}