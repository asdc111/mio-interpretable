package cplex_runners;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import org.ejml.data.DenseMatrix64F;
import org.ejml.ops.MatrixIO;
import org.ejml.simple.SimpleMatrix;

import cplexmodels.Dtree4;
import cplexmodels.Dtree5;
import ilog.concert.IloException;
import ilog.cplex.IloCplex;

public class Dtree5Run {
	public static boolean loadLibrariesCplex(String ConfigFile) throws IOException{
		FileReader fstream = new FileReader(ConfigFile);
		BufferedReader in = new BufferedReader(fstream);

		String CplexFileString = null;

		String line = in.readLine();
		
		while(line != null){
			line.trim();
			
			if(line.length() > 0 && !line.startsWith("#")){
				String[] list = line.split("=");
				
				if(list.length != 2){
					throw new RuntimeException("Unrecognized format for the config file.\n");
				}
				
				if(list[0].trim().equalsIgnoreCase("LIB_FILE")){
					CplexFileString = list[1];
				}
				else{
					System.err.println("Unrecognized statement in Config File: " + line);
					return false;
				}
			}
			
			line = in.readLine();
		}

		File CplexFile = new File(CplexFileString);
		
		System.load(CplexFile.getAbsolutePath());
		
		return true;
	}
	
	 public static void main(String[] args) {
		 
		 	
		 	/*String basedir = "/Users/amulyayadav/Dropbox/Work/PostAAMAS/PhebeWork/Bottleneck/";
		 	double interpretabilityWeights[] = {0.9, 0.6, 0.2, 0.5, 0.5, 0.4, 0.8, 0.3, 0.1, 0.2, 0.3, 0.1, 0.2, 0.5, 0.1, 0.4, 0.7, 0.3, 0.5, 0.5};
		 	double optimalityGap=0.2;
		 	int Depth=2;
		 	int Nmin=1;
		 	double numFolds = 1;
		 	double timeLimit = 1800;
		 	int numFeatures=22, numPoints=2000;
		 	int numFeaturesUsedInTree=3;*/
		 	
		 	double fraction = 1;
		 	
		 	
		 
		 	
		 	DenseMatrix64F dataset;
		    SimpleMatrix originalData, data, train, test;
		    
            try
            {
                String interpretabilityWeightString[] = args[4].split(":");
                double interpretabilityWeights[] = new double[interpretabilityWeightString.length];
                for (int i=0;i<interpretabilityWeights.length;i++)
                        interpretabilityWeights[i] = Double.parseDouble(interpretabilityWeightString[i]);
                double lambda=Double.parseDouble(args[3]);
                double optimalityGap=Double.parseDouble(args[2]);
                int Depth=Integer.parseInt(args[1]);
                double numFolds = 1;
                double timeLimit = Double.parseDouble(args[5]);
                int Nmin=1;
                String basedir = args[0];

                int numFeatures = Integer.parseInt(args[6]);
                int numPoints = Integer.parseInt(args[7]);
                
                int numFeaturesUsedInTree = Integer.parseInt(args[8]);
            	
                
                dataset = MatrixIO.loadCSV(basedir+"masterdataset-mip.csv");

                originalData = SimpleMatrix.wrap(dataset);


                data = originalData.extractMatrix( 0, numPoints, originalData.numCols() - numFeatures , originalData.numCols());

                

                ///renormalize data
                for (int i=0;i<data.numCols()-1;i++)
                {
                        double maxVal=-1, minVal=10000;
                        for (int j=0;j<data.numRows();j++)
                        {
                                if (data.get(j,i)<minVal)
                                        minVal=data.get(j,i);
                                if (data.get(j,i)>maxVal)
                                        maxVal=data.get(j,i);
                        }

                        if (maxVal-minVal!=0)
                        {
                                for (int j=0;j<data.numRows();j++)
                                {
                                        data.set(j, i, (data.get(j,i)-minVal)/(maxVal-minVal));
                                }
                        }
                }
                ///submatrix renormalized
                
                ArrayList<Integer> ones = new ArrayList<Integer>();
                ArrayList<Integer> zeros = new ArrayList<Integer>();

                for (int i=0;i<data.numRows();i++)
                {
                        if (data.get(i, data.numCols()-1)==1)
                                ones.add(i);
                        else
                                zeros.add(i);
                }


                ArrayList<Integer> trainIndices = new ArrayList<Integer>();
                ArrayList<Integer> testIndices = new ArrayList<Integer>();

                int numberOfOnes = (int)(fraction*ones.size());
                int numberOfZeros = (int)(fraction*zeros.size());

                for (int i=0;i<numberOfOnes; i++)
                {
                        int randInt = (int)(Math.random()*ones.size());
                        trainIndices.add(ones.get(randInt));
                        ones.remove(randInt);
                }

                for (int i=0;i<numberOfZeros; i++)
                {
                        int randInt = (int)(Math.random()*zeros.size());
                        trainIndices.add(zeros.get(randInt));
                        zeros.remove(randInt);
                }

                for (int i=0;i<ones.size();i++)
                        testIndices.add(ones.get(i));
                for (int i=0;i<zeros.size();i++)
                        testIndices.add(zeros.get(i));

                train = new SimpleMatrix(0,0);
                test = new SimpleMatrix(0,0);

                for (int i=0;i<trainIndices.size();i++)
                        train = train.combine(SimpleMatrix.END,0,data.extractVector(true, trainIndices.get(i)));

                for (int i=0;i<testIndices.size();i++)
                        test = test.combine(SimpleMatrix.END,0,data.extractVector(true, testIndices.get(i)));
                
                
                ////train and test constructed
                
                
                ///running code
                ArrayList<Double> avgSolQual = new ArrayList<Double>();
                ArrayList<Double> avgTime = new ArrayList<Double>();
                
                Dtree5 model;
                for (double i=0;i<numFolds;i++)
	    		{
	    			model = new Dtree5(basedir+"masterdataset-mip.csv", Depth, Nmin, 0.0, interpretabilityWeights, basedir+"tradeoff-decisiontree.txt", 1 - numFolds/100, train, test, numFeaturesUsedInTree);
	    			model.setBranchPriorities();
	    			model.cplex.setParam(IloCplex.DoubleParam.TiLim, timeLimit);
	    			model.solve();
	    			avgSolQual.add(model.objectiveVal);
	    			avgTime.add(model.runTime);
	    			model.clean();
	    			System.out.println("Run "+(i+1)+" completed..................................");
	    		}
                
                ////print output code
                System.out.print("Average time: ");
                double avgVal=0;
                for (int i=0;i<avgTime.size();i++)
                	avgVal+= avgTime.get(i);
                avgVal/=avgTime.size();
                System.out.println(avgVal/1000+" seconds");
                
            }
            catch(IOException e)
            {
            	e.printStackTrace();
            } catch (IloException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		    

	 }
}
