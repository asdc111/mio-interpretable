package cplexmodels;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;

import org.ejml.simple.SimpleMatrix;

import gurobi.GRB;
import ilog.concert.IloColumn;
import ilog.concert.IloException;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumExpr;
import ilog.concert.IloNumVar;
import ilog.concert.IloNumVarType;
import ilog.concert.IloRange;
import ilog.cplex.CpxException;
import ilog.cplex.IloCplex;
import ilog.cplex.IloCplex.CplexStatus;
import ilog.cplex.IloCplex.UnknownObjectException;



public class Dtree5{
	public IloCplex cplex;
	
    public IloNumVar x[][], a[][], b[], z[], w[][], yplus[][], yminus[][], tp[], tn[];// fp[], fn[], lin[][];// hplus[], hminus[];
    public IloNumVar hplus[], hminus[];
    public IloNumVar lin[][];
    
    public IloNumVar m[];
    
    public HashMap<String, IloRange> rowMap;
	
    SimpleMatrix train, test;
    SimpleMatrix  Xtrain, Ytrain, Xtest, Ytest;
    
    int treeDepth, Nmin;
    double interpretabilityWeights[];
    double lambda,lambda2;
    String outputfile;
    
    int numFeaturesUsedInTree;
    
    static double smallM;
    static double bigM;
    
    static double numerical_instability_epsilon = 0.001;
    
    public double runTime;
    public double objectiveVal;
    
   
    
    public Dtree5(String filename, int treeDepth, int Nmin, double lambda, double interpretabilityWeights[], String outputfile, double fractionTrainSet, SimpleMatrix train, SimpleMatrix test, int numFeaturesUsedInTree)
    {
    	try
    	{
    		this.treeDepth = treeDepth;
    		this.Nmin = Nmin;
    	  	this.lambda = lambda;
        	this.interpretabilityWeights = interpretabilityWeights;
        	this.outputfile = outputfile;
        	this.numFeaturesUsedInTree = numFeaturesUsedInTree;
        	
        	rowMap = new HashMap<String, IloRange>();
        	runTime=-1;
        	
        	
        	this.train = train;
        	this.test = test;
       		Xtrain = train.extractMatrix(0, train.numRows(), 1, train.numCols()-1);//leave first col..last column for label
    		Ytrain = train.extractVector(false, train.numCols()-1);///last label
    		
    		bigM = Xtrain.numRows();
    		
    		cplex = new IloCplex();
			cplex.setName("MIProblem");
			
			specifyModel();
    	}
    	catch(IloException e)
    	{
    		e.printStackTrace();
    	}
    }
    
    public void solve()
    {
    	long start = System.currentTimeMillis();
		try 
		{
			if (cplex.solve())
			{
				cplex.output().println("Solution status = " + cplex.getStatus());
	            cplex.output().println("Solution value  = " + cplex.getObjValue());
	            this.objectiveVal = cplex.getObjValue();
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		this.runTime = System.currentTimeMillis() - start;
		System.out.println("Runtime: "+this.runTime);
    }
    
    public void clean()
    {
    	cplex.end();
    }
    
    public void setBranchPriorities()
    {
    	int numTreeNodeVars = (int) (Math.pow(2, treeDepth+1) - 1);
    	
    	try
    	{
    		///set branch priorities
    		int priorityVal=Integer.MAX_VALUE;
    		for (int f=0;f<Xtrain.numCols();f++)
    		{	
    			if (priorityVal<=0)
					cplex.setPriority(m[f], 0);
				else
					cplex.setPriority(m[f], priorityVal);	
    			priorityVal--;
    		}
    		
    		for (int n=0;n<numTreeNodeVars/2;n++)
    		{	
    			for (int f=0;f<Xtrain.numCols();f++)
    			{
    				if (priorityVal<=0)
    					cplex.setPriority(a[n][f], 0);
    				else
    					cplex.setPriority(a[n][f], priorityVal);		
    			}
    			priorityVal--;
    		}		
		
    		for (int i=0;i<Xtrain.numRows();i++)
    		{
    			for (int n=0;n<numTreeNodeVars/2;n++)
    			{
    				if (priorityVal<=0)
    					cplex.setPriority(w[i][n], 0);
    				else
    					cplex.setPriority(w[i][n], priorityVal);
    			}
    			priorityVal--;
    		}
    	}
    	catch(IloException e)
    	{
    		e.printStackTrace();
    	}
    }
    
    public void specifyModel()
    {
    	try
    	{
    		int numTreeNodeVars = (int) (Math.pow(2, treeDepth+1) - 1);
    		
    		
    		double epsilon[] = new double[Xtrain.numCols()];
    		double feature[] = new double[Xtrain.numRows()];
    		double max_epsilon_entry=-1;
    		int min_epsilon_index=-1;
    		double min_epsilon_entry=10000;
    		for (int i=0;i<Xtrain.numCols();i++)
    		{
    			for (int j=0;j<Xtrain.numRows();j++)
    				feature[j] = Xtrain.get(j, i);
    			Arrays.sort(feature);
    			double minVal=10000;
    			for (int j=feature.length-1;j>0;j--)
    			{
    				double temp = feature[j]-feature[j-1];
    				if (temp!=0 && temp<minVal)
    					minVal = temp;
    			}
    			epsilon[i] = minVal;
    			if (epsilon[i]<min_epsilon_entry)
    			{
    				min_epsilon_index = i;
    				min_epsilon_entry = epsilon[i];
    			}
    			if (epsilon[i]>max_epsilon_entry)
    				max_epsilon_entry = epsilon[i];
    		}
    		smallM = min_epsilon_entry;
    		System.out.println("Min epsilon"+ min_epsilon_entry + " "+min_epsilon_index);
    		
    		m = new IloNumVar[Xtrain.numCols()];
    		for (int f=0;f<Xtrain.numCols();f++)
    		{
    			m[f] = cplex.numVar(0.0, 1.0, IloNumVarType.Bool, "m_"+f);
    		}
    		
    		
    		lin = new IloNumVar[Xtrain.numRows()][numTreeNodeVars/2+1];
    		x = new IloNumVar[Xtrain.numRows()][numTreeNodeVars/2+1];
    		z = new IloNumVar[numTreeNodeVars/2+1];
       		for (int l=0; l<numTreeNodeVars/2+1; l++)
    		{
       			z[l] = cplex.numVar(0.0, 1.0, IloNumVarType.Float, "z_"+l);
    			for (int i=0;i<Xtrain.numRows();i++)
    			{
    				x[i][l] = cplex.numVar(0.0, 1.0, IloNumVarType.Float, "x_"+i+"_"+l);
    				lin[i][l] = cplex.numVar(0.0, 1.0, IloNumVarType.Float, "lin_"+i+"_"+l);
    			}
    		}
       		
       		yplus = new IloNumVar[Xtrain.numRows()][numTreeNodeVars/2];
    		yminus = new IloNumVar[Xtrain.numRows()][numTreeNodeVars/2];
    		for (int i=0;i<Xtrain.numRows();i++)
    		{
    			for (int n=0;n<numTreeNodeVars/2;n++)
    			{
    				yplus[i][n] = cplex.numVar(0.0, 1.0, IloNumVarType.Float, "yplus_"+i+"_"+n);
    				yminus[i][n] = cplex.numVar(0.0, 1.0,IloNumVarType.Float, "yminus_"+i+"_"+n);
    			}
    		}
    		
       		a = new IloNumVar[numTreeNodeVars/2][Xtrain.numCols()];
    		b = new IloNumVar[numTreeNodeVars/2];
    		
    		for (int n=0;n<numTreeNodeVars/2;n++)
    		{	
    			b[n] = cplex.numVar(0.0, 1.0, IloNumVarType.Float, "b_"+n);
    			for (int f=0;f<Xtrain.numCols();f++)
    			{
    				a[n][f] = cplex.numVar(0.0, 1.0, IloNumVarType.Bool, "a_"+n+"_"+f);
    			}
    		}
    		
    		w = new IloNumVar[Xtrain.numRows()][numTreeNodeVars/2];
    		for (int i=0;i<Xtrain.numRows();i++)
			{
    			for (int n=0;n<numTreeNodeVars/2;n++)
    			{
    				w[i][n] = cplex.numVar(0.0, 1.0, IloNumVarType.Bool, "w_"+i+"_"+n);
    			}
    		}
    		
    		tp = new IloNumVar[Xtrain.numRows()];
    		tn = new IloNumVar[Xtrain.numRows()];
    		
    		for (int i=0;i<Xtrain.numRows();i++)
    		{
    			tp[i] = cplex.numVar(0.0, 1.0, IloNumVarType.Float, "tp_"+i);
    			tn[i] = cplex.numVar(0.0, 1.0, IloNumVarType.Float, "tn_"+i);
    		}
    		

    		
    		
    		///objective function
    		///TO DO:CHECK NUMERICAL
    		IloNumExpr obj = cplex.constant(0.0);
    		for (int i=0;i<Xtrain.numRows();i++)
    		{
    			if (Ytrain.get(i)>1-numerical_instability_epsilon)
    				obj = cplex.sum(obj, tp[i]);
    			else if (Ytrain.get(i)<numerical_instability_epsilon)
    				obj = cplex.sum(obj, tn[i]);
    		}
    		//EQUATION 1
    		cplex.addMaximize(obj, "Obj");
    		
    		
    		
    		///constraints begin
       		for (int i=0;i<Xtrain.numRows();i++)
    		{
       			IloNumExpr constraint2 = cplex.constant(0.0);
    			for (int l=0;l<numTreeNodeVars/2+1;l++)
    				constraint2 = cplex.sum(constraint2, x[i][l]);
    			
    			//EQUATION 2
    			IloRange constr2 = cplex.addEq(constraint2, 1.0, "eq2_"+i);
    			rowMap.put("eq2_"+i, constr2);
    		}
       		
    
    		for (int n=0;n<numTreeNodeVars/2;n++)
    		{
    			IloNumExpr constraint3 = cplex.constant(0.0);
    			for (int f=0;f<Xtrain.numCols();f++)
    				constraint3 = cplex.sum(constraint3, a[n][f]);
    			
    			///EQUATION 3
    			IloRange constr3 = cplex.addEq(constraint3, 1.0, "eq3_"+n);
    			rowMap.put("eq3_"+n, constr3);
    			
    		    
    			
    			int rightNode = 2*n+2;
    			ArrayList<Integer> rightIndices = new ArrayList<Integer>();
    			ArrayList<Integer> leftIndices = new ArrayList<Integer>();
    			for (int l=0;l<numTreeNodeVars/2+1;l++)
    			{
    				int currInd = l+numTreeNodeVars/2;
    				while(currInd>rightNode)
    				{
    					if (currInd%2==0)
    						currInd = currInd/2-1;
    					else
    						currInd = currInd/2;
    					
    					if (currInd==rightNode || currInd==rightNode-1)
    						break;
    				}
    				if (currInd==rightNode)
    					rightIndices.add(l);
    				else if (currInd == rightNode - 1)
    					leftIndices.add(l);
    			}
    			
    			for (int i=0;i<Xtrain.numRows();i++)
    			{	
    				for (int l=0;l<rightIndices.size();l++)
    				{
    					IloNumExpr constraint4 = cplex.sum(x[i][rightIndices.get(l)], w[i][n]);
    					///EQUATION 4
    					IloRange constr4 = cplex.addLe(constraint4,  1.0, "eq4_"+n+"_"+i+"_"+l);
    					rowMap.put("eq4_"+n+"_"+i+"_"+l, constr4);
    				}
    				
    				for (int l=0;l<leftIndices.size();l++)
    				{
    					IloNumExpr constraint5 = cplex.diff(x[i][leftIndices.get(l)], w[i][n]);
    					///EQUATION 5
    					IloRange constr5 = cplex.addLe(constraint5,  0.0, "eq5_"+n+"_"+i+"_"+l);
    					rowMap.put("eq5_"+n+"_"+i+"_"+l, constr5);
    				}
    				
    				IloNumExpr constraint6 = cplex.sum( cplex.sum(yminus[i][n], yplus[i][n]), cplex.prod(smallM, w[i][n]));
    				///EQUATION 6
    				IloRange constr6 = cplex.addGe(constraint6, smallM, "eq6_"+i+"_"+n);
    				rowMap.put("eq6_"+i+"_"+n, constr6);
    			}
    		}
    		
    		
    		for (int i=0;i<Xtrain.numRows();i++)
    		{
    			for (int n=0;n<numTreeNodeVars/2;n++)
    			{
    				IloNumExpr constraint7  = cplex.constant(0.0);
    				for (int f=0;f<Xtrain.numCols();f++)
    					constraint7 = cplex.sum(constraint7, cplex.prod(Xtrain.get(i, f), a[n][f]));
    					
    				constraint7 = cplex.sum(constraint7, cplex.prod(-1.0, b[n]));
    				constraint7 = cplex.sum(constraint7, cplex.prod(1.0, yplus[i][n]));
    				constraint7 = cplex.sum(constraint7, cplex.prod(-1.0, yminus[i][n]));
    				///EQUATION 7
    				IloRange constr7 = cplex.addEq(constraint7, 0.0, "eq7_"+i+"_"+n);
    				rowMap.put("eq7_"+i+"_"+n, constr7);
    	
    					
    				
    				IloNumExpr constraint8 = cplex.diff(yplus[i][n], w[i][n]);
					///EQUATION 8
					IloRange constr8 = cplex.addLe(constraint8,  0.0, "eq8_"+i+"_"+n);
					rowMap.put("eq8_"+i+"_"+n, constr8);
					
					
					IloNumExpr constraint9 = cplex.sum(yminus[i][n], w[i][n]);
					///EQUATION 9
					IloRange constr9 = cplex.addLe(constraint9,  1.0, "eq9_"+i+"_"+n);
					rowMap.put("eq9_"+i+"_"+n, constr9);
    			}
    			
    			
    			for (int l=0;l<numTreeNodeVars/2+1;l++)
    			{
    				IloNumExpr constraint10_1 = cplex.constant(0.0);
    				constraint10_1 = cplex.sum(constraint10_1, lin[i][l]);
    				constraint10_1 = cplex.diff(constraint10_1, x[i][l]);
    				constraint10_1 = cplex.diff(constraint10_1, z[l]);
					///EQUATION 10_1
					IloRange constr10_1 = cplex.addGe(constraint10_1,  -1.0, "eq10^1_"+i+"_"+l);
					rowMap.put("eq10^1_"+i+"_"+l, constr10_1);
					
					IloNumExpr constraint10_2 = cplex.diff(lin[i][l], z[l]);
					///EQUATION 10_2
					IloRange constr10_2 = cplex.addLe(constraint10_2, 0.0, "eq10^2_"+i+"_"+l);
					rowMap.put("eq10^2_"+i+"_"+l, constr10_2);
					
					IloNumExpr constraint10_3 = cplex.diff(lin[i][l], x[i][l]);
					///EQUATION 10_3
					IloRange constr10_3 = cplex.addLe(constraint10_3, 0.0, "eq10^3_"+i+"_"+l);
					rowMap.put("eq10^3_"+i+"_"+l, constr10_3);    				
    				
    				//EQUATION 10
    			}
    			
    			////TO DO: CHECK NUMERICAL
    			if (Ytrain.get(i)>1-numerical_instability_epsilon)
    			{
    				IloNumExpr constraint11_1 = cplex.constant(0.0);
    				constraint11_1 = cplex.sum(constraint11_1, tp[i]);
    				
    				for (int l=0;l<numTreeNodeVars/2+1;l++)
    					constraint11_1 = cplex.sum(constraint11_1, cplex.prod(-1.0, lin[i][l]));
    				
    				//EQUATION 11_1
    				IloRange constr11_1 = cplex.addEq(constraint11_1, 0.0, "eq11^1_"+i);
					rowMap.put("eq11^1_"+i, constr11_1); 
    			}
    			
    			if (Ytrain.get(i)<numerical_instability_epsilon)
    			{
    				IloNumExpr constraint11_2 = cplex.constant(0.0);
    				constraint11_2 = cplex.sum(constraint11_2, tn[i]);
    				
    				for (int l=0;l<numTreeNodeVars/2+1;l++)
    					constraint11_2 = cplex.sum(constraint11_2, cplex.diff(lin[i][l], x[i][l]));
    				
    				//EQUATION 11_2
    				IloRange constr11_2 = cplex.addEq(constraint11_2, 0.0, "eq11^2_"+i);
					rowMap.put("eq11^2_"+i, constr11_2); 
    			}
    			///EQUATION 11
    		}
    		
    		for (int n=0;n<numTreeNodeVars/2;n++)
    		{
    			ArrayList<Integer> rightIndices = new ArrayList<Integer>();
    			ArrayList<Integer> leftIndices = new ArrayList<Integer>();
    			
    			int leftChild = 2*n+1;
    			int rightChild = 2*n+2;
    			
    			if (leftChild<numTreeNodeVars/2)
    				leftIndices.add(leftChild);
    			if (rightChild<numTreeNodeVars/2)
    				rightIndices.add(rightChild);
    			
    			int index=0;
    			while(index<leftIndices.size())
    			{
    				int element = leftIndices.get(index);
    				if (2*element+1<numTreeNodeVars/2)
    					leftIndices.add(2*element+1);
    				if (2*element+2<numTreeNodeVars/2)
    					leftIndices.add(2*element+2);
    				index++;
    			}
    			
    			index=0;
    			while(index<rightIndices.size())
    			{
    				int element = rightIndices.get(index);
    				if (2*element+1<numTreeNodeVars/2)
    					rightIndices.add(2*element+1);
    				if (2*element+2<numTreeNodeVars/2)
    					rightIndices.add(2*element+2);
    				index++;
    			}
    			
    			for (int i=0;i<Xtrain.numRows();i++)
    			{
    				for (int nprime=0;nprime<rightIndices.size();nprime++)
    				{
    					IloNumExpr constraint12_1 = cplex.sum(w[i][n], w[i][rightIndices.get(nprime)]);
    					IloRange constr12_1 = cplex.addLe(constraint12_1, 1.0, "eq12^1_"+i+"_"+n+"_"+nprime);
    					rowMap.put("eq12^1_"+i+"_"+n+"_"+nprime, constr12_1);
    					//EQUATION 12_1
    				}
    				
    				for (int nprime=0;nprime<leftIndices.size();nprime++)
    				{
    					IloNumExpr constraint12_2 = cplex.diff(w[i][n], w[i][leftIndices.get(nprime)]);
    					IloRange constr12_2 = cplex.addGe(constraint12_2, 0.0, "eq12^2_"+i+"_"+n+"_"+nprime);
    					rowMap.put("eq12^2_"+i+"_"+n+"_"+nprime, constr12_2);
    					//EQUATION 12_2
    				}
    			}
    		}
    		
    		
    		IloNumExpr constraint14 = cplex.constant(0.0);
    		for (int f=0;f<Xtrain.numCols();f++)
    		{
    			IloNumExpr constraint13_1  = cplex.constant(0.0);
				for (int n=0;n<numTreeNodeVars/2;n++)
					constraint13_1 = cplex.sum(constraint13_1, a[n][f]);
				constraint13_1 = cplex.diff(constraint13_1, m[f]);
				
				IloRange constr13_1 = cplex.addGe(constraint13_1, 0.0, "eq13^1_"+f);
				rowMap.put("eq13^1_"+f, constr13_1);
				//EQUATION 13_1
				
				for (int n=0;n<numTreeNodeVars/2;n++)
				{
					IloNumExpr constraint13_2 = cplex.diff(m[f], a[n][f]);
					IloRange constr13_2 = cplex.addGe(constraint13_2, 0.0, "eq13^2_"+f+"_"+n);
					rowMap.put("eq13^2_"+f+"_"+n, constr13_2);
					//EQUATION 13_2
				}
				
				constraint14 = cplex.sum(constraint14, m[f]);
    		}
    		IloRange constr14 = cplex.addLe(constraint14, numFeaturesUsedInTree, "eq14");
			rowMap.put("eq14", constr14);
			//EQUATION 13_1

    		
    		
    	}
    	catch(IloException e)
    	{
    		System.out.println(e.getMessage());
    		e.printStackTrace();
    	}
    }
}